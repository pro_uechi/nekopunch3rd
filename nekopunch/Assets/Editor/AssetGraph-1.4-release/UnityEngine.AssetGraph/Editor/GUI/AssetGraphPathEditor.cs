﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// アセットバンドルを生成先（パス）を保存するための機能
/// </summary>
public class AssetGraphPathEditor : ScriptableObject
{
    //public string m_cashePath;
    public string m_assetBundleBuildCacheDir;
}
