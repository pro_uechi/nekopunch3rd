﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

using Model = UnityEngine.AssetGraph.DataModel.Version2;

namespace UnityEngine.AssetGraph
{
    public class SettingPathWindow : EditorWindow
    {

        [MenuItem(Model.Settings.GUI_TEXT_MENU_SETTING_PATH, false, 1)]
        static public void Open()
        {
            GetWindow<SettingPathWindow>();
        }

        /// <summary>
        /// アセットバンドルを生成する先を保存するアセットファイルを作成
        /// </summary>
        [MenuItem(Model.Settings.GUI_TEXT_MENU_CREATE_PATH)]
        static void CreateAssetPath()
        {
            var exampleAsset = CreateInstance<AssetGraphPathEditor>();
            AssetDatabase.CreateAsset(exampleAsset, "Assets/AssetGraphEditor.asset");
            AssetDatabase.Refresh();
        }

        private void OnGUI()
        {
            if (GUILayout.Button("Setting", GUILayout.Width(50f)))
            {
                var folderSelected =
                    EditorUtility.OpenFolderPanel(string.Empty, string.Empty, string.Empty);
                if (!string.IsNullOrEmpty(folderSelected))
                {
                    Debug.Log("PATH : "+folderSelected);
                }
            }
        }
    }
}
