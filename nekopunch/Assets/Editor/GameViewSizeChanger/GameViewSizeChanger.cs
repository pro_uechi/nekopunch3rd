﻿using System;
using System.Collections;
using System.Collections.Generic;
using Kyusyukeigo.Helper;
using UnityEditor;
using UnityEngine;

using UnityEngine.SceneManagement; // デバッグ用

namespace Syy.GameViewSizeChanger
{
    public class GameViewSizeChanger : EditorWindow
    {
        [MenuItem("Window/GameViewSizeChanger")]
        public static void Open()
        {
            GetWindow<GameViewSizeChanger>("GameViewSizeChanger");
        }

        private readonly GameViewSizeApplyer[] applyers = new GameViewSizeApplyer[]
        {
            //iOS
            new GameViewSizeApplyer() {Title="iPhone7,8", Aspect="9:16", Width=750, Height=1334, },
            new iPhoneXSizeApplyer() {Title="iPhoneX", Aspect="1:2", Width=1125, Height=2436, },
            new GameViewSizeApplyer() {Title="iPad9.7", Aspect="3:4", Width=1536, Height=2048, },
            // Android
            new GameViewSizeApplyer() {Title="GalaxyS8", Aspect="18.5：9", Width=1440, Height=2960, },
            new GameViewSizeApplyer() {Title="Android", Aspect="16：9", Width=1080, Height=1920, },
            new GameViewSizeApplyer() {Title="Android_18:9", Aspect="18:9", Width=1080, Height=2160, },
            
            //new SizeData() {Title="", Aspect="", Width=1, Height=1, },
        };

        Orientation orientation;
        int selectIndex = 0;

        void OnEnable()
        {
            int index = 0;
            foreach (var applyer in applyers)
            {
                applyer.orientation = orientation;
                applyer.OnChangeGameViewSize += OnChangeGameViewSize;
                if (applyer.IsSelect())
                {
                    selectIndex = index;
                }
                index++;
            }
        }

        void OnDisable()
        {
            foreach (var applyer in applyers)
            {
                applyer.OnChangeGameViewSize -= OnChangeGameViewSize;
            }
        }

        void OnGUI()
        {
            foreach (var applyer in applyers)
            {
                applyer.OnGUI();
            }

            using (var check = new EditorGUI.ChangeCheckScope())
            {
                orientation = (Orientation)EditorGUILayout.EnumPopup("Orientation", orientation);
                if (check.changed)
                {
                    foreach (var applyer in applyers)
                    {
                        applyer.orientation = orientation;
                    }
                }
            }

            var e = Event.current;
            if (e.type == EventType.KeyDown)
            {
                if (e.keyCode == KeyCode.UpArrow)
                {
                    selectIndex--;
                    if (selectIndex < 0)
                    {
                        selectIndex = applyers.Length - 1;
                    }
                    applyers[selectIndex].Apply();
                    e.Use();
                }
                else if (e.keyCode == KeyCode.DownArrow)
                {
                    selectIndex++;
                    if (selectIndex > (applyers.Length - 1))
                    {
                        selectIndex = 0;
                    }
                    applyers[selectIndex].Apply();
                    e.Use();
                }
            }
        }

        void OnChangeGameViewSize()
        {
            Repaint();
            Focus();
            int index = 0;
            foreach (var applyer in applyers)
            {
                if (applyer.IsSelect())
                {
                    selectIndex = index;
                }
                else
                {
                    applyer.NoticeChangedOtherSize();
                }
                index++;
            }

            // IPhoneXの時はノッチ対応
            if (selectIndex == 2 && EditorApplication.isPlaying)
            {
                SafeAreaAdjuster.simulateOnPlay = true;
                // デバッグ用
                // 現在のScene名を取得する
                Scene loadScene = SceneManager.GetActiveScene();
                // Sceneの読み直し
                SceneManager.LoadScene(loadScene.name);
            }
            else if (EditorApplication.isPlaying)
            {
                SafeAreaAdjuster.simulateOnPlay = false;
                // デバッグ用
                // 現在のScene名を取得する
                Scene loadScene = SceneManager.GetActiveScene();
                // Sceneの読み直し
                SceneManager.LoadScene(loadScene.name);
            }
        }
    }
}
