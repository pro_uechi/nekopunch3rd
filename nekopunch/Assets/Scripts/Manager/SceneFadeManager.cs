﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.UI;

public class SceneFadeManager : SingletonMonoBehaviour<SceneFadeManager>
{
    /// <summary>
    /// ローディング用
    /// </summary>
    [SerializeField]
    private GameObject loadingObject;

    /// <summary>
    /// セッティング画面からカーテンなどのアニメーションで移動する際に使う
    /// </summary>
    static public bool SceneLoadFlg;



    public void Start()
    {
        SceneLoadFlg = false;
    }

    /// <summary>
    /// 画面遷移
    /// </summary>
    /// <param name="scene">シーン名</param>
    /// <param name="interval">暗転にかかる時間(秒)</param>
    /// <param name="ButtonRing">SEを鳴らすかどうか</param>
    public void Load(string scene, float interval, bool ButtonRing = false)
    {
        try
        {
            if (ButtonRing) AudioManager.Instance.PlaySE("OkSe");

            //if (scene == GameData.Scene_Shop)
            //{
            //    SceneButtonCanvas.Instance.Enable(false);
            //}
            SceneManager.LoadScene(scene);
            //if (scene == GameData.Scene_Battle)
            //{
            //    // SceneButtonCanvasとPlayerInfoCanvasを非表示にしてから遷移
            //    PlayerInfoCanvas.Instance.Enable(false);
            //    SceneManager.LoadScene(scene);
            //}
            //else
            //{
            //    SceneManager.LoadScene(scene);
            //}
        }
        catch (Exception ex)
        {
            GameObject msgBox = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
            msgBox.GetComponent<MessageBoxManager>().Initialize_OK("シーンの遷移に失敗しました。\n" + ex.Message + "\n" + ex.StackTrace, null);
        }
    }

    /// <summary>
    /// シーン遷移用コルーチン
    /// </summary>
    /// <param name="scene">シーン名</param>
    /// <param name="interval">暗転にかかる時間(秒)</param>
    private IEnumerator TransScene(string scene, float interval)
    {
        SceneLoadFlg = true;
        float time = 0;
        //シーン切り替えアニメーションが実行中であれば、まだスタートしない
        while (this.loadingObject.activeSelf)
        {
            yield return null;
        }

        this.loadingObject.SetActive(true);
        RawImage raw = this.loadingObject.transform.Find("Screen").GetComponent<RawImage>();
        Animator rawAnim = this.loadingObject.transform.Find("Screen").GetComponent<Animator>();
        AnimatorStateInfo Info = rawAnim.GetCurrentAnimatorStateInfo(0);

        // カーテンの演出
        if (raw != null)
        {
            //シーン切り替え時のアニメーション
            yield return new WaitForSeconds(Info.length);

            //while (time <= interval)
            //{
            //    raw.color = new Color(1, 1, 1, Mathf.Lerp(0f, 0.9f, time / interval));
            //    time += Time.deltaTime;
            //    yield return null;
            //}
            // 端数で真っ白にならないことがある為、補完する
            //raw.color = new Color(1, 1, 1, Mathf.Lerp(0f, 1f, time / interval));
        }

        // 「ちょっとまってね！」を表示
        //this.loadingObject.transform.FindChild("Text").gameObject.SetActive(true);

        // シーンがロード終えるまで待つ
        yield return SceneManager.LoadSceneAsync(scene);

        SceneLoadFlg = false;

        time = 0;

        // 「ちょっとまってね！」を非表示
        //this.loadingObject.transform.Find("Text").gameObject.SetActive(false);

        if (raw != null)
        {
            rawAnim.SetTrigger("ChangeScene");
            Info = rawAnim.GetCurrentAnimatorStateInfo(0);
            //シーン切り替え時のアニメーション
            yield return new WaitForSeconds(Info.length);
            //while (time <= interval)
            //{
            //    raw.color = new Color(1, 1, 1, Mathf.Lerp(0.9f, 0f, time / interval));
            //    time += Time.deltaTime;
            //    yield return null;
            //}

            // 端数で真っ白にならないことがある為、補完する
            //raw.color = new Color(1, 1, 1, Mathf.Lerp(1f, 0f, time / interval));
        }

        this.loadingObject.SetActive(false);

        // 遷移先のシーンで「BeforeButton」があれば押下時の遷移先をセット
        //GameObject beforeButton = GameObject.Find("Canvas/BeforeButton");
        //if (beforeButton != null)
        //{
        //    beforeButton.GetComponent<BeforeButtonManager>().SetGotoSceneName(this.beforeSceneName);
        //}
    }
}
