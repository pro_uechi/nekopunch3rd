﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using System.Linq;

public class StageMenuManage : MonoBehaviour
{
    #region 変数

    /// <summary>
    /// スクロールビュー(キャラクター)
    /// </summary>
    //[SerializeField]
   // private GameObject scrollView= null;

    /// <summary>
    /// ノードプレハブ(キャラクター)
    /// </summary>
    [SerializeField]
    private GameObject nodePrefab= null;

    /// <summary>
    /// ノードの親オブジェクト(キャラクター)
    /// </summary>
    [SerializeField]
    private GameObject nodeParent= null;

	/// <summary>
	/// 生成したノード
	/// </summary>
	private Dictionary<int, GameObject> nodes = new Dictionary<int, GameObject>();


	// 問題一覧リストを読込む
	// 問題ファイルの格納ディレクトリ
	//public const string LOAD_QUE_FOLDER = "Question/";
	public const string LOAD_QUE_FOLDER = "";
	// 問題リストの定義ファイル
	public const string LOAD_QUE_CATEGORY_LIST = LOAD_QUE_FOLDER + "LevelList";

	#endregion

	IEnumerator Start()
    {

		StartCoroutine(InstantiateNode_Character());

        //// 途中データがあるか
        //if (GameData.UserData.examLevel  != 0)
        //{
        //          Debug.LogError(GameData.UserData.examLevel + "途中データが存在します。");
        //	string message = "挑戦中ステージがあります。\n続きから始めますか？";

        //	GameObject msgBox = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
        //	msgBox.GetComponent<MessageBoxManager>().Initialize_YesNo(message, GotoResumeBattle, ResetResumeData);

        //}

        // 変更　途中データがあるか　
        if (GameData.UserData.examStage != 0)
        {
            string message = "挑戦中ステージがあります。\n続きから始めますか？";

            GameObject msgBox = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
            msgBox.GetComponent<MessageBoxManager>().Initialize_YesNo(message, GotoResumeBattle, ResetResumeData);

        }

        yield break;
	}

	/// <summary>
	/// キャラクターボタンを生成する
	/// </summary>
	/// <returns></returns>
	IEnumerator InstantiateNode_Character()
	{
		// リストを初期化
		GameData.StageDataList = new List<StageData>();
		Debug.Log("初期化しました。リストの数は" + GameData.StageDataList.Count);

		TextAsset textCategoryJson = Resources.Load(LOAD_QUE_CATEGORY_LIST) as TextAsset;

		GameData.StageDataList = Utility.ListFromJson<StageData>(textCategoryJson.text);

		Debug.Log("問題リスト数は" + GameData.StageDataList.Count);

		//// 問題数だけノードを生成
		foreach(StageData data in GameData.StageDataList) {
			//Debug.Log("なまえ1" + data.name);
			GameObject node = Instantiate(nodePrefab, nodeParent.transform);
			
			node.GetComponent<Button>().onClick.AddListener(delegate { OnClickStageButton(data); });
			//node.GetComponent<Button>().onClick.AddListener(delegate { OnClickStageButton(); });

			// ステージ名
			node.transform.Find("Name").GetComponent<Text>().text = data.name;

			// ロックアイコン　自レベル以上はロック
			if (data.Level > GameData.UserData.UserLevel + 1)
			{
				//node.transform.Find("icon").GetComponent<Image>().sprite = Resources.Load("UI/common/UI_Lock", typeof(Sprite)) as Sprite;
                node.transform.Find("icon").GetComponent<Image>().enabled = true;
			}
		}
		yield break;
	}

	/// <summary>
	/// ステージボタン押下時
	/// </summary>
	/// <param name="num"></param>
	public void OnClickStageButton(StageData stagedata)
	{
        /// <summary>
        /// ユーザーレベルより高いレベルを選んだ場合、これ以降処理を読まない
        /// </summary> 
        if (stagedata.Level > GameData.UserData.UserLevel + 1) { return; }

        // HOMEネコ削除、BATTLEネコ生成
        var deleteObject = GameData.PlayerObjct;
        string CatPrefabIDStr = "Prefabs/cat" + string.Format("{0:00}", SaveData.GetInt(SaveKey.UserCharacter));
        GameObject pObject = Instantiate((GameObject)Resources.Load(CatPrefabIDStr), new Vector3(0.0f, 0.0f, 0.0f), Quaternion.Euler(0, 180, 0));
        DontDestroyOnLoad(pObject);
        GameData.PlayerObjct = pObject;
        GameData.PlayerMeshObject = pObject.transform.Find("mesh").gameObject;
        GameData.PlayerObjct.name = "cat";
        GameData.SetPlayerActive(false);
        Destroy(deleteObject);

        GameData.Levelrank = stagedata.Level;
        GameData.LevelName = stagedata.name;
        SceneFadeManager.Instance.Load(GameData.Scene_Battle, GameData.FadeSpeed, true);
    }


	/// <summary>
	///　前回のつづきからプレイ
	/// </summary>
	public void GotoResumeBattle()
	{

        // HOMEネコ削除、BATTLEネコ生成
        GameObject deleteObject = null;
        deleteObject = GameData.PlayerObjct;
        string CatPrefabIDStr = "Prefabs/cat" + string.Format("{0:00}", SaveData.GetInt(SaveKey.UserCharacter));
        GameObject pObject = Instantiate((GameObject)Resources.Load(CatPrefabIDStr), new Vector3(0.0f, 0.0f, 0.0f), Quaternion.Euler(0, 180, 0));
        DontDestroyOnLoad(pObject);
        GameData.PlayerObjct = pObject;
        GameData.PlayerMeshObject = pObject.transform.Find("mesh").gameObject;
        GameData.PlayerObjct.name = "cat";
        GameData.SetPlayerActive(false);
        
        Destroy(deleteObject);
        GameData.Levelrank = GameData.UserData.examLevel;
		GameData.LevelName = GameData.StageDataList[GameData.Levelrank - 1].name;
		SceneFadeManager.Instance.Load(GameData.Scene_Battle, GameData.FadeSpeed, false);
	}


	/// <summary>
	/// 途中データを初期化
	/// </summary>
	public void ResetResumeData()
	{
		// クリアしてSAVE			
		GameData.UserData.examLevel = 0;
		GameData.UserData.examStage = 0;

		SaveData.SetInt(SaveKey.examLevel, GameData.Levelrank);
		SaveData.SetInt(SaveKey.examStage, GameData.UserData.examStage);
		SaveData.Save();

		//  削除
		Destroy(this);
	}

	/// <summary>
	/// 戻るボタン押下時
	/// </summary>
	public void ReturnButtonOnClick()
	{
		SceneFadeManager.Instance.Load(GameData.Scene_Home, GameData.FadeSpeed);
	}



}
