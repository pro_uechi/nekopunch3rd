﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AutoyaFramework;
using System;
using TMPro;
using DG.Tweening;


public class MatchingManager : MonoBehaviour
{
    #region 変数


	/// <summary>
	/// ステージ名とsubステージ
	/// </summary>
	[SerializeField]
	private TextMeshProUGUI  matchingText = null;


	/// <summary>
	/// 進行位置のPINアイコン
	/// </summary>
	[SerializeField]
	private GameObject mapPinIcon= null;

	#endregion

	IEnumerator Start()
    {
		// ステージ進行状況を設定
		StageInfoSetting();
		yield return null;
	}

	/// <summary>
	/// ステージ進行状況を設定
	/// </summary>
	private void StageInfoSetting()
    {
		matchingText.text = GameData.LevelName + "-" + (GameData.UserData.examStage + 1 ) ;

		// ネコアイコン
		for (int i=1; i <= GameData.numGameToRankConfirm ; i++)
		{
			string targetStr = "Stage/sub" + i;
			GameObject targetNode = GameObject.Find(targetStr);
			if (null != targetNode)
			{
				if (i == GameData.UserData.examStage + 1)
				{
					targetNode.GetComponent<Image>().sprite = Resources.Load("UI/07_Battle/Stage_Player_Icon_Red", typeof(Sprite)) as Sprite;

					// PINアイコンを現在位置に移動
					Vector3 NodePosi = targetNode.transform.position;
					Vector3 IconPosi = mapPinIcon.transform.position;
					IconPosi.x = NodePosi.x;
					mapPinIcon.transform.position = IconPosi;

				}
				else if(i < GameData.UserData.examStage + 1) 
				{
					targetNode.GetComponent<Image>().sprite = Resources.Load("UI/07_Battle/Stage_Player_Icon_Blue", typeof(Sprite)) as Sprite;
				}
			}
			else
			{
				//Debug.Log("ないよ " + targetStr);
			}
		}

		// 足跡line
		for (int i = 1; i <= GameData.numGameToRankConfirm; i++)
		{
			string targetStr = "Stage/Line" + i;
			GameObject targetNode = GameObject.Find(targetStr);
			if (null != targetNode)
			{
				if (i > GameData.UserData.examStage + 1)
				{
					targetNode.SetActive(false);
				}
				else if (i < GameData.UserData.examStage + 1)
				{
					targetNode.SetActive(true);
				}
			}
		}




	}


}
