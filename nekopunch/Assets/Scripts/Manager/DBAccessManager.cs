﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// DBアクセス管理クラス
/// </summary>
public class DBAccessManager : SingletonMonoBehaviour<DBAccessManager>
{

    #region 定数

    /// <summary>
    /// サーバーのルートパス
    /// </summary>
    private const string rootPath = "http://sub0000545829.hmk-temp.com/nekopunch/";
	//private const string rootPath = "http://sub0000545829.hmk-temp.com/TEST_nekopunch_w/";


    /// <summary>
    /// ユーザーマスタ用PHP
    /// </summary>
    public static string php_m_user = "m_user.php";

    /// <summary>
    /// データ引き継ぎテーブル用PHP
    /// </summary>
    public static string php_t_datatransfer = "t_datatransfer.php";

    /// <summary>
    /// 課金者テーブル用PHP
    /// </summary>
    public static string php_m_IAPPlayer = "m_IAPPlayer.php";

    /// <summary>
    /// リトライカウント
    /// </summary>
    private const int retryCount = 3;

    #endregion

    #region 変数

    /// <summary>
    /// 起動するPHPのパス
    /// </summary>
    private static string phpPath;

    /// <summary>
    /// WWWForm
    /// </summary>
    private static WWWForm wwwForm;

    /// <summary>
    /// DBアクセス結果
    /// </summary>
    private static string wwwResult;

    /// <summary>
    /// エラー発生フラグ
    /// </summary>
    public bool ErrFlg;

    #endregion


    // Use this for initialization
    public void Start()
    {
        ErrFlg = false;
    }

    #region DBアクセス実行

    /// <summary>
    /// DBアクセス実行
    /// </summary>
    /// <returns></returns>
    private IEnumerator DBAccess()
    {
        //this.transform.FindChild("DBAccessFilter").gameObject.SetActive(true);
        StartCoroutine("AccessWait");

        using (UnityWebRequest www = UnityWebRequest.Post(phpPath, wwwForm))
        {
            yield return www.SendWebRequest();

            if (string.IsNullOrEmpty(www.error))
            {
                // SQL実行結果を返す
                wwwResult = www.downloadHandler.text;
            }
            else
            {
                //Debug.Log(www.error);
				Debug.LogError(www.error);
				//GameObject errorMsg = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
                //errorMsg.GetComponent<MessageBoxManager>().Initialize_OK("通信エラーが発生しました。", Application.Quit);

                //ErrFlg = true;
                // アプリ進行とめないようにしてみる
                ErrFlg = false; 
				yield break;
            }
        }
        //this.transform.FindChild("DBAccessFilter").gameObject.SetActive(false);
        Handheld.StopActivityIndicator();
    }

    private IEnumerator AccessWait()
    {
#if UNITY_IPHONE
		Handheld.SetActivityIndicatorStyle(UnityEngine.iOS.ActivityIndicatorStyle.Gray);
#elif UNITY_ANDROID
        Handheld.SetActivityIndicatorStyle(AndroidActivityIndicatorStyle.Small);
#endif
        Handheld.StartActivityIndicator();
        yield return new WaitForSeconds(0);
    }

    #endregion


    #region ユーザーデータ

    /// <summary>s
    /// ユーザーデータのインサート
    /// </summary>
    /// <param name="userName">入力したユーザー名</param>
    /// <returns></returns>
    public IEnumerator InsertNewUser(string userName)
    {
        Debug.Log("ユーザーマスタ:INSERT");
        phpPath = rootPath + php_m_user;
        wwwForm = new WWWForm();
        wwwForm.AddField("process", "INSERT");
		wwwForm.AddField("userName", userName);
		wwwForm.AddField("os", SystemInfo.operatingSystem);
		wwwForm.AddField("appVersion", Application.version);

		yield return StartCoroutine("DBAccess");

        if (!ErrFlg)
        {
            GameData.UserData.UserId = int.Parse(wwwResult);

            // 登録したユーザーIDはローカルセーブデータに保存
            SaveData.SetInt(SaveKey.UserId, GameData.UserData.UserId);
            SaveData.Save();
        }
    }

    /// <summary>
    /// ユーザーデータを取得する
    /// </summary>
    /// <param name="userId">ユーザーID</param>
    /// <returns></returns>
    public IEnumerator GetUserData(int userId)
    {
        Debug.Log("ユーザーマスタ:SELECT :"+ userId);
        phpPath = rootPath + php_m_user;
        wwwForm = new WWWForm();
        wwwForm.AddField("process", "SELECT");
        wwwForm.AddField("userId", userId);

        yield return StartCoroutine("DBAccess");

        if (!ErrFlg)
        {
            GameData.UserData = Utility.ListFromJson<UserData>(wwwResult)[0];

			// 階層が異なるので別変数からコピー
			CharacterData MyCharacterData = Utility.ListFromJson<CharacterData>(wwwResult)[0];
			GameData.UserData.MyCharacterData.CharacterName = MyCharacterData.CharacterName;
			GameData.UserData.MyCharacterData.HP = MyCharacterData.HP;
			GameData.UserData.MyCharacterData.ATK = MyCharacterData.ATK;
			GameData.UserData.MyCharacterData.DEF = MyCharacterData.DEF;
			GameData.UserData.MyCharacterData.SPD = MyCharacterData.SPD;

		}
	}


	public static bool canLoad = false;
	/// <summary>
	/// 強制ロードフラグを取得する
	/// </summary>
	/// <returns></returns>
	public IEnumerator SelectLoadFlg()
	{
		Debug.Log("ユーザーマスタ:SELECT - LOADFLG");
		phpPath = rootPath + php_m_user;
		wwwForm = new WWWForm();
		wwwForm.AddField("process", "SELECTLOADFLG");
		wwwForm.AddField("userId", GameData.UserData.UserId);

		yield return StartCoroutine("DBAccess");

		canLoad = false;
		if (!ErrFlg)
		{
			if (wwwResult == "1")
			{
				canLoad = true;
			}
			else
			{
				canLoad = false;
			}
		}

	}


	/// <summary>
	/// 強制ロードフラグをクリア状態に更新する
	/// </summary>
	/// <returns></returns>
	public IEnumerator UpdateCleareLoadFlg()
    {
        Debug.Log("ユーザーマスタ:UPDATE - CLEARELOADFLG");
        phpPath = rootPath + php_m_user;
        wwwForm = new WWWForm();
        wwwForm.AddField("process", "CLEARELOADFLG");
        wwwForm.AddField("userId", GameData.UserData.UserId);

        yield return StartCoroutine("DBAccess");
    }

    /// <summary>
    /// ユーザーデータを更新する
    /// </summary>
    /// <returns></returns>
    public IEnumerator UpdateUserData()
    {
        Debug.Log("ユーザーマスタ:UPDATE");
        phpPath = rootPath + php_m_user;
        wwwForm = new WWWForm();
        wwwForm.AddField("process", "UPDATE");
		wwwForm.AddField("userName", GameData.UserData.MyCharacterData.CharacterName);
        wwwForm.AddField("userlevel", GameData.UserData.UserLevel);
        wwwForm.AddField("money", GameData.UserData.Money);
		wwwForm.AddField("star", GameData.UserData.StarCount);
		wwwForm.AddField("hp",  (int)( GameData.UserData.MyCharacterData.HP));
		wwwForm.AddField("atk", (int)(GameData.UserData.MyCharacterData.ATK));
		wwwForm.AddField("def", (int)(GameData.UserData.MyCharacterData.DEF));
		wwwForm.AddField("spd", (int)(GameData.UserData.MyCharacterData.SPD));
		wwwForm.AddField("os", SystemInfo.operatingSystem);
        wwwForm.AddField("appVersion", Application.version);

		wwwForm.AddField("userId", GameData.UserData.UserId);

		yield return StartCoroutine("DBAccess");
        //yield break;
    }

    #endregion




    #region 引継ぎデータ

    /// <summary>
    /// 引継ぎデータインサート
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public IEnumerator InsertTransferData(string id)
    {
        Debug.Log("引継ぎテーブル:INSERT");
        phpPath = rootPath + php_t_datatransfer;
        wwwForm = new WWWForm();
        wwwForm.AddField("process", "INSERT");
        wwwForm.AddField("id", id);
        wwwForm.AddField("userid", GameData.UserData.UserId);
        wwwForm.AddField("username", GameData.UserData.MyCharacterData.CharacterName);

		yield return StartCoroutine("DBAccess");
    }


    public static bool canTransfer;
    /// <summary>
    /// 引継ぎデータ取得
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public IEnumerator GetTransferData(string id)
    {
        Debug.Log("引継ぎテーブル:SELECT");
        phpPath = rootPath + php_t_datatransfer;
        wwwForm = new WWWForm();
        wwwForm.AddField("process", "SELECT");
        wwwForm.AddField("id", id);

        yield return StartCoroutine("DBAccess");

        if (!ErrFlg)
        {
			// ここでDBのデータをローカルに上書きする
			//GameData.UserData = Utility.ListFromJson<UserData>(wwwResult)[0];

			if (wwwResult != "null")
            {
                canTransfer = true;
            }
            else
            {
                canTransfer = false;
            }
        }
    }

    #endregion




    #region 課金者情報の登録
    /// <summary>
    /// ユーザーマスタへのINSERT
    /// </summary>
    /// <param name="userName">入力したユーザー名</param>
    /// <returns></returns>
    public IEnumerator InsertIAPPlayer(string star)
    {
        Debug.Log("課金ユーザー:INSERT");
        phpPath = rootPath + php_m_IAPPlayer;
        wwwForm = new WWWForm();
        wwwForm.AddField("process", "INSERT");
        wwwForm.AddField("userId", GameData.UserData.UserId);
        wwwForm.AddField("userName", GameData.UserData.MyCharacterData.CharacterName);
		//wwwForm.AddField("Juwel", int.Parse(jewel));
		wwwForm.AddField("star", star);
#if UNITY_ANDROID
        wwwForm.AddField("OS", "Google");
#else
            wwwForm.AddField("OS", "Apple");
#endif

        yield return StartCoroutine("DBAccess");

    }
    #endregion
}
