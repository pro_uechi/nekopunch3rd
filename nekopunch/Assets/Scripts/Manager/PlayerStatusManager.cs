﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;
using System;

/// <summary>
/// ユーザー情報管理
/// </summary>
public class PlayerStatusManager : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
        SetStatus();
    }

    /// <summary>
    /// プレイヤー情報をセット
    /// </summary>
    public void SetStatus()
    {
        //Debug.LogError("SetStatus");
        // キャラ名
        //this.transform.Find("PlayerName/NameText").GetComponent<Text>().text = GameData.UserData.UserName;
        // ランク
        this.transform.Find("LevelFrame/Level").GetComponent<Text>().text = GameData.GetRankName(GameData.UserData.UserLevel);

        //// 次のレベルまでの指標  99以上はカンスト状態とする
        //if (GameInfo.UserData.StudyLevel < 99)
        //{
        //    this.transform.Find("ExpBar").GetComponent<Image>().fillAmount = 1 - ((float)(GameInfo.ExpList[GameInfo.UserData.StudyLevel] - GameInfo.UserData.StudyPoint) / ((float)(GameInfo.ExpList[GameInfo.UserData.StudyLevel] - GameInfo.ExpList[GameInfo.UserData.StudyLevel - 1])));
        //}
        //else
        //{
        //    //int ExpWork = 50000 + ((GameInfo.UserData.StudyLevel - 100) * 1000);
        //    this.transform.Find("ExpBar").GetComponent<Image>().fillAmount = 1 - ((float)( (ExpWork - GameInfo.UserData.StudyPoint)) / (GameInfo.UserData.StudyLevel * 10));
        //}
    }

    // Update is called once per frame
    void Update()
    {
        //this.transform.Find("PlayerName").GetComponent<Text>().text = GameData.UserData.UserName;
    }

    public void SetRank()
    {
        //Debug.LogError("SetRank");
        this.transform.Find("LevelFrame/Level").GetComponent<Text>().text = GameData.GetRankName(GameData.UserData.UserLevel);
    }

}
