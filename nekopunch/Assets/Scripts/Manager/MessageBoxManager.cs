﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class MessageBoxManager : MonoBehaviour
{
    /// <summary>
    /// ダイアログオブジェクト
    /// </summary>
    [SerializeField]
    private GameObject dialog;

    [SerializeField]
    private GameObject yesButton;

    [SerializeField]
    private GameObject noButton;

    [SerializeField]
    private GameObject okButton;

    [SerializeField]
    private GameObject closeButton;

    /// <summary>
    /// 「はい」を押下した時に実行するメソッド
    /// </summary>
    public UnityAction yesEvent;

    /// <summary>
    /// 「いいえ」を押下した時に実行するメソッド
    /// </summary>
    public UnityAction noEvent;

    /// <summary>
    /// 「OK」を押下した時に実行するメソッド
    /// </summary>
    public UnityAction okEvent;

    /// <summary>
    /// 「×」を押下した時に実行するメソッド
    /// </summary>
    public UnityAction closeEvent;

    /// <summary>
    /// ボタン押下時に実行する処理
    /// </summary>
    [SerializeField]
    private UnityEvent events;

    /// <summary>
    /// アニメーター
    /// </summary>
    //Animator animator;


    private void Awake()
    {
        //this.dialog = this.gameObject.transform.Find("Message").gameObject;
        //this.animator = this.dialog.GetComponent<Animator>();
    }

    void Start()
    {
        this.events = new UnityEvent();

        yesButton.gameObject.GetComponent<Button>().onClick.AddListener(YesButton_OnClick);
        noButton.gameObject.GetComponent<Button>().onClick.AddListener(NoButton_OnClick);
        okButton.gameObject.GetComponent<Button>().onClick.AddListener(OKButton_OnClick);
        closeButton.gameObject.GetComponent<Button>().onClick.AddListener(CloseButton_OnClick);
    }

    /// <summary>
    /// 初期化処理(はい／いいえ)
    /// </summary>
    /// <param name="message">ダイアログに表示するメッセージ</param>
    /// <param name="yesEvent">「はい」押下時に起動するメソッド</param>
    /// <param name="noEvent">「いいえ」押下時に起動するメソッド</param>
    public void Initialize_YesNo(string message, UnityAction yesEvent, UnityAction noEvent)
    {
        //this.dialog = this.gameObject.transform.Find("Message").gameObject;
        //this.animator = this.dialog.GetComponent<Animator>();

        SetButtonType(1);
        SetMessage(message);

        this.yesEvent = yesEvent;
        this.noEvent = noEvent;
        this.closeEvent = noEvent;
    }

    /// <summary>
    /// 初期化処理(任意の2択)
    /// </summary>
    /// <param name="leftButtonName">左のボタン名</param>
    /// <param name="rightButtonName">右のボタン名ージ</param>
    /// <param name="message">ダイアログに表示するメッセージ</param>
    /// <param name="yesEvent">左のボタン押下時に起動するメソッド</param>
    /// <param name="noEvent">右のボタン押下時に起動するメソッド</param>
    public void Initialize_Select(string leftButtonName, string rightButtonName, string message, UnityAction yesEvent, UnityAction noEvent)
    {
        yesButton.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = leftButtonName;
        noButton.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = rightButtonName;
        Initialize_YesNo(message, yesEvent, noEvent);
    }

    /// <summary>
    /// 初期化処理(OK)
    /// </summary>
    /// <param name="message">ダイアログに表示するメッセージ</param>
    /// <param name="okEvent">「OK」押下時に起動するメソッド</param>
    public void Initialize_OK(string message, UnityAction okEvent)
    {
        SetButtonType(2);
        SetMessage(message);
        this.okEvent = okEvent;
        this.closeEvent = okEvent;
    }

    /// <summary>
    /// 初期化（メッセージだけ表示）
    /// </summary>
    /// <param name="message">ダイアログに表示するメッセージ</param>
    public void Initialize_MessageOnly(string message)
    {
        SetButtonType(3);
        SetMessage(message);
    }

    /// <summary>
    /// ボタンのタイプを設定する
    /// </summary>
    /// <param name="type">タイプ</param>
    private void SetButtonType(int type)
    {
        switch (type)
        {
            case 1:
                // はい／いいえ を表示
                yesButton.gameObject.SetActive(true);
                noButton.gameObject.SetActive(true);
                okButton.gameObject.SetActive(false);
                break;
            case 2:
                // OK を表示
                yesButton.gameObject.SetActive(false);
                noButton.gameObject.SetActive(false);
                okButton.gameObject.SetActive(true);
                break;
            case 3:
                //ボタン全て非表示
                yesButton.gameObject.SetActive(false);
                noButton.gameObject.SetActive(false);
                okButton.gameObject.SetActive(false);
                closeButton.gameObject.SetActive(false);
                break;
        }
    }

    /// <summary>
    /// メッセージをセットする
    /// </summary>
    /// <param name="message">内容</param>
    private void SetMessage(string message)
    {
        this.dialog.transform.Find("Text").gameObject.GetComponent<TextMeshProUGUI>().text = message;
    }

    /// <summary>
    /// 「はい」ボタン押下時
    /// </summary>
    private void YesButton_OnClick()
    {
        if (this.yesEvent != null)
        {
			AudioManager.Instance.PlaySE("OkSe");
            this.events.AddListener(this.yesEvent);
            this.events.Invoke();
        }

        StartCoroutine("CloseWindow");
    }

    /// <summary>
    /// 「いいえ」ボタン押下時
    /// </summary>
    public void NoButton_OnClick()
    {
		AudioManager.Instance.PlaySE("ReturnSE");
		if (this.noEvent != null)
        {
			this.events.AddListener(this.noEvent);
            this.events.Invoke();
        }

        StartCoroutine("CloseWindow");
    }

    /// <summary>
    /// 「OK」ボタン押下時
    /// </summary>
    private void OKButton_OnClick()
    {
        AudioManager.Instance.PlaySE("OkSe");
        if (this.okEvent != null)
        {
            this.events.AddListener(this.okEvent);
            this.events.Invoke();
        }

        StartCoroutine("CloseWindow");
    }

    /// <summary>
    /// 「×」ボタン押下時
    /// </summary>
    private void CloseButton_OnClick()
    {
        AudioManager.Instance.PlaySE("OkSe");
        if (this.closeEvent != null)
        {
            this.events.AddListener(this.closeEvent);
            this.events.Invoke();
        }

        StartCoroutine("CloseWindow");
    }

    /// <summary>
    /// メッセージボックスを閉じる
    /// </summary>
    /// <returns></returns>
    public IEnumerator CloseWindow()
    {
        // アニメーションを逆再生
        //this.animator.SetFloat("Speed", -1);
        //this.animator.Play(Animator.StringToHash("messageBox"), 0, 1.0f);

        yield return new WaitForSeconds(0.1f);

        Destroy(this.gameObject);
    }
}
