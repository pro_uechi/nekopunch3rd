﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class UnityAdsManager : SingletonMonoBehaviour<UnityAdsManager>
{
    /// <summary>
    /// 動画再生タイプ
    /// </summary>
    private AdType adType;

    /// <summary>
    /// 連続再生阻止Flg
    /// </summary>
    private bool showedFlg = false;

    /// <summary>
    /// 初期化
    /// </summary>
    public void Init()
    {
        Debug.Log("UnityAds初期化");

#if UNITY_IOS
        Advertisement.Initialize ("3268136");
#elif UNITY_ANDROID
        Advertisement.Initialize("3268137");
#endif
    }

    /// <summary>
    /// 動画広告表示
    /// </summary>
    /// <param name="type"></param>
    public void ShowAd(AdType type)
    {
        // 広告を表示できるか調べる
        if (CanShowAd())
        {
            Debug.Log("ShowAd()");
            
            adType = type;
            GameObject msgBox = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
            var options = new ShowOptions { resultCallback = HandleShowResult };

            switch (type)
            {
                case AdType.BattleContinue:
                    Destroy(msgBox);
                    Advertisement.Show(options);
                    break;
                case AdType.NextBossBattle:
                    msgBox.GetComponent<MessageBoxManager>().Initialize_OK("次はボスバトルです。", delegate 
                    {
                        Advertisement.Show(options);
                    });
                    break;
                case AdType.BattleGiveUp:
                    Time.timeScale = 0.0f;              // ゲーム時間を止める
                    Destroy(msgBox);
                    Advertisement.Show(options);
                    break;
                case AdType.WaitTimeHome:
                    msgBox.GetComponent<MessageBoxManager>().Initialize_YesNo("広告を見て\n時間を進めますか？", delegate
                    {
                        GameData.IsHomeAbs = true;
                        Advertisement.Show(options);
                        ItemManager.Instance.LeaveHomeInitialize();
                        Destroy(msgBox);
                    }, null);
                    break;
            }
        }
        // 広告を表示できなければ
        else
        {

            switch (type)
            {
                case AdType.BattleContinue:
                    SceneFadeManager.Instance.Load(GameData.Scene_Battle, GameData.FadeSpeed);
                    break;
                case AdType.NextBossBattle:
                    GameObject msgBox = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
                    msgBox.GetComponent<MessageBoxManager>().Initialize_OK("次はボスバトルです。", delegate
                    {
                        SceneFadeManager.Instance.Load(GameData.Scene_Battle, GameData.FadeSpeed);
                    });
                    break;
                case AdType.BattleGiveUp:
                    SceneFadeManager.Instance.Load(GameData.Scene_Home, GameData.FadeSpeed);
                    break;
            }
        }
    }

    /// <summary>
    /// 広告表示できるかどうか
    /// </summary>
    /// <returns></returns>
    public bool CanShowAd()
    {
        return Advertisement.IsReady() && !showedFlg;
    }

    /// <summary>
    /// 動画広告結果
    /// </summary>
    /// <param name="result"></param>
    private void HandleShowResult(ShowResult result)
    {
        showedFlg = true;
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                // 報酬を分岐させる
                switch (adType)
                {
                    case AdType.BattleContinue:
                        SceneFadeManager.Instance.Load(GameData.Scene_Battle, GameData.FadeSpeed);
                        break;
                    case AdType.NextBossBattle:
                        SceneFadeManager.Instance.Load(GameData.Scene_Battle, GameData.FadeSpeed);
                        break;
                    case AdType.BattleGiveUp:
                        Time.timeScale = 1.0f;          // ゲーム時間を元に戻す
                        SceneFadeManager.Instance.Load(GameData.Scene_Home, GameData.FadeSpeed);
                        break;
                    case AdType.WaitTimeHome:
                        // 待ち時間を5分とばす
                        ItemManager.Instance.StartAnimation(300);
                        GameData.IsHomeAbs = false;
                        break;
                  
                }
                break;

            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                switch (adType)
                {
                    case AdType.BattleContinue:
                        GameData.UserData.battleCount = 0;
                        SceneFadeManager.Instance.Load(GameData.Scene_Battle, GameData.FadeSpeed);
                        break;
                    case AdType.NextBossBattle:
                        SceneFadeManager.Instance.Load(GameData.Scene_Battle, GameData.FadeSpeed);
                        break;
                    case AdType.BattleGiveUp:
                        Time.timeScale = 1.0f;          // ゲーム時間を元に戻す
                        SceneFadeManager.Instance.Load(GameData.Scene_Home, GameData.FadeSpeed);
                        break;
                    case AdType.WaitTimeHome:
                        GameObject msgBox3 = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
                        msgBox3.GetComponent<MessageBoxManager>().Initialize_OK("広告を最後まで見ると\n時間を短縮できるよ！", delegate
                        {
                            ItemManager.Instance.StartAnimation(1);
                            GameData.IsHomeAbs = false;
                            Destroy(msgBox3);
                        }
                        );
                        break;
                }
                break;

            case ShowResult.Failed:
                Time.timeScale = 1.0f;          // ゲーム時間を元に戻す
                Debug.LogError("The ad failed to be shown.");
                break;
        }
        showedFlg = false;
        Debug.Log("結果発表");
    }
}
