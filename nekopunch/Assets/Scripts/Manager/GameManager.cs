﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AutoyaFramework;
using System;
using TMPro;
using DG.Tweening;

enum ExamResult
{
    None,
    Success,
    Fail,
    Max
}

public class GameManager : SingletonMonoBehaviour<GameManager>
{
    #region 変数

    /// <summary>
    /// 基本ポイント
    /// </summary>
    public const int basicPoint_Win = 50;


    /// <summary>
    /// レベルボーナス ポイント
    /// </summary>                                         10  9  8  7  6  5  4  3  2  1   1   2   3   4   5   6   7   8   9   10  
    private static int[] levelBonus = new int[] { 10, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000 };

    private static int[] levelCoinBonus = new int[] { 60, 120, 240, 480, 960, 1220, 1500, 2000};

    /// <summary>
    /// 勝利するためのラウンド数
    /// </summary>
    private const int numRoundsToWin = 2;

    /// <summary>
    /// HUM
    /// </summary>
    private Player hum = null;

    /// <summary>
    /// CPU
    /// </summary>
    [SerializeField]
    private Player cpu = null;

    /// <summary>
    /// Playerクラスの配列
    /// </summary>
    private Player[] players = new Player[2];

    /// <summary>
    /// ゲーム勝者
    /// </summary>
    public static Player gameWinner = null;

    /// <summary>
    /// ゲーム敗者
    /// </summary>
    public static Player gameLoser = null;

    /// <summary>
    /// ラウンド勝者
    /// </summary>
    private Player roundWinner = null;

    /// <summary>
    /// ラウンド敗者
    /// </summary>
    private Player roundLoser = null;

    /// <summary>
    /// 現在のラウンド数
    /// </summary>
    private int roundNum = 0;

    /// <summary>
    /// ラウンド開始アニメーションオブジェクト
    /// </summary>
    [SerializeField]
    private GameObject roundText = null;

    /// <summary>
    /// ファイトテキスト
    /// </summary>
    [SerializeField]
    private GameObject fightText = null;

    /// <summary>
    /// ゲームタイマー
    /// </summary>
    [SerializeField]
    private Timer timer = null;

    /// <summary>
    /// プレイヤ HP 
    /// </summary>
    [SerializeField]
    private GameObject Playerslider = null;

    /// <summary>
    /// CPU  HP 
    /// </summary>
    [SerializeField]
    private GameObject cpuslider = null;

    /// <summary>
    /// プレイヤ SP
    /// </summary>
    [SerializeField]
    private GameObject PlayerSPslider = null;

    /// <summary>
    /// CPU  SP
    /// </summary>
    [SerializeField]
    private GameObject cpuSPslider = null;


    /// <summary>
    /// ログディスプレイ
    /// </summary>
    [SerializeField]
    private LogDisplay logDisp = null;

    /// <summary>
    /// ポイントに加算する時間
    /// </summary>
    private int winTime = 0;

    /// <summary>
    /// ラウンド終了後のディレイ
    /// </summary>
    public float endDelay = 1.0f;

    /// <summary>
    /// マッチングUIオブジェクト
    /// </summary>
    [SerializeField]
    private GameObject matchingUI = null;

    /// <summary>
    /// バトルUIオブジェクト
    /// </summary>
    [SerializeField]
    private GameObject battleUI = null;

    /// <summary>
    /// HPSlider UIオブジェクト
    /// </summary>
    [SerializeField]
    private GameObject HPSliderUI = null;

    // CPUのメッシュオブジェクト
    [SerializeField]
    private GameObject cpuMesh = null;

    /// <summary>
    /// バトル終了後に表示するオブジェクト
    /// </summary>
    [SerializeField]
    private GameObject Result = null;

    /// <summary>
    /// 勝利時のスターオブジェクト
    /// </summary>
    private GameObject[,] starObject = new GameObject[2, numRoundsToWin];

    /// <summary>
    /// 一時停止ボタン
    /// </summary>
    [SerializeField]
    private Button PauseButton = null;

    /// <summary>
    /// 攻撃ボタン
    /// </summary>
    [SerializeField]
    private Button AttackButton = null;

    /// <summary>
    /// 回避ボタン
    /// </summary>
    [SerializeField]
    private Button AvoidanceButton = null;

    /// <summary>
    /// 必殺技ボタン
    /// </summary>
    [SerializeField]
    private Button SpecialButton = null;


    [SerializeField]
    private GameObject SpecialButtonEffect = null;

    /// <summary>
    /// 必殺技ボタン　CPU
    /// </summary>
    [SerializeField]
    private Button CPUSpecialButton = null;



    /// <summary>
    /// HUMの名前
    /// </summary>
    [SerializeField]
    private Text HUMName = null;

    /// <summary>
    /// CPUの名前
    /// </summary>
    [SerializeField]
    private Text CPUName = null;


    /// <summary>
    /// SE文字列
    /// </summary>
    public static string resultSE = string.Empty;

    /// <summary>
    /// 敗北時エフェクト
    /// </summary>
    public static GameObject loseEffect = null;

    /// <summary>
    /// サブステージ全戦終了フラグ
    /// </summary>
    private bool examFinished = false;

    /// <summary>
    /// ステージ全戦の結果
    /// </summary>
    private ExamResult examResult = ExamResult.None;

    /// <summary>
    /// コンティニューを確認するメッセージボックス
    /// </summary>
    [SerializeField]
    private GameObject continueMessageBox = null;

    /// <summary>
    /// 一時停止を確認するメッセージボックス
    /// </summary>
    [SerializeField]
    private GameObject PauseMessageBox = null;



    /// <summary>
    /// サブステージ途中で負けた時のメッセージボックス
    /// </summary>
    [SerializeField]
    private GameObject loseMessageBox = null;

    private int tmpBattleCount = 0;
    private int tmpWin_exam = 0;

    public static int moneyIncrement = 0;


    /// <summary>
    /// 背景画像
    /// </summary>
    [SerializeField]
    private Image background = null;

    /// <summary>
    /// 対戦相手をランダムで取得するかどうか
    /// </summary>
    //public static bool getRandom = true;

    #endregion

    IEnumerator Start()
    {
        {
            // ランダム対戦相手取得
            //       yield return StartCoroutine(DBAccessManager.Instance.GetRandomPare());
            //       if (DBAccessManager.Instance.ErrFlg)
            //       {
            //yield break;
            //       }
        }

        // HUMの衣装を設定
        //yield return StartCoroutine(EquipManager.Instance.InitEquipData());

        // CPUの衣装を設定
        //yield return StartCoroutine(EquipManager.Instance.SetEntryDataEquip(cpuMesh, GameData.EntryData_Enemy));
        //yield return StartCoroutine(EquipManager.Instance.SetEntryDataEquip(cpuMesh));

        //// プレイヤー生成
        //string CatPrefabIDStr = "Prefabs/cat" + string.Format("{0:00}", SaveData.GetInt(SaveKey.UserCharacter));
        //GameObject pObject = Instantiate((GameObject)Resources.Load(CatPrefabIDStr), new Vector3(0.0f, 0.0f, 0.0f), Quaternion.Euler(0, 180, 0));
        //DontDestroyOnLoad(pObject);
        //GameData.PlayerObjct = pObject;
        //GameData.PlayerMeshObject = pObject.transform.Find("mesh").gameObject;
        //GameData.PlayerObjct.name = "cat";
        //GameData.SetPlayerActive(true);
        // CPU情報
        GameData.CpuTableNo = ((GameData.Levelrank - 1) * GameData.numGameToRankConfirm) + GameData.UserData.examStage;
        int stagecount = GameData.BattleData.CpuCatID[GameData.CpuTableNo];
        string CpuCatID = "Prefabs/CPU" + string.Format("{0:00}", stagecount);
        // CPU生成
        GameObject cpuobj = Instantiate((GameObject)Resources.Load(CpuCatID), new Vector3(0.0f, 0.0f, 0.0f), Quaternion.Euler(0, 0, 0));
        GameData.CpuObjct = cpuobj;
        Player playerObject = cpuobj.GetComponent<Player>();
        cpu = playerObject;
        cpu.name = "CPU";




        // HUMの参照を取得して各値を代入
        hum = GameData.PlayerObjct.GetComponent<Player>();


        // CPUの強さを設定
        //GameObject.Find("CPU").GetComponent<CPU>().SetActionRate();
        cpu.GetComponent<CPU>().SetActionRate();


        hum.wins = 0;
        cpu.wins = 0;

        // 敵を設定
        hum.enemy = cpu;
        cpu.enemy = hum;

        // エフェクトの位置を調整
        hum.Effectpos = new Vector3(0, 0.0f, 0.0f);
        cpu.Effectpos = new Vector3(10.0f, 0.0f, 0.0f);

        // ボタンを設定
        AttackButton.onClick.AddListener(hum.Attack);
        AvoidanceButton.onClick.AddListener(hum.Avoidance);
        SpecialButton.onClick.AddListener(hum.Special);

        SpecialButtonEffect.SetActive(false);
        SpecialButton.gameObject.SetActive(false);
        CPUSpecialButton.gameObject.SetActive(false);
        
        cpuSPslider.SetActive(true);
        PlayerSPslider.SetActive(true);
        PauseButton.onClick.AddListener(onClickPauseButton);


        // Playerクラスの配列に代入
        players[0] = hum;
        players[1] = cpu;

        //ラウンド勝利数に応じて表示されるオブジェクト
        starObject[0, 0] = GameObject.Find("Canvas_UI/SafeArea/BattleUI/RoundStarPlayer/star1");
        starObject[0, 1] = GameObject.Find("Canvas_UI/SafeArea/BattleUI/RoundStarPlayer/star2");
        starObject[1, 0] = GameObject.Find("Canvas_UI/SafeArea/BattleUI/RoundStarCPU/star1");
        starObject[1, 1] = GameObject.Find("Canvas_UI/SafeArea/BattleUI/RoundStarCPU/star2");

        // マッチングの情報を初期化
        InitMatch();

        // ポイント増減をリセット
        GameData.PointIncrement = 0;

        /// <summary>
        /// レベルに合わせて背景を変える
        /// </summary>
        background.sprite = Resources.Load("UI/07_Battle/Background/Background_" + String.Format("{0:00}", GameData.Levelrank/* + 1*/), typeof(Sprite)) as Sprite;
        // ゲーム開始); 
        StartCoroutine(GameLoop());
        yield return null;

    }

    /// <summary>
    /// マッチングの情報初期化
    /// </summary>
    private void InitMatch()
    {
        // ユーザー名
        HUMName.text = GameData.UserData.MyCharacterData.CharacterName;
        CPUName.text = GameData.BattleData.CPUnameList[GameData.CpuTableNo];


        // ユーザー名
        logDisp.Name[AvatarNumberConstants.PLAYER].text = GameData.UserData.MyCharacterData.CharacterName;
        logDisp.Name[AvatarNumberConstants.CPU].text = GameData.BattleData.CPUnameList[GameData.CpuTableNo];


        // バトルUIを非表示
        battleUI.SetActive(false);
        // マッチングUIを表示
        matchingUI.SetActive(true);

    }


    // ゲームループ-----------------------------------------------------------------------------------------
    private IEnumerator GameLoop()
    {
        yield return StartCoroutine(Macthing());
        yield return StartCoroutine(RoundStarting());
        yield return StartCoroutine(RoundPlaying());
        yield return StartCoroutine(RoundEnding());

        // ゲーム引き分けの場合
        if (hum.wins == numRoundsToWin && cpu.wins == numRoundsToWin)
        {
            GameFnishPopup(false);
        }
        // ゲームの勝者がいる場合
        else if (gameWinner != null)
        {
            if (gameWinner == hum)
            {
                GameFnishPopup(true);
            }
            else
            {
                GameFnishPopup(false);
            }
        }
        else
        {
            StartCoroutine(GameLoop());
        }
    }

    private IEnumerator Macthing()
    {
        //// エフェクトの位置を調整　
        //hum.Effectpos = new Vector3(0, 0.0f, 0.0f);
        //cpu.Effectpos = new Vector3(10.0f, 0.0f, 0.0f);



        // バトルの最初だけマッチングの演出を行う
        if (roundNum == 0)
        {
            /// <summary>
            /// <変更>敵のアニメーションが流れないのでコメントアウト
            /// </summary>
            // プレイヤーをリセット 
            //ResetPlayers();

            AudioManager.Instance.StopSE();
            // プレイヤーのアニメーター変更
            players[0].SetAnimatorController(AnimatorControllerType.Standing);
            // コンティニューメッセージボックスを非表示
            continueMessageBox.SetActive(false);
            loseMessageBox.SetActive(false);

            // CPUの大きさを設定
            cpu.transform.localScale = new Vector3(1.8f, 1.8f, 1.8f);

            yield return new WaitForSeconds(4.5f);
        }

        yield break;
    }

    private IEnumerator RoundStarting()
    {
        // プレイヤーをリセット
        ResetPlayers();
        // スライダーに値を反映
        CheckSlider();

        // プレイヤーの大きさを設定
        GameData.SetPlayerScale(new Vector3(0.9f, 0.9f, 0.9f));
        GameData.SetPlayerPosition(new Vector3(3.8f, 1.8f, 0.0f));

        // CPUの大きさを設定
        cpu.transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);
        cpu.transform.position = new Vector3(-4.1f, 1.8f, 0.0f);

        // 背景、UIを入れ替え
        matchingUI.SetActive(false);
        battleUI.SetActive(true);
        HPSliderUI.SetActive(true);

        logDisp.GetComponent<LogDisplay>().Win.SetActive(false);
        logDisp.GetComponent<LogDisplay>().Lose.SetActive(false);
        logDisp.GetComponent<LogDisplay>().Draw.SetActive(false);

        SpecialButton.gameObject.SetActive(false);
        CPUSpecialButton.gameObject.SetActive(false);
        SpecialButtonEffect.SetActive(false);
        cpuSPslider.SetActive(true);
        PlayerSPslider.SetActive(true);

        // ラウンド数を増加
        ++roundNum;

        // 開始アニメーション
        yield return StartCoroutine(StartAnim());
    }

    private IEnumerator RoundPlaying()
    {
        // ゲームタイマーをリセット
        timer.ResetTime();

        // ゲームタイマースタート
        timer.GameStart();

        // 操作可能にする
        for (int i = 0; i < players.Length; ++i)
        {
            players[i].SetAnimatorController(AnimatorControllerType.Battle);
            players[i].action = true;
        }

        // 勝敗が決定していない、もしくは時間切れになっていない場合ループ
        while (!OneCharacterLeft() && !timer.TimeOut())
        {
            yield return null;
        }
    }

    private IEnumerator RoundEnding()
    {
        // 前のラウンドの勝者、敗者をnullにする
        roundWinner = null;
        roundLoser = null;
        gameWinner = null;

        // タイマーをとめる
        timer.Stop();

        // 操作不可能にする
        for (int i = 0; i < players.Length; ++i)
        {
            players[i].action = false;
        }

        // 時間切れで無い場合
        if (!timer.TimeOut())
        {
            roundWinner = GetRoundWinner();

            //相打ちの場合
            if (roundWinner == null)
            {
                RoundDraw();
            }
        }
        //時間切れ
        else
        {
            roundWinner = GetRoundWinner();

            //相打ちの場合
            if (roundWinner == null)
            {
                RoundDraw();
            }
        }

        roundWinner = GetRoundWinner();
        roundLoser = GetRoundLoser();


        if (roundWinner != null && roundLoser != null)
        {
            // スコアを増加
            roundWinner.wins++;
        }

        // ゲームの勝者がいるかを確認
        gameWinner = GetGameWinner();

        // ユーザー情報の更新は決着がついた時点で行わないと故意にアプリを切断した場合結果が無効にできてしまう。
        if (roundWinner == hum)
        {
            winTime += timer.GetTime();
        }

        UpdateData();

        if (roundWinner != null && roundLoser != null)
        {
            // 勝者のアニメーターを変更
            //roundWinner.SetAnimatorController(AnimatorControllerType.Standing);

            // コルーチンを中断させる
            roundWinner.isInterrupt = true;
            roundLoser.isInterrupt = true;
            roundWinner.animator.speed = 1.0f;
            roundLoser.animator.speed = 1.0f;
            roundWinner.animator.SetTrigger("Win");
            yield return null;
            // 負けアニメーション
            roundLoser.animator.SetTrigger("Lose");

            // 敗北アニメーションの終了を待機
            //while (!roundLoser.FinishedLoseAnim())
            //        {
            //yield return new WaitForSeconds(1.0f);
            //yield return null;
            //        }

            // 敗者のアニメーターを変更
            //roundLoser.SetAnimatorController(AnimatorControllerType.Standing);

            yield return new WaitForSeconds(0.2f);

            // エフェクトリセット
            if (loseEffect != null)
            {
                Destroy(loseEffect);
                loseEffect = null;
            }


            // 勝ち負け表示
            if (roundWinner == hum)
            {
                // SE
                AudioManager.Instance.PlaySE("RoundWin");

                logDisp.Win.SetActive(true);
                logDisp.Lose.SetActive(false);
                logDisp.Draw.SetActive(false);
            }
            else
            {
                // SE
                AudioManager.Instance.PlaySE("RoundLose");

                logDisp.Win.SetActive(false);
                logDisp.Lose.SetActive(true);
                logDisp.Draw.SetActive(false);
            }
        }

        // スターアニメーション
        StarAnim();

        // 1秒待ってからリザルトテキストを非表示
        yield return new WaitForSeconds(1.0f);

        logDisp.GetComponent<LogDisplay>().Win.SetActive(false);
        logDisp.GetComponent<LogDisplay>().Lose.SetActive(false);
        logDisp.GetComponent<LogDisplay>().Draw.SetActive(false);

        // 指定した時間を待機
        if (gameWinner == null)
        {
            yield return new WaitForSeconds(endDelay);
        }
        else
        {
            yield break;
        }
    }
    // -----------------------------------------------------------------------------------------------------------


    /// <summary>
    /// ラウンド開始アニメーション
    /// </summary>
    /// <returns></returns>
    private IEnumerator StartAnim()
    {
        roundText.SetActive(true);
        fightText.SetActive(true);

        //TextMeshProUGUI TextNotation = roundText.GetComponent<TextMeshProUGUI>();
        //TextNotation.text = "Round" + roundNum.ToString();
        roundText.GetComponent<Image>().sprite = Resources.Load("UI/07_Battle/round" + String.Format("{0:0}", roundNum), typeof(Sprite)) as Sprite;

        //ラウンド表示のアニメーションが終わるのを待つ（+1fはディレイ）
        yield return new WaitForSeconds(RoundAnimation.AnimTime * 2 + RoundAnimation.waitTime);

        roundText.SetActive(false);
        fightText.SetActive(false);
    }

    /// <summary>
    /// ゲームが終了したときに適切なポップアップを表示
    /// </summary>
    private void GameFnishPopup(bool win)
    {
        AudioManager.Instance.StopBGM();
        // BGMをとめる
        AudioManager.Instance.PlaySE("BattleEnd");

        if (win)
        {
            //  結果を記録　
            GameData.UserData.examStage++;

            int level = GameData.UserData.UserLevel;
            // 一定のゲーム数に到達したらランクを確認
            if (GameData.UserData.examStage >= GameData.numGameToRankConfirm)
            {
                // バトルUIを非表示
                //battleUI.SetActive(false);
                // クリアレベルを上げ
                if(GameData.UserData.UserLevel < GameData.MaxLevelRank && GameData.UserData.UserLevel + 1 == GameData.Levelrank)
                {
                    GameData.UserData.UserLevel++;
                }

                // 途中SAVE			
                SaveData.SetInt(SaveKey.examLevel, GameData.Levelrank);
                SaveData.SetInt(SaveKey.examStage, GameData.UserData.examStage);
                //　クリアレベルを保存
                SaveData.SetInt(SaveKey.clearLevel, GameData.UserData.UserLevel);
                SaveData.Save();
                // ユーザーデータ更新
                StartCoroutine(DBAccessManager.Instance.UpdateUserData());

                // ポップアップ表示
                GameObject msgBox = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));

                //　最後のレベルのラスボスを倒したならエンディングシーンへ
                if (GameData.Levelrank == GameData.MaxLevelRank)
                {
                    msgBox.GetComponent<MessageBoxManager>().Initialize_OK(
                    GameData.LevelName + "-" + (GameData.UserData.examStage) + "クリアー!\n"
                        + (moneyIncrement) + "コインゲット！"
                        ,
                    // データを初期化
                    delegate 
                    {
                        AudioManager.Instance.StopBGM();
                        AudioManager.Instance.StopSE();
                        InitializeData();
                        // ResetPlayers();
                        Destroy(msgBox);

                        // レベル初クリア時に新しいネコが解放されたことを伝える
                        if (level < GameData.Levelrank)
                        {
                            // ポップアップ表示
                            GameObject msgBox1 = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
                            msgBox1.GetComponent<MessageBoxManager>().Initialize_OK("新しいネコが解放されました！", delegate
                            {
                                SceneFadeManager.Instance.Load(GameData.Scene_Ending, GameData.FadeSpeed);
                                Destroy(msgBox1);
                            });
                        }
                        else
                        {
                            SceneFadeManager.Instance.Load(GameData.Scene_Ending, GameData.FadeSpeed);
                        }
                    }
                    );
                }
                else
                {
                    msgBox.GetComponent<MessageBoxManager>().Initialize_OK(
                    GameData.LevelName + "-" + (GameData.UserData.examStage) + "クリアー!\n"
                     + (moneyIncrement) + "コインゲット！"
                     ,
                    // データを初期化
                    delegate 
                    {
                        AudioManager.Instance.StopBGM();
                        AudioManager.Instance.StopSE();
                        InitializeData();
                        // ResetPlayers();
                        Destroy(msgBox);

                        // レベル初クリア時に新しいネコが解放されたことを伝える
                        if (level < GameData.Levelrank)
                        {
                            // ポップアップ表示
                            GameObject msgBox2 = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
                            msgBox2.GetComponent<MessageBoxManager>().Initialize_OK("新しいネコが解放されました！", delegate
                            {
                                SceneFadeManager.Instance.Load(GameData.Scene_Home, GameData.FadeSpeed);
                                Destroy(msgBox2);
                            });
                                
                        }
                        else
                        {
                            SceneFadeManager.Instance.Load(GameData.Scene_Home, GameData.FadeSpeed);
                        }
                    }
                    );
                }
            }
            else
            {
                // バトルUIを非表示
                //battleUI.SetActive(false);

                // 途中SAVE			
                SaveData.SetInt(SaveKey.examLevel, GameData.Levelrank);
                SaveData.SetInt(SaveKey.examStage, GameData.UserData.examStage);
                SaveData.Save();

                // ポップアップ表示
                GameObject msgBox = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
                msgBox.GetComponent<MessageBoxManager>().Initialize_OK(
                GameData.LevelName + "-" + (GameData.UserData.examStage) + "クリアー!\n"
                 + (moneyIncrement) + "コインゲット！"
                 ,
                // 次のバトルシーンへ移動
                delegate 
                {
                    AudioManager.Instance.StopBGM();
                    AudioManager.Instance.StopSE();
                    Destroy(msgBox);

                    // 次の敵がボスなら、その前に広告を表示
                    if (GameData.UserData.examStage == 4)
                    {
                        UnityAdsManager.Instance.ShowAd(AdType.NextBossBattle);
                    }
                    else
                    {
                        SceneFadeManager.Instance.Load(GameData.Scene_Battle, GameData.FadeSpeed);
                    }
                }
                );
            }
        }
        else
        {
            // コンティニューメッセージボックスを表示
            continueMessageBox.SetActive(true);
        }

    }

    /// <summary>
    /// データ更新
    /// </summary>
    private void UpdateData()
    {
        // ゲーム引き分けの場合
        if (hum.wins == numRoundsToWin && cpu.wins == numRoundsToWin)
        {
            GameDraw();
        }
        // ゲームの勝者がいるか確認
        else if (gameWinner != null)
        {
            if (gameWinner == hum)
            {
                HUMGameWin();
            }
            else if (gameWinner == cpu)
            {
                CPUGameWin();
            }

        }
        // 結果をローカルにセーブ
        SaveData.Save();
    }

    /// <summary>
    /// HUMが勝利した場合
    /// </summary>
    private void HUMGameWin()
    {
        //AudioManager.Instance.StopBGM();
        //resultSE = "BattleWinSe";

        //// 0レベル始まりになるように調整
        //int level = GameData.UserData.UserLevel;
        //if (level > 20)
        //{
        //    level = 20;
        //}
        //// ポイント計算
        //moneyIncrement = basicPoint_Win + levelBonus[level];
        //GameData.UserData.Money += moneyIncrement;

        int level = GameData.Levelrank - 1;

        moneyIncrement = levelCoinBonus[level];
        GameData.UserData.Money += moneyIncrement;

        SaveData.SetInt(SaveKey.moneyData, GameData.UserData.Money);
        SaveData.Save();
        // ユーザーデータ更新
        StartCoroutine(DBAccessManager.Instance.UpdateUserData());
        GameData.UserData.battleCount++;
    }

    /// <summary>
    /// CPUが勝利した場合
    /// </summary>
    private void CPUGameWin()
    {
        //AudioManager.Instance.StopBGM();
        //resultSE = "BattleLoseSe";

        // バトルモードで結果を分岐
        GameData.UserData.battleCount++;
    }

    /// <summary>
    /// 引き分け処理(ゲーム)
    /// </summary>
    private void GameDraw()
    {
        // 結果
        GameData.UserData.battleCount++;
    }

    /// <summary>
    /// コンティニュー
    /// </summary>
    public void Continue()
    {
        // チケット消費
        //GameData.UserData.MyItemData.PossessionItemList_Ticket[GameData.continueTicketID - 1]--;
        //StartCoroutine(DBAccessManager.Instance.UpdateMyItemData());

        // 結果をなかったことにする
        //GameData.UserData.examResult.RemoveAt(GameData.UserData.examResult.Count - 1);
        //SaveData.SetList<int>(SaveKey.ExamResult, GameData.UserData.examResult);
        // SaveData.Save();
        AudioManager.Instance.StopSE();
        AudioManager.Instance.StopBGM();
        AudioManager.Instance.PlaySE("OkSe");
        
        // 広告を表示
        UnityAdsManager.Instance.ShowAd(AdType.BattleContinue);

        //　プレイヤー情報初期化
        //ResetPlayers();
        // シーン読み込み
        //SceneFadeManager.Instance.Load(GameData.Scene_Battle, GameData.FadeSpeed);
    }

    /// <summary>
    /// あきらめる
    /// </summary>
    public void GiveUp()
    {

        // 挑戦レベルが4以下ならメッセージを表示
        if(GameData.Levelrank <= 4)
        {
            AudioManager.Instance.PlaySE("OkSe");
            GameObject msgBox = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));

            msgBox.GetComponent<MessageBoxManager>().Initialize_OK("攻略のヒント\nSHOPでアイテムを購入してみよう！", delegate
            {
                SceneFadeManager.Instance.Load(GameData.Scene_Home, GameData.FadeSpeed);
                AudioManager.Instance.StopSE();
                AudioManager.Instance.PlaySE("OkSe");
                // ステージデータ初期化
                InitializeData();
                //ResetPlayers();
                // コンティニューメッセージボックス非表示
                continueMessageBox.SetActive(false);
            });
        }
        // それ以外なら
        else
        {
            SceneFadeManager.Instance.Load(GameData.Scene_Home, GameData.FadeSpeed);
            AudioManager.Instance.StopSE();
            AudioManager.Instance.PlaySE("OkSe");
            // ステージデータ初期化
            InitializeData();
            //ResetPlayers();
            // コンティニューメッセージボックス非表示
            continueMessageBox.SetActive(false);
        }
    }

    /// <summary>
    /// 引き分け処理(ラウンド)
    /// </summary>
    private void RoundDraw()
    {
        logDisp.Win.SetActive(false);
        logDisp.Lose.SetActive(false);
        logDisp.Draw.SetActive(true);
        for (int i = 0; i < players.Length; ++i)
        {
            ++players[i].wins;
            players[i].SetAnimatorController(AnimatorControllerType.Standing);
        }





        // SE
        //AudioManager.Instance.PlaySE("RoundLose");
        //if (hum.wins != numRoundsToWin && cpu.wins != numRoundsToWin)
        //{
        //    AudioManager.Instance.PlaySE("RoundLose");
        //}
        winTime += timer.GetTime();

        StarAnim();
    }

    /// <summary>
    /// スターアニメーション
    /// </summary>
    private void StarAnim()
    {
        // スターアニメーション(ラウンドを取った時点でトリガーをオンにしたほうがいいかも)
        for (int i = 0; i < hum.wins; i++)
        {
            starObject[0, i].GetComponent<Animator>().SetTrigger("GetRound");
        }
        for (int i = 0; i < cpu.wins; i++)
        {
            starObject[1, i].GetComponent<Animator>().SetTrigger("GetRound");
        }
    }

    /// <summary>
    /// ラウンド終了判定
    /// </summary>
    /// <returns></returns>
    private bool OneCharacterLeft()
    {
        int numCharacterLeft = 0;
        // 値をスライダーに反映
        CheckSlider();

        // 必殺技
        if (hum.specialgauge >= 100)
        {
            if(!SpecialButton.gameObject.activeSelf)
            {
                PlayerSPslider.SetActive(false);
                AudioManager.Instance.PlaySE("SpGaugeMax");
                SpecialButton.gameObject.SetActive(true);
                SpecialButtonEffect.SetActive(true);
            }
        }
        else
        {
            PlayerSPslider.SetActive(true);
            SpecialButton.gameObject.SetActive(false);
            SpecialButtonEffect.SetActive(false);
        }

        // CPU  必殺技
        if (cpu.specialgauge >= 100)
        {
            if (!CPUSpecialButton.gameObject.activeSelf)
            {
                cpuSPslider.SetActive(false);
                CPUSpecialButton.gameObject.SetActive(true);
            }
        }
        else
        {
            cpuSPslider.SetActive(true);
            CPUSpecialButton.gameObject.SetActive(false);
        }


        // 自分ボタン
        if (players[0].action == true)
        {
            AttackButton.GetComponent<Button>().interactable = true;
            AvoidanceButton.GetComponent<Button>().interactable = true;
            SpecialButton.GetComponent<Button>().interactable = true;

        }
        else
        {
            AttackButton.GetComponent<Button>().interactable = false;
            AvoidanceButton.GetComponent<Button>().interactable = false;
            SpecialButton.GetComponent<Button>().interactable = false;
        }


        for (int i = 0; i < players.Length; i++)
        {
            if (players[i].hp > 0)
            {
                ++numCharacterLeft;
            }
        }

        return numCharacterLeft <= 1;
    }

    /// <summary>
    /// ラウンドの勝者がいるかを確認
    /// </summary>
    /// <returns></returns>
    private Player GetRoundWinner()
    {
        //for (int i = 0; i < players.Length; i++)
        //{
        //    if (players[i].hp > 0)
        //    {
        //        return players[i];
        //    }
        //}

        Slider pSlider = Playerslider.GetComponent<Slider>();
        Slider cSlider = cpuslider.GetComponent<Slider>();


        //      if (players[0].hp > players[1].hp)
        //{
        //	return players[0];
        //}
        //else if(players[0].hp < players[1].hp)
        //{
        //	return players[1];
        //}

        /// <summary>
        /// ↑変更　スライダーの割合に応じて勝者を決める
        /// </summary>
        if (pSlider.value > cSlider.value)
        {
            return players[0];
        }
        else if (pSlider.value < cSlider.value)
        {
            return players[1];
        }

        return null;
    }

    /// <summary>
    /// ラウンドの敗者がいるかを確認
    /// </summary>
    /// <returns></returns>
    private Player GetRoundLoser()
    {

        Slider pSlider = Playerslider.GetComponent<Slider>();
        Slider cSlider = cpuslider.GetComponent<Slider>();

        /// <summary>
        /// ↑変更　スライダーの割合に応じて敗者を決める
        /// </summary>
        if (pSlider.value > cSlider.value)
        {
            return players[1];
        }
        else if (pSlider.value < cSlider.value)
        {
            return players[0];
        }

        return null;
    }

    /// <summary>
    /// ゲームの勝者がいるかを確認
    /// </summary>
    /// <returns></returns>
    private Player GetGameWinner()
    {
        for (int i = 0; i < players.Length; i++)
        {
            if (players[i].wins == numRoundsToWin)
            {
                return players[i];
            }
        }

        return null;
    }


    /// <summary>
    /// すべてのプレイヤーをリセットする
    /// </summary>
    public void ResetPlayers()
    {
        for (int i = 0; i < players.Length; ++i)
        {
            players[i].ResetPlayer();
        }
    }


    /// <summary>
    /// humが勝利したかどうか
    /// </summary>
    /// <returns></returns>
    public bool IsWin()
    {
        if (gameWinner == hum && !(hum.wins == numRoundsToWin && cpu.wins == numRoundsToWin))
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    /// <summary>
    /// 一時停止ボタン
    /// </summary>
    /// <returns></returns>
    public void onClickPauseButton()
    {
        AudioManager.Instance.PlaySE("OkSe");
        // Unity時間停止
        Time.timeScale = 0f;

        // コンティニューメッセージボックスを表示
        PauseMessageBox.SetActive(true);
    }

    /// <summary>
    /// 一時停止ボタン　再開
    /// </summary>
    public void PauseContinue()
    {
        AudioManager.Instance.PlaySE("OkSe");
        // Unity時間再開
        Time.timeScale = 1.0f;
        // コンティニューメッセージボックスを表示
        PauseMessageBox.SetActive(false);

    }

    /// <summary>
    /// 一時停止ボタン　あきらめる
    /// </summary>
    public void PauseGiveUp()
    {
        AudioManager.Instance.StopSE();
        AudioManager.Instance.StopBGM();
        AudioManager.Instance.PlaySE("OkSe");
        // Unity時間再開
        Time.timeScale = 1.0f;
        // コンティニューメッセージボックス非表示
        continueMessageBox.SetActive(false);
        // 連戦リストクリア
        GameData.UserData.examStage = 0;
        //　データ初期化
        InitializeData();
        // 広告を表示
        UnityAdsManager.Instance.ShowAd(AdType.BattleGiveUp);
        //// pauseを非表示
        //PauseMessageBox.SetActive(false);

        //ResetPlayers();
    }

    /// <summary>
    /// 値に応じてスライダーを反映させる　
    /// </summary>
    void CheckSlider()
    {
        // HPバー
        Slider HPslider = Playerslider.GetComponent<Slider>();
        //HPslider.value = hum.hp * 0.1f;
        HPslider.value = hum.hp / GameData.UserData. MyCharacterData.HP;

        HPslider = PlayerSPslider.GetComponent<Slider>();
        HPslider.value = hum.specialgauge * 0.1f;

        HPslider = cpuslider.GetComponent<Slider>();
        //HPslider.value = cpu.hp * 0.1f;
        HPslider.value = cpu.hp / GameData.BattleData.CpuHpTable[GameData.CpuTableNo];

        HPslider = cpuSPslider.GetComponent<Slider>();
        HPslider.value = cpu.specialgauge * 0.1f;
    }

    /// <summary>
    /// データ初期化　
    /// </summary>
    public void InitializeData()
    {
        GameData.UserData.examStage = 0;                                        // 挑戦中のステージ初期化
        GameData.BattleMode = BattleMode.None;                                  // バトルモード初期化
        SaveData.SetInt(SaveKey.examStage, GameData.UserData.examStage);        // データを設定
        SaveData.Save();                                                        // データをセーブ

        //// BATTLEネコ削除、HOMEネコ生成
        //GameObject deleteObject = null;
        //deleteObject = GameData.PlayerObjct;
        //string CatPrefabIDStr = "Prefabs/cat" + string.Format("{0:00}", SaveData.GetInt(SaveKey.UserCharacter));
        //GameObject pObject = Instantiate((GameObject)Resources.Load(CatPrefabIDStr), new Vector3(0.0f, 0.0f, 0.0f), Quaternion.Euler(0, 180, 0));
        //DontDestroyOnLoad(pObject);
        //GameData.PlayerObjct = pObject;
        //GameData.PlayerMeshObject = pObject.transform.Find("mesh").gameObject;
        //GameData.PlayerObjct.name = "cat";
        //GameData.SetPlayerActive(false);
        //Destroy(deleteObject);

        //Debug.LogError("BATTLE終了");
    }


}
