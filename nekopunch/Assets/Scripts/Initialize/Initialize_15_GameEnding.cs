﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Initialize_15_GameEnding : MonoBehaviour
{
    [SerializeField, Header("背景の画像")]
    private Image m_backGroundImage;

    [SerializeField, Header("ゲームクリアメッセージ")]
    private GameObject m_gameClearMessage;

    private Animator m_animator;

    void Start()
    {
        GameData.SetPlayerActive(false);
        string CatPrefabIDStr = "Prefabs/cat" + string.Format("{0:00}", SaveData.GetInt(SaveKey.UserCharacter));
        GameObject pObject = Instantiate((GameObject)Resources.Load(CatPrefabIDStr), new Vector3(3.8f, 1.8f, 0.0f), Quaternion.Euler(0, 180, 0));
        pObject.transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);
        m_animator = pObject.GetComponent<Animator>();

        StartCoroutine(GameLoop());
    }

    /// <summary>
    /// ループ処理
    /// </summary>
    /// <returns></returns>
    private IEnumerator GameLoop()
    {
        AudioManager.Instance.PlaySE("GameClearSE");
        yield return new WaitForSeconds(1.0f);
        m_animator.SetTrigger("Win");
        yield return new WaitForSeconds(1.5f);
        m_gameClearMessage.SetActive(true);
    }

    /// <summary>
    /// ホーム画面に戻るためのボタン
    /// </summary>
    public void OnReturnHomeButton()
    {
        SceneFadeManager.Instance.Load(GameData.Scene_Home, 1.0f, true);
    }
}
