﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AutoyaFramework;
using TMPro;

public class Initialize_00_Manager : MonoBehaviour
{
    public GameObject playerObject = null;

	public RuntimeAnimatorController basicAnimCtrl = null;
    public RuntimeAnimatorController battleAnimCtrl = null;

    private void Start()
    {
        // ゲームのフレームレートを設定
        Application.targetFrameRate = GameData.FrameRate;

        // Maio初期化
        //MaioManager.Instance.Init();

        // UnityAds初期化
        UnityAdsManager.Instance.Init();

        // ローカル通知全キャンセル
        LocalNotificationWrapper.CancelAllScheduledNotifications();
        // アプリバッジを消す
        LocalNotificationWrapper.DisableApplicationBadge();

        /// <summary>
        /// ローカル通知
        /// </summary>
        //int afterHour = 24;
        //int afterSec = afterHour * 60 * 60;
        // LocalNotificationWrapper.ReserveNotification("ネコパンチ", "今日もあそぼう！", afterSec);

        GameData.BasicAnimCtrl = basicAnimCtrl;
        GameData.BattleAnimCtrl = battleAnimCtrl;

        //// プレイヤー非表示
        //GameData.SetPlayerActive(false);

        // ホームプレイヤー非表示
        GameData.SetHomePlayerActive(false);
    }
}
