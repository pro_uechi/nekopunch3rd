﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Initialize_03_Title : MonoBehaviour
{
    [SerializeField]
    private Text versionText;

    /// <summary>
    /// 初めてタイトルシーンに入ったか？
    /// </summary>
    private static bool isFirst = true;

    private void Start()
    {
        // 初めてタイトルシーンじゃなければ
        if(!isFirst)
        {
            // ホーム画面を離れる際に呼ばれる
            ItemManager.Instance.LeaveHomeInitialize();
            // PlayerInfoCanvasをfalse
            PlayerInfoCanvas.Instance.Enable(false);
        }

        // ホームプレイヤー非表示
        GameData.SetHomePlayerActive(false);
        AudioManager.Instance.PlayBGM("Title", 0.4f);
        versionText.text = Application.version;

        isFirst = false;
    }
}
