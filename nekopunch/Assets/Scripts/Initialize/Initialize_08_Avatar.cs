﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Initialize_08_Avatar : MonoBehaviour
{

    private const int MAX_AVATOR = 11;

    [SerializeField, Header("常に変更可能なネコ")]
    private int _displayNeko = 2;

    [SerializeField]
    private GameObject[] _nekoAvators = new GameObject[MAX_AVATOR];

    //[SerializeField, Header("各猫の名前")]
    //private string[] _nekoNames = new string[MAX_AVATOR];

    [SerializeField, Header("各ネコの画像")]
    private Sprite[] _nekoSprites = new Sprite[MAX_AVATOR];

    private void Awake()
    {
        CheckAvator();
    }

    /// <summary>
    /// どのくらいネコを表示できるか調べる
    /// </summary>
    void CheckAvator()
    {
        //// ネコの名前初期化
        //foreach (var neko in _nekoAvators)
        //{
        //    //neko.transform.Find("Text").GetComponent<Text>().text = "？？？";

        //}

        // 表示するネコのアバター数を求める
        int num = (GameData.UserData.UserLevel) + _displayNeko;

        // ネコのアバター数が最大値を上回ったら、最大値に戻す
        if (num > MAX_AVATOR) { num = MAX_AVATOR; }

        // ネコのアバター表示
        for (int i = 0; i < num; i++)
        {
            int n = i + 1;
            //_nekoAvators[i].transform.Find("Text").GetComponent<Text>().text = _nekoNames[i];
            _nekoAvators[i].transform.Find("Image").GetComponent<Image>().sprite = _nekoSprites[i];
            _nekoAvators[i].transform.Find("QImage").gameObject.SetActive(false);
            _nekoAvators[i].GetComponent<Button>().onClick.AddListener(() => AvatorChangeButtons(n));
        }
    }

    /// <summary>
    /// ネコの変更ボタン
    /// </summary>
    /// <param name="avatar"></param>
    public void AvatorChangeButtons(int number)
    {
        string catPath = "Prefabs/home_cat0";
        // 10番目以降のネコはcatPathを変える
        if (number >= 10) { catPath = "Prefabs/home_cat"; }

        // ネコを変更する
        AudioManager.Instance.PlaySE("OkSe");
        GameObject deleteObject = GameData.HomePlayerObject;
        GameObject playerObject = Instantiate((GameObject)Resources.Load(catPath+ number.ToString()), new Vector3(0.0f, 0.0f, 0.0f), Quaternion.Euler(0, 180, 0));
        DontDestroyOnLoad(playerObject);
        GameData.HomePlayerObject = playerObject;
        GameData.HomePlayerObject.name = "home_cat";
        Destroy(deleteObject);
        GameData.SetHomePlayerScale(new Vector3(0.8f, 0.8f, 0.8f));
        GameData.SetHomePlayerPosition(GameData.DefalutPos);
        SaveData.SetInt(SaveKey.UserCharacter, number);
        SaveData.Save();
        ItemManager.Instance.SetAnimator();
    }
}
