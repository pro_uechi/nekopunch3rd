﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Initialize_12_Shop : MonoBehaviour
{
    /// <summary>
    /// アイテム数
    /// </summary>
    private const int MAX_ITEM_VALUE = 8;

    /// <summary>
    /// 各アイテムボタン
    /// </summary>
    [SerializeField, Header("各アイテムボタン")]
    private Button[] _items = new Button[MAX_ITEM_VALUE];

    /// <summary>
    /// 各×ボタン
    /// </summary>
    [SerializeField, Header("各×ボタン")]
    private Button[] _closeButton = new Button[9];

    /// <summary>
    /// 各アイテム値段テキスト
    /// </summary>
    [SerializeField, Header("各アイテム値段テキスト")]
    private Text[] _itemTexts = new Text[4];

    /// <summary>
    /// 各アイテムのスターテキスト
    /// </summary>
    [SerializeField, Header("各アイテムのスターテキスト")]
    private Text[] _starTexts = new Text[4];

    /// <summary>
    /// 各アイテムの上昇値テキスト
    /// </summary>
    [SerializeField, Header("各アイテム上昇値テキスト")]
    private Text[] _itemValueTexts = new Text[MAX_ITEM_VALUE];

    /// <summary>
    /// コインで購入する際に表示されるメッセージ
    /// </summary>
    [SerializeField, Header("コインで購入する際に表示されるメッセージ")]
    private GameObject _buyCoinMessage;

    /// <summary>
    /// スターで購入する際に表示されるメッセージ
    /// </summary>
    [SerializeField, Header("スターで購入する際に表示されるメッセージ")]
    private GameObject _buyStarMessage;

    /// <summary>
    /// 購入完了メッセージ
    /// </summary>
    [SerializeField, Header("購入完了メッセージ")]
    private GameObject _buyItemMessage;

    /// <summary>
    /// 購入できませんメッセージ
    /// </summary>
    [SerializeField, Header("購入できませんメッセージ")]
    private GameObject _canNotBuyMessage;

    /// <summary>
    /// コインが足りませんメッセージ
    /// </summary>
    [SerializeField, Header("コインが足りませんメッセージ")]
    private GameObject _notCoinMessage;

    /// <summary>
    /// スターが足りませんメッセージ
    /// </summary>
    [SerializeField, Header("スターが足りませんメッセージ")]
    private GameObject _notStarMessage;

    /// <summary>
    /// 初期化処理
    /// </summary>
    private void Awake()
    {
        // ItemManagerの各変数に格納していく
        ItemManager.Instance.ItemPriceTexts = _itemTexts;
        ItemManager.Instance.ItemBuyStarValueTexts = _starTexts;
        ItemManager.Instance.ItemBuyUpwardValueTexts = _itemValueTexts;
        ItemManager.Instance.BuyCoinMessage = _buyCoinMessage;
        ItemManager.Instance.BuyStarMessage = _buyStarMessage;
        ItemManager.Instance.BuyItemMessage = _buyItemMessage;
        ItemManager.Instance.CanNotBuyItemMessage = _canNotBuyMessage;
        ItemManager.Instance.NotCoinMessage = _notCoinMessage;
        ItemManager.Instance.NotStarMessage = _notStarMessage;

        // ボタンのイベント設定
        _items[0].onClick.AddListener(ItemManager.Instance.OnClickBuyNekoJarashi);
        _items[1].onClick.AddListener(ItemManager.Instance.OnClickBuyBall);
        _items[2].onClick.AddListener(ItemManager.Instance.OnClickBuyMouse);
        _items[3].onClick.AddListener(ItemManager.Instance.OnClickBuyCatFood);
        _items[4].onClick.AddListener(ItemManager.Instance.OnClickBuyATKKakera);
        _items[5].onClick.AddListener(ItemManager.Instance.OnClickBuyDEFKakera);
        _items[6].onClick.AddListener(ItemManager.Instance.OnClickBuySPDKakera);
        _items[7].onClick.AddListener(ItemManager.Instance.OnClickBuyHPKakera);
        _closeButton[0].onClick.AddListener(ItemManager.Instance.OnClickReturnShop);
        _closeButton[1].onClick.AddListener(ItemManager.Instance.OnClickReturnShop);
        _closeButton[2].onClick.AddListener(ItemManager.Instance.OnClickReturnShop);
        _closeButton[3].onClick.AddListener(ItemManager.Instance.OnClickReturnShop);
        _closeButton[4].onClick.AddListener(ItemManager.Instance.OnClickReturnShop);
        _closeButton[5].onClick.AddListener(ItemManager.Instance.OnClickReturnShop);
        _closeButton[6].onClick.AddListener(ItemManager.Instance.OnClickReturnShop);
        _closeButton[7].onClick.AddListener(ItemManager.Instance.OnClickReturnShop);
        _closeButton[8].onClick.AddListener(() => ItemManager.Instance.OnClick_BuyItem_ReturnShop());
        _closeButton[9].onClick.AddListener(() => ItemManager.Instance.OnClick_BuyItem_ReturnShop());
        //_closeButton[7].onClick.AddListener(ItemManager.Instance.OnClick_BuyItem_ReturnShop);
        //_closeButton[8].onClick.AddListener(ItemManager.Instance.OnClick_BuyItem_ReturnShop);
    }

    private void Start()
    {
        // アイテム初期化
        ItemManager.Instance.Initialize();
    }

    /// <summary>
    /// スターで買った際は広告を表示しない
    /// </summary>
    public void ChangeStarButton()
    {
        RemoveButtonEvent();
        _closeButton[8].onClick.AddListener(() => ItemManager.Instance.OnClick_BuyItem_ReturnShop(false));
        _closeButton[9].onClick.AddListener(() => ItemManager.Instance.OnClick_BuyItem_ReturnShop(false));
    }

    /// <summary>
    /// コインで買った際は広告を表示する
    /// </summary>
    public void ChangeCoinButton()
    {
        RemoveButtonEvent();
        _closeButton[8].onClick.AddListener(() => ItemManager.Instance.OnClick_BuyItem_ReturnShop());
        _closeButton[9].onClick.AddListener(() => ItemManager.Instance.OnClick_BuyItem_ReturnShop());
    }

    /// <summary>
    /// ボタンイベント削除
    /// </summary>
    private void RemoveButtonEvent()
    {
        _closeButton[8].onClick.RemoveAllListeners();
        _closeButton[9].onClick.RemoveAllListeners();
    }
}
