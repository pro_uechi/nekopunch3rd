﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;
using AutoyaFramework;

public class NeedCharacterData
{
    public string id;
    public string name;
    public string prefecture_name;
	public string city_name;
    public string permission_no;
	public string url;
	public string mark_Info;
	public string[] text;
    //応援メッセージ
    public string[] yell_message;
    //勝利時のメッセージ
    public string[] win_message_1;
    public string[] win_message_2;
    public string[] win_message_3;
    public List<string[]> win_message = new List<string[]>();

    public void SetMessage()
    {
        win_message.Add(win_message_1);
        win_message.Add(win_message_2);
        win_message.Add(win_message_3);
    }

    public static int MESSAGE_MAX = 3;
}


public class Pictures : SingletonMonoBehaviour<Pictures>
{
    //説明で使うオブジェクトの親
    public GameObject description;

    //説明に乗るキャラの写真
    GameObject picture;
    Text text_name;
    public Text text_profile;

    //ボタンによってのデータを格納する配列
    const int MAX_BUTTON_AREA = 8;
    const int MAX_BUTTON_PARENT = 9;
    const int BUTTON_PARENT_DISTANCE = 250;
    const int MAX_BUTTON_ROW = 4;
    static public int MAX_BUTTON_NUM = MAX_BUTTON_PARENT * MAX_BUTTON_ROW;
    const string IMG_OBJ_NAME = "Image";
    const string SYMBOL_TAG = "(";
    const float DESCRIPTION_PANEL_FIRSTPOS = -0.19f;

    //ボタンを格納する
    GameObject[] button = new GameObject[MAX_BUTTON_NUM];
    GameObject[] image = new GameObject[MAX_BUTTON_NUM];
    GameObject[] bt_area_obj = new GameObject[MAX_BUTTON_AREA];

    //説明文が貼ってあるパネルとテキスト
    public GameObject scroll_panel_text;
    public GameObject scroll_panel;

    List<PictureBookCharacterData> pbCharacterdata = new List<PictureBookCharacterData>();

    //スプライトレンダーの固定値
    const float render_sizeX = 2f;
    const float render_sizeY = 2.5f;

    //一列に入っているボタンの数
    const int ROW_BUTTON_VOLUM = 4;

    //キャラにマスクをかける用のパレット
    Color mask_color = new Color(0, 0, 0);
    Color clear_color = new Color(1, 1, 1);

    bool description_scroll_stop_flg;
    int now_number;

    int now_load_num;
    public GameObject content;

    //キャラクタースクロール用
    Vector2 first_content_pos;

    //スクロール表示範囲
    float ViewHeght;
    public GameObject ScrollView;

    //public Sprite sp = null;

    private List<string> getCharaList;

    public static List<string> canUseCharaList = new List<string>();

}