﻿using System;
using System.Collections;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement; //デバッグ用

/// <summary>
/// ゲーム内通貨処理
/// </summary>
public class GameMoneyController : MonoBehaviour
{

    /// <summary>
    /// ゲーム内通貨を表示するテキスト
    /// </summary>
    private TextMeshProUGUI GameMoneyTextMeshProUGUI;

    /// <summary>
    /// 増加量
    /// </summary>
    private int upSpeed = 10;

    /// <summary>
    /// 減少量
    /// </summary>
    private int downSpeed = 100;

    /// <summary>
    /// 演出用
    /// </summary>
    private int workMoney = 0;
    private int MoneyBuckup = 0;

    /// <summary>
    /// 所持金UPDATE管理フラグ
    /// </summary>
    private bool updateFlg = false;
    private bool MoneyBuckupFlg = true;

    // Use this for initialization
    void Start()
    {
        // 所持金を表示
        this.workMoney = GameData.UserData.Money;
        this.GameMoneyTextMeshProUGUI = this.transform.Find("MoneyText").GetComponent<TextMeshProUGUI>();
        this.GameMoneyTextMeshProUGUI.text = string.Format("{0:#,0}", workMoney);
    }

    // Update is called once per frame
    void Update()
    {
        try
        {
            //this.GameMoneyTextMeshProUGUI.fontSize= 46;

            // マイナス値にならないように保険
            if (workMoney < 0) { workMoney = 0; }
            if (GameData.UserData.Money < 0) { GameData.UserData.Money = 0; }


            if (GameData.UserData != null)
            {
                if (GameData.UserData.Money != workMoney)
                {
                    //this.GameMoneyTextMeshProUGUI.fontSize =46;
                    //this.GameMoneyTextMeshProUGUI.color = Color.red;

                    // アイテム購入後の所持金を保存
                    if (MoneyBuckupFlg)
                    {
                        MoneyBuckup = GameData.UserData.Money;
                        MoneyBuckupFlg = false;
                    }


                    // 演出が終わっていない場合でも所持金を合わせる
                    if (MoneyBuckup != GameData.UserData.Money)
                    {
                        updateFlg = false;
                        MoneyBuckupFlg = true;
                    }

                    // 値が変わった瞬間に一度だけ実行
                    if (!updateFlg)
                    {
                        // お金の数を保存
                        SaveData.SetInt(SaveKey.moneyData, GameData.UserData.Money);
                        SaveData.Save();
                        this.updateFlg = true;
                    }

                    // 増える場合
                    if (GameData.UserData.Money > workMoney)
                    {
                        int amount = Mathf.Abs(GameData.UserData.Money - workMoney);
                        if (amount < 100)
                        {
                            upSpeed = 1;
                        }
                        else if (amount < 1000)
                        {
                            upSpeed = 10;
                        }
                        else if (amount < 10000)
                        {
                            upSpeed = 100;
                        }
                        else
                        {
                            upSpeed = 1000;
                        }

                        workMoney += upSpeed;

                        // 足しすぎたら揃える
                        if (workMoney > GameData.UserData.Money)
                        {
                            workMoney = GameData.UserData.Money;
                        }
                    }
                    // 減る場合
                    else
                    {
                        int amount = Mathf.Abs(GameData.UserData.Money - workMoney);
                        if (amount < 100)
                        {
                            downSpeed = 1;
                        }
                        else if (amount < 1000)
                        {
                            downSpeed = 10;
                        }
                        else if (amount < 10000)
                        {
                            downSpeed = 100;
                        }
                        else
                        {
                            downSpeed = 1000;
                        }

                        workMoney -= downSpeed;

                        // 引きすぎたら揃える
                        if (workMoney < GameData.UserData.Money)
                        {
                            workMoney = GameData.UserData.Money;
                        }
                    }
                    this.GameMoneyTextMeshProUGUI.text = string.Format("{0:#,0}", workMoney);
                }
                else
                {
                    this.updateFlg = false;
                    this.MoneyBuckupFlg = true;
                }
            }
        }
        catch (Exception ex)
        {
            GameObject errBox = Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
            errBox.GetComponent<MessageBoxManager>().Initialize_OK("マネー処理でエラーが発生しました。\n" + ex.Message + "\n" + ex.StackTrace, null);
        }
    }

    /// <summary>
    /// 所持金を更新する
    /// </summary>
    private IEnumerator UpdateMoney()
    {
        yield return StartCoroutine(DBAccessManager.Instance.UpdateUserData());
    }
}
