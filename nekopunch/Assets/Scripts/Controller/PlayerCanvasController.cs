﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

/// <summary>
/// 共通プレイヤーステータス
/// </summary>
public class PlayerCanvasController : SingletonMonoBehaviour<PlayerCanvasController> {

    private Button ReturnButton;

    // Use this for initialization
    void Start () {
		DontDestroyOnLoad(this.gameObject);
        //this.ReturnButton = this.gameObject.transform.Find("ReturnButton").gameObject;
    }
	
	// Update is called once per frame
	void Update ()
    {
        //if (GameInfo.Scene_MiniGame.Equals(SceneManager.GetActiveScene().name))
        //{
        //    this.ReturnButton.SetActive(true);
        //}
        //else
        //{
        //    this.ReturnButton.SetActive(false);
        //}
    }

    public void Enable(bool flg)
    {
        this.gameObject.SetActive(flg);
    }
}
