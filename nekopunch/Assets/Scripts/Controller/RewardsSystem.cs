﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RewardsSystem : MonoBehaviour
{
    // パネル
    [SerializeField]
    private GameObject panel = null;

    /// <summary>
    /// ノードプレハブ
    /// </summary>
    [SerializeField]
    private GameObject nodePrefab = null;

    /// <summary>
    /// ノードの親オブジェクト
    /// </summary>
    [SerializeField]
    private GameObject nodeParent = null;

    /// <summary>
    /// 生成したノード配列
    /// </summary>
    private GameObject[] nodes;

    /// <summary>
    /// 経過時間
    /// </summary>
    private float totalTime = 0;

    /// <summary>
    /// 移動する時間(何秒ごとに移動するか)
    /// </summary>
    private float moveTime = 0;
    private float initMoveTime = 0.05f;

    /// <summary>
    /// ルーレットを動かすかどうか
    /// </summary>
    private bool isMove = false;

    /// <summary>
    /// ルーレットを止めるかどうか
    /// </summary>
    private bool stop = false;

    /// <summary>
    /// 現在のノード
    /// </summary>
    private GameObject currentNode = null;

    /// <summary>
    /// 移動した回数
    /// </summary>
    private int moveCount = 0;

    /// <summary>
    /// 当選アイテムの値
    /// </summary>
    private int getItemValue = 0;

    /// <summary>
    /// パネルテキスト
    /// </summary>
    [SerializeField]
    private TextMeshProUGUI panelText = null;

    /// <summary>
    /// OKボタン
    /// </summary>
    [SerializeField]
    private GameObject okButton = null;


    //IEnumerator Start()
    //{
    //    // データベースからリワードの中身を取得
    //    //yield return StartCoroutine(DBAccessManager.Instance.GetRewardsItemData());
    //}

    private void Update()
    {
        if (isMove)
        {
            totalTime += Time.deltaTime;
            if (totalTime >= moveTime)
            {
                // 順番
                totalTime = 0;
                getItemValue = moveCount % nodes.Length;
                currentNode.transform.Find("SelectImage").GetComponent<Image>().enabled = false;
                currentNode = nodes[getItemValue];
                currentNode.transform.Find("SelectImage").GetComponent<Image>().enabled = true;
                moveCount++;

                // ランダム
                //totalTime = 0;
                //getItemValue = Random.Range(0, nodes.Length);
                //while (getItemValue == prevValue)
                //{
                //    getItemValue = Random.Range(0, nodes.Length);
                //}
                //prevValue = getItemValue;
                //currentNode.transform.Find("SelectImage").GetComponent<Image>().enabled = false;
                //currentNode = nodes[getItemValue];
                //currentNode.transform.Find("SelectImage").GetComponent<Image>().enabled = true;
            }

            if (stop)
            {
                if (moveTime < 1)
                {
                    float speed = Random.Range(0.1f, 0.3f);
                    moveTime += Time.deltaTime * speed;
                }
                else
                {
                    isMove = false;
                    StartCoroutine(Rewards(getItemValue));
                }
            }
        }
    }

    /// <summary>
    /// タッチボタン押下時
    /// </summary>
    public void OnClickTouchButton()
    {
        stop = true;
    }

    /// <summary>
    /// ルーレット開始
    /// </summary>
    public void StartRoulette()
    {
        // リセット
        ResetRoulette();

        // パネル表示
        PanelEnable(true);

        // ルーレットスタート
        isMove = true;
    }

    /// <summary>
    /// リワード
    /// </summary>
    IEnumerator Rewards(int value)
    {
        RewardsItemData item = GameData.RewardsItemDataList[value];
        string str = string.Empty;
        switch (item.ItemType)
        {
            case "coin":
                GameData.UserData.Money += item.Amount;
                str = "コイン+" + item.Amount;
                break;

            //case "continueticket":
            //    GameData.UserData.MyItemData.PossessionItemList_Ticket[GameData.continueTicketID - 1]++;
            //    str = "再戦チケット+" + item.Amount;
            //    break;
        }

        // ユーザーデータ更新
        StartCoroutine(DBAccessManager.Instance.UpdateUserData());
       // StartCoroutine(DBAccessManager.Instance.UpdateMyItemData());

        // 待ってからポップアップ表示
        yield return new WaitForSeconds(1.5f);
        AudioManager.Instance.PlaySE("BattleWinSe");
        panelText.text = str + "\nゲット！";
        okButton.SetActive(true);
    }

    /// <summary>
    /// ノード生成
    /// </summary>
    private void InstantiateNode()
    {
        // 全てのノードを削除
        if (nodes != null)
        {
            for (int i = 0; i < nodes.Length; ++i)
            {
                Destroy(nodes[i]);
            }
        }

        // アイテムの数だけノードを生成
        nodes = new GameObject[GameData.RewardsItemDataList.Count];
        int count = 0;
        foreach (RewardsItemData item in GameData.RewardsItemDataList)
        {
            // 生成
            GameObject node = Instantiate(nodePrefab, nodeParent.transform);
            // 画像分岐
            switch (item.ItemType)
            {
                case "coin":
                    node.transform.Find("ItemImage").GetComponent<Image>().sprite = Resources.Load("UI/06_Home/icon_coin_01", typeof(Sprite)) as Sprite;
                    break;

                case "continueticket":
                    node.transform.Find("ItemImage").GetComponent<Image>().sprite = Resources.Load("UI/06_Home/list_ticket_return", typeof(Sprite)) as Sprite;
                    break;
            }
            // テキスト
            node.transform.Find("Text (TMP)").GetComponent<TextMeshProUGUI>().text = item.Amount.ToString();
            // 配列に代入
            nodes[count] = node;
            count++;
        }

        // 現在のノードを設定
        currentNode = nodes[0];
    }

    /// <summary>
    /// パネル表示非表示
    /// </summary>
    public void PanelEnable(bool flag)
    {
        if (panel != null)
        {
            panel.SetActive(flag);
        }
    }

    /// <summary>
    /// リセット
    /// </summary>
    private void ResetRoulette()
    {
        panelText.text = "タッチしてね!";
        InstantiateNode();
        isMove = false;
        stop = false;
        moveCount = 0;
        getItemValue = 0;
        moveTime = initMoveTime;
        okButton.SetActive(false);
    }
}
