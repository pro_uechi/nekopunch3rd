﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInfoCanvas : SingletonMonoBehaviour<PlayerInfoCanvas>
{
    /// <summary>
    /// プレイヤー名
    /// </summary>
    [SerializeField]
    private TMPro.TextMeshProUGUI playerName = null;
    [SerializeField]
    private TMPro.TextMeshProUGUI HpText = null;
    [SerializeField]
    private TMPro.TextMeshProUGUI AttackText = null;
    [SerializeField]
    private TMPro.TextMeshProUGUI DefenceText = null;
    [SerializeField]
    private TMPro.TextMeshProUGUI SpeedText = null;

    [SerializeField]
    private GameObject ChangeNameDialog = null;

    [SerializeField]
    private InputField m_InputField = null;

    [SerializeField, Header("名前変更に使用されるテキスト")]
    private Text changeNameText = null; 

    /// <summary>
    /// 攻撃演出用変数
    /// </summary>
    private float workAtkValue = 0;

    /// <summary>
    /// 守備演出用変数
    /// </summary>
    private float workDefValue = 0;

    /// <summary>
    /// スピード演出用変数
    /// </summary>
    private float workSpdValue = 0;

    /// <summary>
    /// HP演出用変数
    /// </summary>
    private float workHpValue = 0;

    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
        SetPlayerInfo();
    }

    private void Update()
    {
        WorkHp();
        WorkAtk();
        WorkDef();
        WorkSpd();
    }

    /// <summary>
    /// PlayerInfoCanvasのオンオフ
    /// </summary>
    /// <param name="flg"></param>
    public void Enable(bool flag)
    {
        gameObject.SetActive(flag);
    }

    /// <summary>
    /// プレイヤー情報をセット
    /// </summary>
    public void SetPlayerInfo()
    {
        workHpValue = GameData.UserData.MyCharacterData.HP;
        workAtkValue = GameData.UserData.MyCharacterData.ATK;
        workDefValue = GameData.UserData.MyCharacterData.DEF;
        workSpdValue = GameData.UserData.MyCharacterData.SPD;

        playerName.text = GameData.UserData.MyCharacterData.CharacterName;
        HpText.text = GameData.UserData.MyCharacterData.HP.ToString();
        AttackText.text = GameData.UserData.MyCharacterData.ATK.ToString();
        DefenceText.text = GameData.UserData.MyCharacterData.DEF.ToString();
        SpeedText.text = GameData.UserData.MyCharacterData.SPD.ToString();
    }


    private void WorkHp()
    {

        // 増える場合
        if (GameData.UserData.MyCharacterData.HP > workHpValue)
        {
            int upSpeed = 0;
            int amount = Mathf.Abs((int)GameData.UserData.MyCharacterData.HP - (int)workHpValue);

            if (amount < 100)
            {
                upSpeed = 1;
            }
            else if (amount < 1000)
            {
                upSpeed = 10;
            }
            else if (amount < 10000)
            {
                upSpeed = 100;
            }
            else
            {
                upSpeed = 1000;
            }

            workHpValue += upSpeed;

            // 足しすぎたら揃える
            if (workHpValue > GameData.UserData.MyCharacterData.HP)
            {
                workHpValue = GameData.UserData.MyCharacterData.HP;
            }

            HpText.text = workHpValue.ToString();
        }
    }

    private void WorkAtk()
    {

        // 増える場合
        if (GameData.UserData.MyCharacterData.ATK > workAtkValue)
        {
            int upSpeed = 0;
            int amount = Mathf.Abs((int)GameData.UserData.MyCharacterData.ATK - (int)workAtkValue);

            if (amount < 100)
            {
                upSpeed = 1;
            }
            else if (amount < 1000)
            {
                upSpeed = 10;
            }
            else if (amount < 10000)
            {
                upSpeed = 100;
            }
            else
            {
                upSpeed = 1000;
            }

            workAtkValue += upSpeed;

            // 足しすぎたら揃える
            if (workAtkValue > GameData.UserData.MyCharacterData.ATK)
            {
                workAtkValue = GameData.UserData.MyCharacterData.ATK;
            }

            AttackText.text = workAtkValue.ToString();
        }
    }

    private void WorkDef()
    {

        // 増える場合
        if (GameData.UserData.MyCharacterData.DEF > workDefValue)
        {
            int upSpeed = 0;
            int amount = Mathf.Abs((int)GameData.UserData.MyCharacterData.DEF - (int)workDefValue);

            if (amount < 100)
            {
                upSpeed = 1;
            }
            else if (amount < 1000)
            {
                upSpeed = 10;
            }
            else if (amount < 10000)
            {
                upSpeed = 100;
            }
            else
            {
                upSpeed = 1000;
            }

            workDefValue += upSpeed;

            // 足しすぎたら揃える
            if (workDefValue > GameData.UserData.MyCharacterData.DEF)
            {
                workDefValue = GameData.UserData.MyCharacterData.DEF;
            }

            DefenceText.text = workDefValue.ToString();
        }
    }

    private void WorkSpd()
    {

        // 増える場合
        if (GameData.UserData.MyCharacterData.SPD > workSpdValue)
        {
            int upSpeed = 0;
            int amount = Mathf.Abs((int)GameData.UserData.MyCharacterData.SPD - (int)workSpdValue);

            if (amount < 100)
            {
                upSpeed = 1;
            }
            else if (amount < 1000)
            {
                upSpeed = 10;
            }
            else if (amount < 10000)
            {
                upSpeed = 100;
            }
            else
            {
                upSpeed = 1000;
            }

            workSpdValue += upSpeed;

            // 足しすぎたら揃える
            if (workSpdValue > GameData.UserData.MyCharacterData.SPD)
            {
                workSpdValue = GameData.UserData.MyCharacterData.SPD;
            }

            SpeedText.text = workSpdValue.ToString();
        }
    }


    /// <summary>
    /// 名前変更のダイアログを表示
    /// </summary>
    public void OnClickChangeNameDialog()
    {
        changeNameText.text = string.Empty;
        m_InputField.text = GameData.UserData.MyCharacterData.CharacterName;
        ChangeNameDialog.SetActive(true);
    }

    /// <summary>
    /// 名前変更のダイアログを非表示
    /// </summary>
    public void OnClickCloseChangeNameDialog()
    {
        // ネコを変更する
        AudioManager.Instance.PlaySE("OkSe");
        ChangeIsFirstEnterName();
        ChangeNameDialog.SetActive(false);
    }

    /// <summary>
    /// 名前変更
    /// </summary>
    public void OnClickChangeName()
    {
        // ネコを変更する
        AudioManager.Instance.PlaySE("OkSe");
        if (changeNameText.text.Length != 0)
        {
            GameData.UserData.MyCharacterData.CharacterName = changeNameText.text;
            SaveData.SetString(SaveKey.UserCatName, GameData.UserData.MyCharacterData.CharacterName);
            SaveData.Save();
            SetPlayerInfo();
        }
        ChangeIsFirstEnterName();
        ChangeNameDialog.SetActive(false);
    }

    /// <summary>
    /// 初めて名前を入力したか調べる
    /// </summary>
    public void ChangeIsFirstEnterName()
    {
        // 初めて名前を入力したなら...
        if (SaveData.GetInt(SaveKey.IsFirstEnterName, 0) == 0)
        {
            // IsFirstEnterNameを1にして、ChangeNameDialogを最初に表示しないようにする
            GameData.UserData.IsFirstEnterName = 1;
            SaveData.SetInt(SaveKey.IsFirstEnterName, GameData.UserData.IsFirstEnterName);
        }
    }

}
