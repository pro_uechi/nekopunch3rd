﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AutoyaFramework;
using TMPro;

public class CreditsList : MonoBehaviour
{
    TextMeshProUGUI ListText;

	//クレジットファイルの格納ディレクトリ
	//public const string LOAD_QUE_FOLDER = "Question/";
	public const string LOAD_CREDIT_LIST_FOLDER = "";

	// クレジットファイルリストの定義ファイル
	public const string LOAD_CREDIT_LIST = LOAD_CREDIT_LIST_FOLDER + "CreditsList";

	// Use this for initialization
	void Start()
    {
        ListText = transform.Find("Scroll View/Viewport/Content/Text").GetComponent<TextMeshProUGUI>();



		StartCoroutine(LoadCreditsText());
    }

    IEnumerator LoadCreditsText()
    {
		TextAsset textCategoryJson = Resources.Load(LOAD_CREDIT_LIST) as TextAsset;
		Debug.Log(name + "アセットの読み込み 成功");
		ListText.text = textCategoryJson.text;


		//// アセットバンドル内のアセット読み込みとテキストに表示
		//Autoya.AssetBundle_LoadAsset<TextAsset>
  //      (
  //          assetName: "Assets/AssetBundleMaterials/Descriptions/CreditsList.txt",
  //          loadSucceeded: (name, orgText) =>
  //          {
  //              Debug.Log(name + "アセットの読み込み 成功");

  //              ListText.text = orgText.text;

  //          },
  //          loadFailed: (name, err, reason, status) =>
  //          {
  //              Debug.LogFormat(name + "アセットの読み込み 失敗\n\n{0}\n\n{1}\n\n{2}", err, reason, status);
  //          }
  //      );
        yield break;
    }
}
