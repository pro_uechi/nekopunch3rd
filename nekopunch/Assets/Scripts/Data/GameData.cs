﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;
using System.Security.Cryptography;
using System.IO;
using System.Text;

#region enum

/// <summary>
/// 広告タイプ
/// </summary>
public enum AdType
{
    None,
    Home,
    BattleContinue,
    BattleGiveUp,
    NextBossBattle,
    LoginBonus,
    WaitTimeHome,
    Max
}

/// <summary>
/// アニメーターコントローラータイプ
/// </summary>
public enum AnimatorControllerType
{
    None,
    Basic,
    Battle,
    Standing,
    Lose,
    Max
}

/// <summary>
/// アイテムタイプ
/// </summary>
public enum ItemType
{
    Tops,
    FaceAcc,
    HairAcc,
    FrontHair,
    BackHair,

    Ticket,
    Max
}

/// <summary>
/// バトルモード
/// </summary>
public enum BattleMode
{
    None,
    Exam,
	Final,
	Max
}

#endregion

public class GameData : SingletonMonoBehaviour<GameData>
{
    #region 定数

    /// <summary>
    /// フレームレート
    /// </summary>
    public const int FrameRate = 60;

    /// <summary>
    /// フェードイン・フェードアウト速度
    /// </summary>
    public const float FadeSpeed = 0.3f;

    ///// <summary>
    ///// 都道府県の数
    ///// </summary>
    public const int PrefectureVolume = 47;

    ///// <summary>
    ///// エリアの数
    ///// </summary>
    //public const int RegionVolume = 8;

    /// <summary>
    /// 秒
    /// </summary>
    public const int SECOND = 60;

    /// <summary>
    /// 分
    /// </summary>
    public const int MINUTE = SECOND * 60;

    /// <summary>
    /// 時間
    /// </summary>
    public const int HOUR = MINUTE * 60;


    /// <summary>
    /// シェア用URL
    /// </summary>
    public const string SHARE_URL = "https://bit.ly/2IF86IC";

    /// <summary>
    /// ストアURL
    /// </summary>
    public const string REVIEW_URL_IPHONE = "https://apps.apple.com/us/app/%E4%B8%80%E6%92%83%E5%BF%85%E6%AE%BA%E3%83%8D%E3%82%B3%E3%83%91%E3%83%B3%E3%83%81/id1505482197?l=ja&ls=1";
    public const string REVIEW_URL_ANDROID = "https://play.google.com/store/apps/details?id=jp.co.Prolead.nekopunch";

    /// <summary>
    /// 利用規約URL
    /// </summary>
    public const string REGAL_URL = "http://prolead.co.jp/transaction/nekopunch_legal.html";

    /// <summary>
    /// MaioID
    /// </summary>
#if UNITY_IOS
    //public const string MaioMediaId = "DemoPublisherMedia";
    //public const string MaioZoneId = "DemoPublisherZone";
	public const string MaioMediaId = "mf85eb759bf8f473ba711b9afe27c5ad6";
    public const string MaioZoneId = "zce8f38e7140f75b4133327f879bce6d1";

#elif UNITY_ANDROID
    public const string MaioMediaId = "DemoPublisherMediaForAndroid";
    public const string MaioZoneId = "DemoPublisherZoneForAndroid";
    //public const string MaioMediaId = "m782e24cdc760efcdd951244fadb6bcdd";
    //public const string MaioZoneId = "zc899cc4ecf2be0fbeb61d8dfbe21df72";
#endif

    /// <summary>
    /// サウンドON,OFF
    /// </summary>
    public const int SETTING_SOUND_NONE = 0;   // 初期値
    public const int SETTING_SOUND_OFF = 1;    // OFF
    public const int SETTING_SOUND_ON = 2;     // ON

    /// <summary>
    /// タグ名
    /// </summary>    //public const string tag_HurtBox = "HurtBox";


    /// <summary>
    /// 1キャラゲットするのにかかるチケット枚数
    /// </summary>
    public const int price_getCharacter = 5;


    /// <summary>
    /// 1ステージの連続ゲーム数
    /// </summary>
    public const int numGameToRankConfirm = 5;

    /// <summary>
    /// 対戦相手をランダムで取得するときの幅
    /// </summary>
    public const int initEnemyEntryDataLevelLinmit = 1;
    public static int enemyEntryDataLevelLinmit = initEnemyEntryDataLevelLinmit;


    /// <summary>
    /// レビューポップアップを表示する
    /// </summary>
    public const int numWinToReview = 30;

    /// <summary>
    /// 暗号化パスワード
    /// </summary>
    private const string PASSWORD = "prolead";

    /// <summary>
    /// レベルの上限値
    /// </summary>
    public const int MaxLevelRank = 8;

    /// <summary>
    /// 現在はホームシーンか？
    /// </summary>
    public static bool IsHome = false;

    /// <summary>
    /// ホーム中に広告を表示したか？
    /// </summary>
    public static bool IsHomeAbs = false;

    #endregion

    #region シーン名

    /// <summary>
    /// シーン名:00_Managerシーン
    /// </summary>
    public const string Scene_Manager = "00_Manager";

    /// <summary>
    /// シーン名:01_Splashシーン
    /// </summary>
    public const string Scene_Splash = "01_Splash";

    /// <summary>
    /// シーン名:02_DataCheckシーン
    /// </summary>
    public const string Scene_DataCheck = "02_DataCheck";

    /// <summary>
    /// シーン名:03_Titleシーン
    /// </summary>
    public const string Scene_Title = "03_Title";

	/// <summary>
	/// シーン名:04_StageMenuシーン
	/// </summary>
	public const string Scene_Menu = "04_StageMenu";


	/// <summary>
	/// シーン名:06_Homeシーン
	/// </summary>
	public const string Scene_Home = "06_Home";

    /// <summary>
    /// シーン名:07_Battleシーン
    /// </summary>
    public const string Scene_Battle = "07_Battle";

    /// <summary>
    /// シーン名:08_Avatarシーン
    /// </summary>
    public const string Scene_Avatar = "08_Avatar";

    /// <summary>
    /// シーン名:10_Settingシーン
    /// </summary>
    public const string Scene_Setting = "10_Setting";

    /// <summary>
    /// シーン名:11_Chargesシーン
    /// </summary>
    public const string Scene_Charges = "11_Charges";

    /// <summary>
    /// シーン名:12_Shopシーン
    /// </summary>
    public const string Scene_Shop = "12_Shop";

    /// <summary>
    /// シーン名:15_GameEndingシーン
    /// </summary>
    public const string Scene_Ending = "15_GameEnding";



    #endregion

    #region 一時保存データ

    /// <summary>
    /// プレイヤーオブジェクト
    /// </summary>
    public static GameObject PlayerObjct = new GameObject();

    /// <summary>
    /// プレイヤーメッシュオブジェクト
    /// </summary>
    public static GameObject PlayerMeshObject = new GameObject();

    /// <summary>
    /// ホームで使用するオブジェクト
    /// </summary>
    public static GameObject HomePlayerObject = new GameObject();

    /// <summary>
    /// ホームプレイヤーメッシュオブジェクト
    /// </summary>
    public static GameObject HomePlayerMeshObject = new GameObject();

	/// <summary>
	/// プレイヤーオブジェクト
	/// </summary>
	public static GameObject CpuObjct = new GameObject();


	/// <summary>
	/// ホーム時のアニメーターコントローラー
	/// </summary>
	public static RuntimeAnimatorController BasicAnimCtrl;

    /// <summary>
    /// バトル時のアニメーターコントローラー
    /// </summary>
    public static RuntimeAnimatorController BattleAnimCtrl;

    /// <summary>
    /// アプリ情報データ
    /// </summary>
    public static AppInfoData AppInfoData;

    /// <summary>
    /// ユーザー情報(戦績は累計)
    /// </summary>
    public static UserData UserData = new UserData();


	/// <summary>
	/// CPU情報
	/// </summary>
	public static BattleData BattleData = new BattleData();


	/// <summary>
	/// キャラクターデータリスト
	/// </summary>
	public static List<CharacterData> CharacterDataList;


	/// <summary>
	/// 挑戦中のレベル
	/// </summary>
	public static int  Levelrank ;

    /// <summary>
    /// 挑戦中のレベル名
    /// </summary>
    public static string LevelName;

	/// <summary>
	/// 挑戦中のCPUテーブル番号
	/// </summary>
	public static int CpuTableNo;

	/// <summary>
	/// ステージ数データリスト
	/// </summary>
	public static List<StageData> StageDataList;


	/// <summary>
	/// 一試合のポイント増減
	/// </summary>
	public static int PointIncrement;

    /// <summary>
    /// アイテムデータリスト
    /// </summary>
    public static List<ItemData> ItemDataList;

    /// <summary>
    /// リワードアイテムデータリスト
    /// </summary>
    public static List<RewardsItemData> RewardsItemDataList;

    /// <summary>
    /// バトルモード
    /// </summary>
    public static BattleMode BattleMode;

    /// <summary>
    /// プレイヤーの初期位置
    /// </summary>
    public static Vector3 DefalutPos = new Vector3(-4.0f, 0.0f, 0.0f);

    #endregion

    #region Utility

    /// <summary>
    /// テクスチャーが入っているフォルダのパス（Resoursesから見ての）
    /// </summary>
    public const string TexturesPath = "Textures/";

    /// <summary>
    /// 無敵モード
    /// </summary>
    public static bool invincibleMode = false;



    /// <summary>
    /// キャラクターテクスチャ名を返す
    /// </summary>
    /// <param name="regionid"></param>
    /// <param name="prefecturesid"></param>
    /// <param name="characterid"></param>
    /// <returns></returns>
    public static string GetCharacterTextureName(int regionid, int prefecturesid, int characterid)
    {
        string str = string.Format("{0:00}", regionid) + "_" + string.Format("{0:00}", prefecturesid) + "_" + string.Format("{0:00}", characterid);
        return str;
    }


    /// <summary>
    /// Colorを文字列に変換
    /// </summary>
    public static string ConvertColorToString(Color color)
    {
        string str = (int)(color.r * 255) + "/" + (int)(color.g * 255) + "/" + (int)(color.b * 255);
        return str;
    }

    /// <summary>
    /// 文字列をColorに変換
    /// </summary>
    /// <returns></returns>
    public static Color ConvertStringToColor(string str)
    {
        char[] delimiterChar = { '/' };
        string[] splitStr = str.Split(delimiterChar);

        Color color = new Color(float.Parse(splitStr[0]) / 255, float.Parse(splitStr[1]) / 255, float.Parse(splitStr[2]) / 255);
        return color;
    }



    /// <summary>
    /// リストを文字列に変換
    /// </summary>
    public static string ConvertListToString<T>(List<T> list, bool sort)
    {
        if (list != null && list.Count > 0)
        {
            if (sort)
            {
                // 重複をなくす
                HashSet<T> hs = new HashSet<T>(list);
                list = new List<T>(hs);

                // リストを昇順にソート
                list.Sort();
            }

            string str = string.Empty;
            for (int i = 0; i < list.Count - 1; ++i)
            {
                str += list[i].ToString() + "/";
            }
            str += list[list.Count - 1].ToString();

            return str;
        }

        Debug.LogError("空文字を返します。");
        return string.Empty;
    }

    /// <summary>
    /// 文字列をリストに変換
    /// </summary>
    /// <returns></returns>
    public static List<T> ConvertStringToList<T>(string str, bool sort)
    {
        if (str != string.Empty)
        {
            char[] delimiterChar = { '/' };
            string[] splitStr = str.Split(delimiterChar);

            List<T> list = new List<T>(splitStr.Length);

            Type type = typeof(T);
            if (type == typeof(int))
            {
                for (int i = 0; i < splitStr.Length; ++i)
                {
                    list.Add((T)(object)int.Parse(splitStr[i]));
                }
            }
            else if (type == typeof(string))
            {
                for (int i = 0; i < splitStr.Length; ++i)
                {
                    list.Add((T)(object)(splitStr[i]));
                }
            }

            if (sort)
            {
                list.Sort();
            }

            return list;
        }

        Debug.LogError("リストを初期化して返します。");
        return new List<T>();
    }

    /// <summary>
    /// アニメーターコントローラーを切り替える
    /// </summary>
    /// <param name="type"></param>
    public static void SetAnimatorController(AnimatorControllerType type)
    {
        PlayerObjct.GetComponent<Animator>().speed = 1;

        switch (type)
        {
            case AnimatorControllerType.None:
                PlayerObjct.GetComponent<Animator>().runtimeAnimatorController = null;
                break;

            case AnimatorControllerType.Basic:
                PlayerObjct.GetComponent<Animator>().runtimeAnimatorController = BasicAnimCtrl;
                break;

            case AnimatorControllerType.Battle:
                PlayerObjct.GetComponent<Animator>().runtimeAnimatorController = BattleAnimCtrl;
                break;
        }
    }

    /// <summary>
    /// プレイヤーの表示非表示
    /// </summary>
    /// <param name="active"></param>
    public static void SetPlayerActive(bool active)
    {
        PlayerObjct.SetActive(active);
	}

    /// <summary>
    /// ホームプレイヤーの表示非表示
    /// </summary>
    /// <param name="active"></param>
    public static void SetHomePlayerActive(bool active)
    {
        HomePlayerObject.SetActive(active);
    }

    /// <summary>
    /// プレイヤーの大きさを設定
    /// </summary>
    /// <param name="scale"></param>
    public static void SetPlayerScale(Vector3 scale)
    {
        PlayerObjct.transform.localScale = scale;
	}

    /// <summary>
    /// ホームプレイヤーの大きさ設定
    /// </summary>
    /// <param name="scale"></param>
    public static void SetHomePlayerScale(Vector3 scale)
    {
        HomePlayerObject.transform.localScale = scale;
    }

	/// <summary>
	/// プレイヤーの大きさを設定
	/// </summary>
	/// <param name="scale"></param>
	public static void SetPlayerPosition(Vector3 pos)
    {
		PlayerObjct.transform.position = pos;
	}

    /// <summary>
    /// ホームプレイヤーの大きさを設定
    /// </summary>
    /// <param name="scale"></param>
    public static void SetHomePlayerPosition(Vector3 pos)
    {
        HomePlayerObject.transform.position = pos;
    }

    /// <summary>
    /// ユーザーレベルを返す
    /// </summary>
    /// <param name="userlevel"></param>
    /// <returns></returns>
    public static string GetRankName(int userlevel)
    {
        int level = userlevel;
        string str = "";
        if (level == 0)
        {
            return "初段";
        }

        //マイナス値だった場合
        if (level < 0)
        {
            str = "級";
            level = Mathf.Abs(level);
        }
        //プラス値なら
        else if (level > 0)
        {
            level++;
            str = "段";
        }

        return toKansuji(level) + str;
    }

    /// <summary>
    /// 数字を漢数字に変換して返す
    /// </summary>
    /// <param name="number"></param>
    /// <returns></returns>
    private static string toKansuji(int number)
    {
        if (number == 0)
        {
            return "〇";
        }

        string[] kl = new string[] { "", "十", "百", "千" };
        string[] tl = new string[] { "", "万", "億", "兆", "京" };
        string[] nl = new string[] { "", "一", "二", "三", "四", "五", "六", "七", "八", "九" };
        string str = "";
        int keta = 0;
        while (number > 0)
        {

            int k = keta % 4;
            int n = (int)(number % 10);

            //単位が変わって初めての桁、かつその桁の数値が「0」でない（(例：一億万)となるのを防ぐ）
            if (k == 0 && number % 10000 > 0)
            {
                str = tl[keta / 4] + str;
            }

            //今見ている数値が"十", "百", "千"の位であり、かつ「１」だった場合
            if (k != 0 && n == 1)
            {
                str = kl[k] + str;
            }
            //今見ている数値が「０」でない場合
            else if (n != 0)
            {
                str = nl[n] + kl[k] + str;
            }

            keta++;
            number /= 10;
        }
        return str;
    }


    private static char[] alphabet = new char[] { 'a', 'd', 'm', 'x', 'z', 'e', 'c', 'k', 'v', 's' };
    private const int length = 2;
    /// <summary>
    /// ユーザーIDを文字列に変換
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public static string ConvertUserIDToString(int id)
    {
        string idStr = String.Format("{0:D6}", id);
        char[] charArray = idStr.ToCharArray();
        string str = string.Empty;
        for (int i = 0; i < charArray.Length; ++i)
        {
            int num = int.Parse(charArray[i].ToString());
            str += alphabet[num];
        }
        // ユーザーIDの末尾2桁を文字列に追加

        string addStr = String.Format("{0:D7}", id);
        addStr = addStr.Substring(addStr.Length - length, length);
        str += addStr;

        return str;
    }

    /// <summary>
    /// 文字列をユーザーIDに変換
    /// </summary>
    /// <param name="cryptText"></param>
    /// <returns></returns>
    public static int ConvertStringToUserID(string text)
    {
        // 末尾の2文字を削除
        text = text.Remove(text.Length - length, length);

        char[] charArray = text.ToCharArray();
        string str = string.Empty;
        for (int i = 0; i < charArray.Length; ++i)
        {
            switch (charArray[i])
            {
                case 'a':
                    str += "0";
                    break;
                case 'd':
                    str += "1";
                    break;
                case 'm':
                    str += "2";
                    break;
                case 'x':
                    str += "3";
                    break;
                case 'z':
                    str += "4";
                    break;
                case 'e':
                    str += "5";
                    break;
                case 'c':
                    str += "6";
                    break;
                case 'k':
                    str += "7";
                    break;
                case 'v':
                    str += "8";
                    break;
                case 's':
                    str += "9";
                    break;
            }
        }

        int num = int.Parse(str);
        return num;
    }

    #endregion

}
