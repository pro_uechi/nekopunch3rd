﻿using System;
using UnityEngine;

/// <summary>
/// アプリ情報データ
/// </summary>
[Serializable]
public class AppInfoData
{
    /// <summary>
    /// OS
    /// </summary>
    public string os = string.Empty;

    /// <summary>
    /// アプリバージョン
    /// </summary>
    public string appVersion = string.Empty;

    /// <summary>
    /// メンテナンス状態 0:メンテナンス中ではない 1:メンテナンス中
    /// </summary>
    public int maintenance = 0;

    /// <summary>
    /// メンテナンス情報
    /// </summary>
    public string maintenanceInfo = string.Empty;
}