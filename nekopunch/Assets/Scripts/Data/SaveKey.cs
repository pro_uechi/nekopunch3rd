﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// セーブデータキー
/// </summary>
public class SaveKey : SingletonMonoBehaviour<SaveKey>
{
    /// <summary>
    /// ユーザー情報を取得・保存するキー
    /// </summary>
    public const string UserId = "UserId";

    /// <summary>
    /// ユーザーの使用しているネコの名前
    /// </summary>
    public const string UserCatName = "UserCatName";

	/// <summary>
	/// ユーザー情報を取得・保存するキー
	/// </summary>
	public const string UserData = "UserData";

    /// <summary>
    /// ホームにいる時間を取得・保存するキー
    /// </summary>
    public const string HomeTime = "HomeTime";

    /// <summary>
    /// 装備データを取得、保存するキー
    /// </summary>
    public const string EquipData = "EquipData";

    /// <summary>
    /// ユーザーが使用しているキャラクターデータを取得、保存するキー
    /// </summary>
    public const string UserCharacter = "UserCharacter";

    /// <summary>
    /// ユーザーが獲得しているキャラクターデータを取得、保存するキー
    /// </summary>
    public const string GetCharData = "GetCharData";

    /// <summary>
    /// ユーザーが保持している戦績やポイントのデータ
    /// </summary>
    public const string PlayerUsetData = "PlayerUserData";

    /// <summary>
    /// BGMの設定を取得、保存するキー
    /// </summary>
    public const string BGMSetting = "BGMSetting";

    /// <summary>
    /// SEの設定を取得、保存するキー
    /// </summary>
    public const string SESetting = "SESetting";


    /// <summary>
    /// バトル回数を取得、保存するキー
    /// </summary>
    public const string BattleCount = "BattleCount";

	/// <summary>
	/// バトル途中の結果を取得、保存するキー
	/// </summary>
	public const string examLevel = "examLevel";
	public const string examStage = "examStage";

	/// <summary>
	/// レビュー状態を取得、保存するキー
	/// </summary>
	public const string Reviewed = "Reviewed";

    /// <summary>
    /// クリア済みのレベル
    /// </summary>
    public const string clearLevel = "clearLevel";

    /// <summary>
    /// 攻撃力保存
    /// </summary>
    public const string ATKData = "ATKData";

    /// <summary>
    /// 防御力保存
    /// </summary>
    public const string DEFData = "DEFData";

    /// <summary>
    /// 体力保存
    /// </summary>
    public const string HPData = "HPData";

    /// <summary>
    /// スピード保存
    /// </summary>
    public const string AgilityData = "AgilityData";

    /// <summary>
    /// お金保存
    /// </summary>
    public const string moneyData = "moneyData";

    /// <summary>
    /// スターのデータ
    /// </summary>
    public const string starData = "starData";

    /// <summary>
    /// 上昇値保存
    /// </summary>
    public const string RiseValueData = "RiseValueData";

    /// <summary>
    /// 値段の値保存
    /// </summary>
    public const string PriceValueData = "PriceValueData";

    /// <summary>
    /// 各アニメーション時間の保存
    /// </summary>
    public const string AniTimes = "AniTimes";

    /// <summary>
    /// 各アニメーションの名前保存
    /// </summary>
    public const string AniNames = "AniNames";

    /// <summary>
    /// 各アニメーションの上昇値のインデックス
    /// </summary>
    public const string AniRiseIndexs = "AniRiseIndexs";

    /// <summary>
    /// ステータスメッセージの名前を取得
    /// </summary>
    public const string StatusMessageName = "StatusMessageName";

    /// <summary>
    ///　初めて名前を入力するか？の名前保存
    /// </summary>
    public const string IsFirstEnterName = "IsFirstEnterName";
}
