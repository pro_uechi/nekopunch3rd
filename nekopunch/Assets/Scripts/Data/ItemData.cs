﻿using System;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// アイテムデータ
/// </summary>
[Serializable]
public class ItemData
{
    /// <summary>
    /// アイテムID
    /// </summary>
    public int ItemId = 0;

    /// <summary>
    /// アイテムタイプ
    /// </summary>
    public string ItemType = "";

    /// <summary>
    /// 価格
    /// </summary>
    public int Price = 0;

    /// <summary>
    /// 支払い方法
    /// </summary>
    public int PaymentType = 0;

    /// <summary>
    /// 優先度
    /// </summary>
    public int Priority = 0;

    // 各パーツごとの所持リスト文字列
    //public string PossessionItemListStr_Tops = string.Empty;
    //public string PossessionItemListStr_Face_Acc = string.Empty;
    //public string PossessionItemListStr_Hair_Acc = string.Empty;
    //public string PossessionItemListStr_Hair_Front = string.Empty;
    //public string PossessionItemListStr_Hair_Back = string.Empty;
    //public string PossessionItemListStr_Eye = string.Empty;
    //public string PossessionItemListStr_Mouth = string.Empty;
    //public string PossessionItemListStr_Bottoms = string.Empty;
    //public string PossessionItemListStr_Shoes = string.Empty;
    //public string PossessionItemListStr_Socks = string.Empty;

    //// 各パーツごとの所持リスト
    //public List<int> PossessionItemList_Tops = new List<int>();
    //public List<int> PossessionItemList_Face_Acc = new List<int>();
    //public List<int> PossessionItemList_Hair_Acc = new List<int>();
    //public List<int> PossessionItemList_Hair_Front = new List<int>();
    //public List<int> PossessionItemList_Hair_Back = new List<int>();
    //public List<int> PossessionItemList_Eye = new List<int>();
    //public List<int> PossessionItemList_Mouth = new List<int>();
    //public List<int> PossessionItemList_Bottoms = new List<int>();
    //public List<int> PossessionItemList_Shoes = new List<int>();
    //public List<int> PossessionItemList_Socks = new List<int>();

    //// チケット
    //public string PossessionItemListStr_Ticket = string.Empty;
    //public List<int> PossessionItemList_Ticket = new List<int>();
}
