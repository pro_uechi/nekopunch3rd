﻿using System;
using UnityEngine;

/// <summary>
/// ユーザー情報
/// </summary>
[Serializable]
public class CharacterData
{
    /// <summary>
    /// スピードのマックス値
    /// </summary>
    public float MAX_HP = 9999;

    /// <summary>
    /// アタックのマックス値
    /// </summary>
    public float MAX_ATK = 999;

    /// <summary>
    /// ディフェンスのマックス値
    /// </summary>
    public float MAX_DEF = 999;

    /// <summary>
    /// スピードのマックス値
    /// </summary>
    public float MAX_SPD = 999;

    /// <summary>
    /// キャラクターID
    /// </summary>
    public int CharacterId = 0;

	/// <summary>
	/// キャラクター名
	/// </summary>
	public string CharacterName = "ネコ";

    /// <summary>
    /// スピードのデフォルト値(SPDの値と同じにする)
    /// </summary>
    public float Defalut_SPD = 10;

	/// <summary>
	/// HP
	/// </summary>
	public float HP = 100;

	/// <summary>
	/// ATK
	/// </summary>
	public float ATK = 10;

	/// <summary>
	/// DEF
	/// </summary>
	public float DEF = 10;
    
	/// <summary>
	/// SPD
	/// </summary>
	public float SPD = 10;

}