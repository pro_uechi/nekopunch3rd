﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class ReturnButton : SingletonMonoBehaviour<ReturnButton>
{
    private Button Bt_Return;

    // Use this for initialization
    void Start () {
        Bt_Return = this.GetComponent<Button>();
    }

    public void ReturnButtonRemoveAllListeners()
    {
        Bt_Return.onClick.RemoveAllListeners();
    }

    public void ReturnButtonSetListeners(UnityAction action)
    {
        ReturnButtonRemoveAllListeners();
        Bt_Return.onClick.AddListener(action);
    }

    public void Eneble(bool flg)
    {
        this.gameObject.SetActive(flg);
    }

    /// <summary>
    ///　イベントを一つだけ入れる用
    /// </summary>
    public void SetEvent(UnityAction action)
    {
        Eneble(true);
        ReturnButtonRemoveAllListeners();
        ReturnButtonSetListeners(action);
    }
}
