﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuButton : MonoBehaviour
{
    /// <summary>
    /// メニューボタン押下時
    /// </summary>
    public void OnClickMenuButton()
    {
        // 設定シーンに遷移
        SceneFadeManager.Instance.Load(GameData.Scene_Setting, GameData.FadeSpeed, true);
    }
}
