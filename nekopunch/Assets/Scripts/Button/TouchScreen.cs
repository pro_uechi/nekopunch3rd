﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;
using System.Text;

/// <summary>
/// タイトル画面 ボタン押下制御
/// </summary>
public class TouchScreen : MonoBehaviour
{
    /// <summary>
    /// メッセージボックス
    /// </summary>
    private GameObject messageBoxPrefab;

    // 連打防止用
    private bool DontTouchFlg = false;

    // Use this for initialization
    void Start()
    {
        DontTouchFlg = false;
        messageBoxPrefab = (GameObject)Resources.Load("Prefabs/MessageBox");
    }

    /// <summary>
    /// タイトル画面タッチ時
    /// </summary>
    public void Screen_OnTouch()
    {
        AudioManager.Instance.PlaySE("OkSe");
        SceneFadeManager.Instance.Load(GameData.Scene_Home, GameData.FadeSpeed, true);

  　}

  

}
