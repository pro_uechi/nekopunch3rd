﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GoToAvator: MonoBehaviour
{

    public void Onclick_GoToAvator()
    {
        SceneFadeManager.Instance.Load(GameData.Scene_Avatar, GameData.FadeSpeed, true);

    }


}
