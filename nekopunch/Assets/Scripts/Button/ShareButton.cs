﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShareButton : MonoBehaviour
{
    /// <summary>
    /// Shareボタン押下時
    /// </summary>
    public void ShareButtonOnClick()
    {
#if UNITY_EDITOR
        // シミュレータでは何もしない
        Debug.Log("UnityEditorでは動作しません");
#else
        SocialConnector.SocialConnector.Share("おすすめアプリ！", GameData.SHARE_URL);
#endif
    }
}
