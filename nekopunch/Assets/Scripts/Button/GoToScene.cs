﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


static class ModeConstants
{
	public const int MULTI = 0;
	public const int SOLO = 1;
}

public class GoToScene : MonoBehaviour
{
    //public void GoToAvatarScene()
    //{
    //    if (CheckSameScene(GameData.Scene_Avatar)) return;
    //    //AudioManager.Instance.PlaySE("OkSe")
    //    SceneFadeManager.Instance.Load(GameData.Scene_Avatar, GameData.FadeSpeed, true);
    //}

    //public void GoToBattleScene(int mode)
    //{
    //    if (CheckSameScene(GameData.Scene_Battle)) return;
    //    GameManager.Mode = mode;
    //    if (CheckAvatarScene(() => ConfirmAndGoToScene(GameData.Scene_Battle, true), () => ConfirmAndGoToScene(GameData.Scene_Battle, false))) return;
    //    SceneFadeManager.Instance.Load(GameData.Scene_Battle, GameData.FadeSpeed, true);
    //}

    //public void GoToSettingScene()
    //{
    //    if (CheckSameScene(GameData.Scene_Setting)) return;
    //    if (CheckAvatarScene(() => ConfirmAndGoToScene(GameData.Scene_Setting, true), () => ConfirmAndGoToScene(GameData.Scene_Setting, false))) return;
    //    SceneFadeManager.Instance.Load(GameData.Scene_Setting, GameData.FadeSpeed, true);
    //}

    //public void GoToRankingScene()
    //{
    //    if (CheckSameScene(GameData.Scene_Ranking)) return;
    //    if (CheckAvatarScene(() => ConfirmAndGoToScene(GameData.Scene_Ranking, true), () => ConfirmAndGoToScene(GameData.Scene_Ranking, false))) return;
    //    SceneFadeManager.Instance.Load(GameData.Scene_Ranking, GameData.FadeSpeed, true);
    //}

    //public void GoToSelectAreaScene()
    //{
    //    if (CheckSameScene(GameData.Scene_SelectArea)) return;
    //    // 現在の各IDを退避
    //    Initialize_04_SelectArea.tmpId[0] = GameData.UserData.RegionId;
    //    Initialize_04_SelectArea.tmpId[1] = GameData.UserData.PrefecturesId;
    //    Initialize_04_SelectArea.tmpId[2] = GameData.UserData.CharacterId;

    //    //AudioManager.Instance.PlaySE("OkSe")
    //    SceneFadeManager.Instance.Load(GameData.Scene_SelectArea, GameData.FadeSpeed, true);
    //}

    //public void GoToGachaScene()
    //{
    //    if (CheckSameScene(GameData.Scene_Gacha)) return;
    //    if (CheckAvatarScene(() => ConfirmAndGoToScene(GameData.Scene_Gacha, true), () => ConfirmAndGoToScene(GameData.Scene_Gacha, false))) return;
    //}


    public void GoToShopScene()
    {
        if (CheckSameScene(GameData.Scene_Shop)) return;

        SceneFadeManager.Instance.Load(GameData.Scene_Shop, GameData.FadeSpeed);

    }

    /// <summary>
    /// 衣装を確定してから遷移
    /// </summary>
    private void ConfirmAndGoToScene(string sceneName)
    {

        SceneFadeManager.Instance.Load(sceneName, GameData.FadeSpeed);
    }

    /// <summary>
    /// メッセージボックスを表示させる
    /// </summary>
    /// <param name="Yes_action"></param>
    /// <param name="No_action"></param>
    private void SetMessageBox(UnityAction Yes_action, UnityAction No_action)
    {
        GameObject msgBox = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
        msgBox.GetComponent<MessageBoxManager>().Initialize_YesNo("この衣装で\nいいですか？", Yes_action, No_action);
    }

    /// <summary>
    /// 着せ替え画面かどうかを確認し、着せ替え画面であればシーン移動の流れを変える
    /// </summary>
    private bool CheckAvatarScene(UnityAction Yes_action, UnityAction No_action)
    {
        if (!CheckSameScene(GameData.Scene_Avatar)) return false;
        SetMessageBox(Yes_action, No_action);
        return true;
    }

    /// <summary>
    /// 現在のシーンが引数のシーン名と同じかどうか
    /// </summary>
    /// <param name="scenename"></param>
    /// <returns></returns>
    private bool CheckSameScene(string scenename)
    {
        if (SceneManager.GetActiveScene().name == scenename) return true;

        return false;
    }

	/// <summary>
	/// 戻るボタン押下時
	/// </summary>
	public void ReturnButtonOnClick()
	{
        AudioManager.Instance.PlaySE("OkSe");
        SceneFadeManager.Instance.Load(GameData.Scene_Home, GameData.FadeSpeed);
	}


	///// <summary>
	///// 前回のデータがないかを確認
	///// </summary>
	//public void GoToSoloModeBattleCheck()
	//{
	//    //前回のデータがない場合
	//    if (GameManager.SoloData.WinCount == 0)
	//    {
	//        GoToBattleScene(ModeConstants.SOLO);
	//        return;
	//    }
	//    AudioManager.Instance.PlaySE("OkSe");
	//    string message = ResultProduction.MAX_WINS.ToString() + "戦中" + GameManager.SoloData.WinCount.ToString() + "勝中です。" + "\n続きから始めますか？";

	//    GameObject msgBox = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
	//    msgBox.GetComponent<MessageBoxManager>().Initialize_YesNo(message, GotoSoloModeBattle, ResetSoloData);
	//}

	///// <summary>
	///// ソロデータを初期化
	///// </summary>
	//public void ResetSoloData()
	//{
	//    GameManager.SoloData = new SoloModeData();
	//    SaveData.SetClass(SaveKey.SoloModeData, GameManager.SoloData);
	//    SaveData.Save();

	//    SceneFadeManager.Instance.Load(GameData.Scene_Home, GameData.FadeSpeed);
	//}

	///// <summary>
	/////　ソロデータを保持したまま、前回のつづきからプレイ
	///// </summary>
	//public void GotoSoloModeBattle()
	//{
	//    GameManager.Mode = ModeConstants.SOLO;
	//    //EquipManager.Instance.AnimatorSetBoolBattleScene(true);
	//    //AudioManager.Instance.PlaySE("OkSe")
	//    SceneFadeManager.Instance.Load(GameData.Scene_Battle, GameData.FadeSpeed, true);
	//}
}
