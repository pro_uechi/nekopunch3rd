﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecretButton : MonoBehaviour
{
    /// <summary>
    /// シークレットボタン押下時
    /// </summary>
    public void SecretButtonOnClick()
    {
        GameObject.Find("DontDestroy").transform.GetChild(0).gameObject.SetActive(true);
    }
}
