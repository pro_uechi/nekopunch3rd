﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BuyStarMessageButton : MonoBehaviour
{
    [SerializeField, Header("Initialize_12_Shop")]
    private Initialize_12_Shop _initialize_12_Shop;

    [SerializeField, Header("購入できないメッセージ")]
    private GameObject _canNotBuyMessage;

    [SerializeField, Header("購入完了メッセージ")]
    private GameObject _buyItemMessage;

    [SerializeField, Header("スターが足りませんメッセージ")]
    private GameObject _notStarMessage;

    [SerializeField, Header("アイテムのスター必要数")]
    private Text _itemStarText;



    private void OnEnable()
    {
        DisplayStarValue();
    }

    /// <summary>
    /// 必要スター数表示
    /// </summary>
    void DisplayStarValue()
    {
        var num = ItemManager.Instance.GetItemPrice_And_StarValue();
        _itemStarText.text = "×\t" + num.Item2;
    }


    /// <summary>
    /// スターでアイテムを購入する処理
    /// </summary>
    public void OnClickBuyItem_WithStar()
    {

        // スターで購入した際のボタンイベントに設定
        _initialize_12_Shop.ChangeStarButton();

        AudioManager.Instance.PlaySE("OkSe");

        // プレイヤーのステータスが最大値か調べる
        if (ItemManager.Instance.Check_MaxPlayerStatus())
        {
            this._canNotBuyMessage.SetActive(true);
            this.gameObject.SetActive(false);
            return;
        }

        // スターがあるか調べる
        if (ItemManager.Instance.CheckBuyItem_WithStar())
        {
            _buyItemMessage.SetActive(true);
            this.gameObject.SetActive(false);
        }
        // 無ければ、スターが足りないメッセージ表示
        else
        {
            _notStarMessage.SetActive(true);
            this.gameObject.SetActive(false);
        }
    }
}
