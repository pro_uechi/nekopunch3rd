﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdButton_UnityAds : MonoBehaviour
{
    /// <summary>
    /// 動画広告ボタン
    /// </summary>
    [SerializeField]
    private GameObject adButton = null;


    void Update()
    {
        if (UnityAdsManager.Instance.CanShowAd() && GameData.UserData.LoginFlg == 1)
        {
            if (adButton.activeSelf == false)
            {
                adButton.SetActive(true);
            }
        }
        else
        {
            if (adButton.activeSelf == true)
            {
                adButton.SetActive(false);
            }
        }
    }

    /// <summary>
    /// 広告ボタン押下時
    /// </summary>
    public void OnClickAdButton()
    {
        Debug.Log("AdButtonOnClick()");
        UnityAdsManager.Instance.ShowAd(AdType.Home);
    }
}
