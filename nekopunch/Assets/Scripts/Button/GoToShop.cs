﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoToShop : MonoBehaviour
{
    public void Onclick_GoToShop()
    {
        SceneFadeManager.Instance.Load(GameData.Scene_Shop, GameData.FadeSpeed, true);

        //SceneManager.LoadScene("12_Shop");
    }

}
