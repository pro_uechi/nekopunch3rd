﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BuyCoinMessageButton : MonoBehaviour
{
    [SerializeField, Header("Initialize_12_Shop")]
    private Initialize_12_Shop _initialize_12_Shop;

    [SerializeField, Header("購入できないメッセージ")]
    private GameObject _canNotBuyMessage;

    [SerializeField, Header("コインが足りませんメッセージ")]
    private GameObject _notCoinMessage;

    [SerializeField, Header("購入完了メッセージ")]
    private GameObject _buyItemMessage;

    [SerializeField, Header("アイテムの価格テキスト")]
    private Text _itemPriceText;


    private void OnEnable()
    {
        DisplayPrice();
    }

    /// <summary>
    /// 価格を表示
    /// </summary>
    void DisplayPrice()
    {
        var num = ItemManager.Instance.GetItemPrice_And_StarValue();
        _itemPriceText.text = "×\t" + num.Item1;
    }

    /// <summary>
    /// お金でアイテムを購入する処理
    /// </summary>
    public void OnClickBuyItem_WithMoney()
    {

        // お金で購入した際のボタンイベントに設定
        _initialize_12_Shop.ChangeCoinButton();

        AudioManager.Instance.PlaySE("OkSe");

        // プレイヤーのステータスが最大値か調べる
        if (ItemManager.Instance.Check_MaxPlayerStatus())
        {
            this._canNotBuyMessage.SetActive(true);
            this.gameObject.SetActive(false);
            return;
        }

        // お金があるか調べる
        if (ItemManager.Instance.CheckBuyItem_WithMoney())
        {
            _buyItemMessage.SetActive(true);
            this.gameObject.SetActive(false);
        }
        // 無ければ、お金が足りないメッセージ表示
        else
        {
            _notCoinMessage.SetActive(true);
            this.gameObject.SetActive(false);
        }
    }
}
