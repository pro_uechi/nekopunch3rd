﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MatchingUIAnim : MonoBehaviour
{

	/// <summary>
	///  時間差設定
	/// </summary>
	public static float waitTime = 0.25f;

	/// <summary>
	/// HUMのUI
	/// </summary>
	[SerializeField]
    private RectTransform HUMUI = null;

    /// <summary>
    /// CPUのUI
    /// </summary>
    [SerializeField]
    private RectTransform CPUUI = null;

	/// <summary>
	/// VS画像
	/// </summary>
	[SerializeField]
	private RectTransform VSImage= null;


	/// <summary>
	/// HUMのUI
	/// </summary>
	[SerializeField]
	private GameObject MatchUI = null;

	/// <summary>
	/// HUMのUI
	/// </summary>
	[SerializeField]
	private GameObject PLAYER= null;

	/// <summary>
	/// HUMのUI
	/// </summary>
	[SerializeField]
	private GameObject CPU = null;



	private void Start()
    {
		PLAYER = GameData.PlayerObjct;

		CPU = GameData.CpuObjct;


		InitPosition();
        //Animation();
    }

	private void OnEnable()
	{
		
		StartCoroutine(Setseq());
	}

	/// <summary>
	/// 位置を初期化
	/// </summary>
	private void InitPosition()
    {
        HUMUI.transform.localPosition = new Vector3( -1200.0f, 0.0f, 0.0f);
		PLAYER.transform.localPosition = new Vector3(-4.0f, 6.6f, 0.0f);

		CPUUI.transform.localPosition = new Vector3(1200.0f, 0.0f, 0.0f);
		CPU.transform.localPosition = new Vector3(-1.4f, 6.6f, 0.0f);
	}



	/// <summary>
	/// アニメーション
	/// </summary>
	IEnumerator Setseq()
    {
        float duration = 0.25f;

		Sequence seq = DOTween.Sequence();

		yield return new WaitForSeconds(0.25f);
		

		// プレイヤ側
		seq.Append(HUMUI.DOLocalMoveX(-50.0f, duration));
		seq.Append(PLAYER.transform.DOLocalMoveX(7.0f, duration));

		//yield return new WaitForSeconds(waitTime);
		yield return new WaitForSeconds(0.25f);

        // CPU側
        //seq.Append(CPUUI.DOLocalMoveX(39.0f, duration));
        seq.Append(CPUUI.DOLocalMoveX(85.0f, duration));
        seq.Append(CPU.transform.DOLocalMoveX(-7.0f, duration));

        // VS twennまでの間
        yield return new WaitForSeconds(0.2f);
		// VS
		seq.Append(VSImage.DOShakeScale(1.0f, duration));
		yield return new WaitForSeconds(3.2f);
		//seq.Append(MatchUI.transform.DOLocalMoveY(5000f, 0.2f));

		// マッチング画面をFADE
		seq.Append(MatchUI.GetComponent<CanvasGroup>().DOFade(0.0f, 0.2f));

		// CPUの大きさを設定
		CPU.transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);
		CPU.transform.position = new Vector3(-4.1f, 1.8f, 0.0f);

		// プレイヤーの大きさを設定
		PLAYER.transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);
		PLAYER.transform.position = new Vector3(3.8f, 1.8f, 0.0f);
		


		yield return null;

	}
}
