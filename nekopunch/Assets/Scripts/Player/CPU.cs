﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CPUState
{
    Normal,
    Cheat
}

public class CPU : Player
{
    /// <summary>
    /// 行動時間
    /// </summary>
    private float actionTime = 0.7f;

    /// <summary>
    /// 累計時間
    /// </summary>
    private float totalTime = 0;

    /// <summary>
    /// CPUが行動を起こす割合(0～1)
    /// </summary>
    private float actionRate = 0;

    /// <summary>
    /// チート行動を起こす割合
    /// </summary>
    private float cheatRate = 0;

    /// <summary>
    /// CPUの状態
    /// </summary>
    private CPUState cpuState;

    /// <summary>
    /// フレーム数
    /// </summary>
    private int frame = 0;

    /// <summary>
    /// 待ちフレーム
    /// </summary>
    private int waitFrame = 0;
    
    /// <summary>
    /// 攻撃のパーセント
    /// </summary>
    private int atkPercent = 0;

    /// <summary>
    /// IsChangeNormalの関数で使用
    /// </summary>
    private float normalTime = 0;


    private void Awake()
    {
        // アニメーターを代入
        animator = GetComponent<Animator>();
        //　初期化　
        hp = GameData.BattleData.CpuHpTable[GameData.CpuTableNo];
        // ノーマル状態から始める
        cpuState = CPUState.Normal;
    }

    private void Start()
    {
        // バトルタイプ設定
        SetBattleType(BattleType.Cpu);

        // 最初の待ちフレームを設定
        waitFrame = Random.Range(20, 61);
    }

    private void Update()
    {
        if (action)
        {
            // 状態で行動分岐
            switch (cpuState)
            {
                case CPUState.Normal:
                    Normal();
                    break;

                case CPUState.Cheat:
                    Cheat();
                    break;
            }
        }
    }

    /// <summary>
    /// ノーマル状態
    /// </summary>
    private void Normal()
    {
        // チートモードに変更できるか調べる
        if(IsChangeCheatMode())
        {
            cpuState = CPUState.Cheat;
            damageCount = 0;
            totalTime = 0;
            return;
        }

        totalTime += Time.deltaTime;

        if (totalTime >= actionTime)
        {
            totalTime = 0;

            // PlayerネコがPinch状態なら...
            if(GameData.PlayerObjct.GetComponent<HUM>().animator.GetCurrentAnimatorStateInfo(0).IsName("Pinch"))
            {
                // SpecialGaugeが100なら必殺技をうつ
                if (specialgauge >= 100)
                {
                    Special();
                }
                // それ以外なら通常攻撃
                else
                {
                    Attack();
                }

                return;
            }


            float rate = Random.Range(0.0f, 1.0f);
            if (actionRate > rate)
            {
                int actionType = Random.Range(0, 100);

                // actionタイプが設定した値以下なら攻撃
                if (actionType <= atkPercent)
                {
                    // 攻撃
                    Attack();

                }
                else
                {
                    // 回避
                    Avoidance();
                }
            }
        }
    }

    /// <summary>
    /// チート
    /// </summary>
    private void Cheat()
    {
        // ノーマルモードに変更できるか調べる
        if (IsChangeNormalMode())
        {
            cpuState = CPUState.Normal;
            damageCount = 0;
            normalTime = 0;
            frame = 0;
            return;
        }

        // PlayerネコがPinch状態なら...
        if (GameData.PlayerObjct.GetComponent<HUM>().animator.GetCurrentAnimatorStateInfo(0).IsName("Pinch"))
        {
            // SpecialGaugeが100なら必殺技をうつ
            if (specialgauge >= 100)
            {
                Special();
            }
            // それ以外なら通常攻撃
            else
            {
                Attack();
            }
            return;
        }

        if (GameData.PlayerObjct.GetComponent<HUM>().animator.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
        {
            Avoidance();
            return;
        }

        if (frame == waitFrame)
        {
            if (specialgauge >= 100)
            {
                Special();
            }
            else
            {
                Attack();
            }

            frame = 0;
            waitFrame = Random.Range(60, 120);
        }
        ++frame;
    }

    /// <summary>
    /// CPUの行動確率を設定する
    /// </summary>
    public void SetActionRate()
    {
		// レベルに応じて行動割合を決定
		actionRate = GameData.BattleData.actionProbabilityTable[GameData.CpuTableNo] * 0.01f;
        // 攻撃の割合を設定
        atkPercent = GameData.BattleData.actionAtkPercentTable[GameData.CpuTableNo];

	}

    /// <summary>
    /// 初期化処理
    /// </summary>
    public override void InitializeStatus()
    {
        hp = GameData.BattleData.CpuHpTable[GameData.CpuTableNo];
        attack = GameData.BattleData.CpuAtkTable[GameData.CpuTableNo];
        defense = GameData.BattleData.CpuDefTable[GameData.CpuTableNo];
        agility = GameData.BattleData.CpuSpsTable[GameData.CpuTableNo];

        ChangeState();
        normalTime = 0;
        damageCount = 0;
        waitFrame = 0;
        frame = 0;
    }

    /// <summary>
    /// レベルに応じて状態を変える
    /// </summary>
    void ChangeState()
    {
        
        switch (GameData.Levelrank)
        {
            case 1:
            case 2:
            case 3:
            case 4:
            case 6:
                cpuState = CPUState.Normal;
                break;
            case 5:
            case 7:
            case 8:
                cpuState = CPUState.Cheat;
                break;

        }

    }


    /// <summary>
    /// チートモードに変える事ができるか調べる（TRUE: 変えることができる FALSE: 変えることができない）
    /// </summary>
    private bool IsChangeCheatMode()
    {
        if (GameData.Levelrank < 2) { return false; }

        int num = 0;

        switch (GameData.Levelrank)
        {
            case 2:
                num = 7;
                break;
            case 3:
                num = 6;
                break;
            case 4:
                num = 5;
                break;
            case 5:
                num = 4;
                break;
            case 6:
                num = 4;
                break;
            case 7:
                num = 3;
                break;
            case 8:
                // 1～3の値が入る
                num = Random.Range(1, 4); 
                break;
        }

        if (num <= damageCount)
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// ノーマルモードに変える事ができるか調べる（TRUE: 変えることができる FALSE: 変えることができない）
    /// </summary>
    private bool IsChangeNormalMode()
    {
        int waitTime = 0;

        switch (GameData.Levelrank)
        {
            case 2:
                waitTime = 1;
                break;
            case 3:
                waitTime = 2;
                break;
            case 4:
                waitTime = 3;
                break;
            case 5:
                waitTime = 5;
                break;
            case 6:
                waitTime = 6;
                break;
            case 7:
                // 1～7秒の値が入る
                waitTime = Random.Range(1, 8);
                break;
            case 8:
                // 5～8秒の値が入る
                waitTime = Random.Range(5, 9);
                break;
        }

        normalTime += Time.deltaTime;


        if(waitTime <= normalTime)
        {
            return true;
        }

        return false;
    }
}
