﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 重心の位置を変更
/// </summary>
public class CenterOfMass : MonoBehaviour
{

	/// <summary>
	/// 重心の位置
	/// </summary>
	public Transform com;

	/// <summary>
	/// オブジェクトがEnableになったとき
	/// </summary>
	void OnEnable()
	{
		Rigidbody2D rb = GetComponent<Rigidbody2D>();
		rb.centerOfMass = com.localPosition;
	}
}