﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtkBox : MonoBehaviour
{
    public GameObject parent = null;


    /// <summary>
    /// 当たり判定(攻撃判定領域へ入った瞬間)
    /// </summary>
    /// <param name="col"></param>
    private void OnTriggerEnter2D(Collider2D col)
    {
		parent.GetComponent<Player>().OnTriggerEnter2D_AtkBox(col);
    }

	/// <summary>
	/// 当たり判定(攻撃判定領域に入ったままのとき)
	/// </summary>
	/// <param name="col"></param>
	private void OnTriggerStay2D(Collider2D col)
    {
        //parent.GetComponent<Player>().OnTriggerStay2D_HitBox(col);
    }

	/// <summary>
	/// 当たり判定(攻撃判定領域から抜けた時)
	/// </summary>
	/// <param name="col"></param>
	private void OnTriggerExit2D(Collider2D col)
    {
        //parent.GetComponent<Player>().OnTriggerExit2D_HitBox(col);
    }
}
