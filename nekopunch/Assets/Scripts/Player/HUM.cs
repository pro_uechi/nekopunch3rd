﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUM : Player
{
    private void Awake()
    {
        // アニメーターを代入
        animator = GetComponent<Animator>();
    }

    private void Start()
    {
        // アニメーターを代入
        animator = GetComponent<Animator>();

        // 敵を設定
        SetBattleType_old(BattleType.BalanceType);
    }

    /// <summary>
    /// 攻撃ボタン押下時
    /// </summary>
    public void OnClickAttackButton()
    {
		// 攻撃
		Attack();
    }

    /// <summary>
    /// 回避ボタン押下時
    /// </summary>
    public void OnClickAvoidanceButton()
    {
        // 回避
        Avoidance();
    }

    /// <summary>
    /// プレイヤーのステータス初期化
    /// </summary>
    public override void InitializeStatus()
    {
        hp = GameData.UserData. MyCharacterData.HP;
        attack = GameData.UserData. MyCharacterData.ATK;
        defense = GameData.UserData. MyCharacterData.DEF;
        agility = GameData.UserData. MyCharacterData.SPD;
    }
}