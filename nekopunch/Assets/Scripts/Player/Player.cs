﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public enum BattleType
    {
        None,
        BalanceType,
        AttackType,
        DefenseType,
        AgilityType,
        Cpu,
        Max
    }



    #region 変数

    /*
     * 攻撃力、防御力、素早さの3つのステータスがある。
     * それぞれ5段階まである。
     * 割り振れるステータスは総量がある。総量が9の場合(atk:3, def:3, agi:3), (atk:5, def:2, agi:2)など。
    */

    /// <summary>
    /// 基準段階
    /// </summary>
    private const int level_std = 3;

    /// <summary>
    /// 1段階分の倍率
    /// </summary>
    private const float level_mul = 0.25f;

    /// <summary>
    /// 追加するアニメーター再生速度の最大
    /// </summary>
    private const float ADD_MAX_ANIMATOR_SPEED = 0.85f;

    /// <summary>
    /// 攻撃力 
    /// </summary>
    public float attack { get; set; }

    /// <summary>
    /// 防御力
    /// </summary>
    public float defense { get; set; }

    /// <summary>
    /// 素早さ
    /// </summary>
    public float agility { get; set; }

    /// <summary>
    /// 体力
    /// </summary>
    public float hp { get; set; }
    //public float hp ;

    /// <summary>
    /// 累計時間
    /// </summary>
    private float[] totalTimes = { 0.0f, 0.0f };

    /// <summary>
    /// ピンチになる　攻撃ミス回数
    /// </summary>
    private int pinchMissCount = 2;

    /// <summary>
    /// 攻撃ミス回数
    /// </summary>
    private int miss = 0;

    /// <summary>
    /// 必殺技　ゲージ
    /// </summary>
    public int specialgauge = 0;

    /// <summary>
    /// 連続でダメージを受けた回数(CPUで使用)
    /// </summary>
    protected int damageCount = 0;

    /// <summary>
    /// クリティカルになる確率(%)
    /// </summary>
    private float criticalRate = 30;

    /// <summary>
    /// 自分のバトルタイプ
    /// </summary>
    private BattleType myBattleType = BattleType.BalanceType;

    /// <summary>
    /// アニメーター
    /// </summary>
    public Animator animator { get; set; }


    /// <summary>
    /// ラウンド勝利数
    /// </summary>
    [HideInInspector]
    public int wins = 0;

    /// <summary>
    /// 気を付けのアニメーターコントローラー
    /// </summary>
    public RuntimeAnimatorController StandingAnimCtrl;

    /// <summary>
    /// バトル時のアニメーターコントローラー
    /// </summary>
    public RuntimeAnimatorController BattleAnimCtrl;

    /// <summary>
    /// 敗北時のアニメーターコントローラー
    /// </summary>
    private RuntimeAnimatorController LoseAnimCtrl;


    /// <summary>
    /// ダメージエフェクトプレハブ
    /// </summary>
    [SerializeField]
    private GameObject damageEffectPrefab = null;


    /// <summary>
    /// ダメージエフェクトプレハブ
    /// </summary>
    [SerializeField]
    private GameObject stepEffectPrefab = null;



    /// <summary>
    /// SPエフェクトプレハブ
    /// </summary>
    [SerializeField]
    private GameObject EffectSPCutINPrefab = null;
    /// <summary>
    /// SPエフェクトプレハブ
    /// </summary>
    [SerializeField]
    private GameObject EffectLinePrefab = null;
    /// <summary>
    /// SPエフェクトプレハブ
    /// </summary>
    [SerializeField]
    private GameObject EffectTextPrefab = null;

    /// <summary>
    /// ダメージエフェクトプレハブ
    /// </summary>
    private GameObject EffectSPCutIN = null;
    private GameObject EffectLine = null;
    private GameObject EffectText = null;

    /// <summary>
    /// ダメージエフェクト
    /// </summary>
    private GameObject damageEffect = null;
    private GameObject stepEffect = null;


    /// <summary>
    /// 敗北フラグ
    /// </summary>
    [HideInInspector]
    public bool isLose = false;

    [HideInInspector]
    public bool isInterrupt = false;

    /// <summary>
    /// ボタン連打防止フラグ
    /// </summary>
    private bool buttonEnable = true;

    /// <summary>
    /// 動作するかどうか
    /// </summary>
    public bool action = false;

    /// <summary>
    /// ボタン連打防止時間
    /// </summary>
    private WaitForSeconds buttonEnablingDelay = new WaitForSeconds(0.7f);

    /// <summary>
    /// 敵
    /// </summary>
    [HideInInspector]
    public Player enemy = null;

    /// <summary>
    /// エフェクトを生成する位置
    /// </summary>
    public Vector3 Effectpos;

    #endregion

    /// <summary>
    /// バトルタイプを設定する
    /// </summary>
    /// <param name="type"></param>
    public void SetBattleType(BattleType type)
    {
        //GameData.UserData.examLevel = 
        //GameData.UserData.examStage = 

        //SetStatus(3, 3, 3);
        SetStatus();

    }

    /// <summary>
    /// ステータスを設定する
    /// </summary>
    /// <param name="atk">攻撃段階(1から5段階)</param>
    /// <param name="def">防御段階(1から5段階)</param>
    /// <param name="agi">素早さ段階(1から5段階)</param>
    private void SetStatus()
    {
        //GameData.BattleData.CpuAtkTable
        //GameData.BattleData.CpuHpTable
        //GameData.BattleData.CpuDefTable
        //GameData.BattleData.CpuSpsTable

        int atk_lv = 3;
        int def_lv = 3;
        int agi_lv = 3;

        if (atk_lv > 0 && def_lv > 0 && agi_lv > 0)
        {
            // 倍率計算
            float atk_mul = 1 + ((atk_lv - level_std) * level_mul);
            float def_mul = 1 + ((def_lv - level_std) * level_mul);
            float agi_mul = 1 + ((agi_lv - level_std) * level_mul);

            hp = GameData.BattleData.CpuHpTable[GameData.CpuTableNo];
            attack = GameData.BattleData.CpuAtkTable[GameData.CpuTableNo];
            defense = GameData.BattleData.CpuDefTable[GameData.CpuTableNo];
            agility = GameData.BattleData.CpuSpsTable[GameData.CpuTableNo];
            // 必殺技
            specialgauge = 0;

            // ミス回数
            miss = 0;
        }

    }


    /// <summary>
    /// バトルタイプを設定する
    /// </summary>
    /// <param name="type"></param>
    public void SetBattleType_old(BattleType type)
    {
        switch (type)
        {
            case BattleType.BalanceType:
                myBattleType = BattleType.BalanceType;
                SetStatus_old(3, 3, 3);
                break;

            case BattleType.AttackType:
                myBattleType = BattleType.AttackType;
                SetStatus_old(5, 2, 2);
                break;

            case BattleType.DefenseType:
                myBattleType = BattleType.DefenseType;
                SetStatus_old(2, 5, 2);
                break;

            case BattleType.AgilityType:
                myBattleType = BattleType.AgilityType;
                SetStatus_old(2, 2, 5);
                break;
        }
    }

    /// <summary>
    /// ステータスを設定する
    /// </summary>
    /// <param name="atk">攻撃段階(1から5段階)</param>
    /// <param name="def">防御段階(1から5段階)</param>
    /// <param name="agi">素早さ段階(1から5段階)</param>
    private void SetStatus_old(int atk_lv, int def_lv, int agi_lv)
    {
        if (atk_lv > 0 && def_lv > 0 && agi_lv > 0)
        {
            // 倍率計算
            float atk_mul = 1 + ((atk_lv - level_std) * level_mul);
            float def_mul = 1 + ((def_lv - level_std) * level_mul);
            float agi_mul = 1 + ((agi_lv - level_std) * level_mul);

            //// 基準値に倍率をかけて各ステータスを計算
            //attack = GameData.UserData.Status.ATK;
            //defense = GameData.UserData.Status.DEF;
            //agility = GameData.UserData.Status.SPD;
            //hp = GameData.UserData.Status.HP;
            specialgauge = 0;
            miss = 0;
        }
        else
        {
            Debug.LogError("無効な値です。");
        }
    }

    /// <summary>
    /// プレイヤーをリセットする
    /// </summary>
    public void ResetPlayer()
    {
        action = false;
        isLose = false;
        isInterrupt = false;
        specialgauge = 0;
        miss = 0;

        InitializeStatus();
        animator.speed = GetAgility();      // 素早さはアニメーターに反映
        AllResetAnimation();                // Triggerをリセットする
        animator.SetTrigger("Normal");      // AnimatorをNormal状態にする 
    }

    /// <summary>
    /// 継承先に実装を書く
    /// </summary>
    public virtual void InitializeStatus()
    {

    } 

    /// <summary>
    /// 攻撃
    /// </summary>
    public void Attack()
    {
        if (action)
        {
            if (animator.GetCurrentAnimatorStateInfo(0).IsName("Normal"))
            {
                if (hp > 0)
                {
                    action = false;
                    // 回避されたかチェック
                    StartCoroutine(CheckAvoided());
                }
            }
        }
    }

    /// <summary>
    /// 回避
    /// </summary>
    public void Avoidance()
    {
        if (action)
        {

            if (animator.GetCurrentAnimatorStateInfo(0).IsName("Normal"))
            {
                if (hp > 0)
                {
                    action = false;
                    Debug.Log("避けアニメ");

                    // SE
                    AudioManager.Instance.PlaySE("Deffenece");

                    // エフェクト
                    if (stepEffect != null)
                    {
                        Destroy(stepEffect);
                        stepEffect = null;
                    }

                    stepEffect = Instantiate(stepEffectPrefab, Effectpos, Quaternion.identity);
                    // ラグ
                    StartCoroutine(StepPLag());

                }
            }

        }
    }

    /// <summary>
    /// Stepアニメのラグ作成コルーチン
    /// </summary>
    /// <returns></returns>kaihi
    private IEnumerator StepPLag()
    {
        // ダメージを受けたら処理を終了する
        if (isInterrupt) { yield break; }

        /// <summary>
        /// STEP1
        /// </summary>
        animator.SetTrigger("Step1");

        while (!FinishedAnim("Step1"))
        {
            if (isInterrupt) { yield break; }
            yield return null;
        }

        /// <summary>
        /// STEP2
        /// </summary>
        animator.SetTrigger("Step2");


        float animationSpeed = GetAgility() / 10;
        int count = 0;

        while(count < 10)
        {
            if (isInterrupt) { yield break; }
            yield return new WaitForSeconds(animationSpeed);
            count++;
        }

        /// <summary>
        /// 相手が攻撃、SP発動中は戻らない
        /// </summary>
        if (enemy)
        {
            while (enemy.animator.GetCurrentAnimatorStateInfo(0).IsName("Attack") ||
            enemy.animator.GetCurrentAnimatorStateInfo(0).IsName("Special"))
            {
                if (isInterrupt && !enemy)
                {
                    yield break;
                }
                yield return null;
            }
        }

        /// <summary>
        /// STEP3
        /// </summary>
        animator.SetTrigger("Step3");

        while (!FinishedAnim("Step3"))
        {
            if (isInterrupt) { yield break; }
            yield return null;
        }

        /// <summary>
        /// NORMAL
        /// </summary>
        animator.SetTrigger("Normal");
        action = true;
    }



    /// <summary>
    /// 必殺技
    /// </summary>
    public void Special()
    {
        if (action)
        {
            if (animator.GetCurrentAnimatorStateInfo(0).IsName("Normal"))
            {
                if (hp > 0)
                {
                    action = false;
                    // SE
                    AudioManager.Instance.PlaySE("SpCat");

                    // 一旦リセット
                    specialgauge = 0;

                    // エフェクト
                    if (EffectSPCutIN != null)
                    {
                        Destroy(EffectSPCutIN);
                        EffectSPCutIN = null;
                    }

                    if (this.gameObject.name == "cat")
                    {
                        // 変更　ネコの色に合わせてカットインの種類を変える
                        int catid = SaveData.GetInt(SaveKey.UserCharacter);
                        string SPprefabStr = "Prefabs/Effect_Special_cutin" + string.Format("{0:00}", catid);
                        GameObject EffectSPCutINPrefab = (GameObject)Instantiate((GameObject)Resources.Load(SPprefabStr));
                        EffectSPCutIN = Instantiate(EffectSPCutINPrefab, new Vector3(-10.0f, 0.0f, 0.0f), Quaternion.identity);
                    }
                    else
                    {
                        // 変更　ネコの色に合わせてカットインの種類を変える
                        int catid = GameData.BattleData.CpuCatID[GameData.CpuTableNo];
                        string SPprefabStr = "Prefabs/Effect_Special_cutin" + string.Format("{0:00}", catid);
                        GameObject EffectSPCutINPrefab = (GameObject)Instantiate((GameObject)Resources.Load(SPprefabStr));
                        EffectSPCutIN = Instantiate(EffectSPCutINPrefab, new Vector3(10.0f, 0.0f, 0.0f), Quaternion.Euler(0, 180, 0));
                    }
                    // ラグ
                    StartCoroutine(SPLag());
                }

            }

        }
    }

    /// <summary>
    /// SPカットイン用のラグ作成コルーチン
    /// </summary>
    /// <returns></returns>
    private IEnumerator SPLag()
    {
        yield return new WaitForSeconds(0.8f);
        // ダメージを受けたら処理を終了する
        if (isInterrupt) { yield break; }
        // アニメーション　切り替え
        animator.SetTrigger("Special");


        if (EffectLine != null)
        {
            Destroy(EffectLine);
            EffectLine = null;
        }
        EffectLine = Instantiate(EffectLinePrefab, new Vector3(-3.0f, 12.0f, 0.0f), Quaternion.identity);
        Destroy(EffectLine, 1.0f);

        if (EffectText != null)
        {
            Destroy(EffectText);
            EffectText = null;
        }
        EffectText = Instantiate(EffectTextPrefab, new Vector3(0.0f, 0.0f, 0.0f), Quaternion.identity);
        EffectText.GetComponent<ParticleSystem>().Play();

        // 必殺技中はアニメーターの時間を1.0にする
        animator.speed = 1.0f;

        /// <summary>
        /// アニメーション終了　
        /// </summary>
        while (!FinishedAnim("Special"))
        {
            // ダメージを受けたら処理を抜ける
            if(isInterrupt)
            {
                // アニメーターの時間を元に戻す
                animator.speed = GetAgility();
                Destroy(EffectLine);
                Destroy(EffectText);
                yield break;
            }
            yield return null;
        }

        // アニメーターの時間を元に戻す
        animator.speed = GetAgility();
        action = true;
        animator.SetTrigger("Normal");
    }



    /// <summary>
    /// ボタン連打防止コルーチン
    /// </summary>
    /// <returns></returns>
    private IEnumerator ButtonEnabling()
    {
        //yield return buttonEnablingDelay;

        // アイドル状態になるまで待機
        while (!animator.GetCurrentAnimatorStateInfo(0).IsName("Normal"))
        {
            yield return null;
        }
        buttonEnable = true;
    }

    /// <summary>
    /// ダメージ
    /// </summary>
    private void Damage(float atk_enemy)
    {
        // hpを変更
        hp -= CalcDamage(atk_enemy);
        // ダメージを受けた回数を増やす
        damageCount++;

        // Hpが0未満なら0にする
        if (hp < 0) { hp = 0; }

        // 敵HPが０以下ならこれ以降処理を読まない
        if (enemy.hp <= 0 || animator.GetCurrentAnimatorStateInfo(0).IsName("Lose")) { return; }
        // 負け判定
        if (hp <= 0 && !isLose)
        {
            animator.SetTrigger("Lose");
            isLose = true;
        }
        else
        {
            /// <summary>
            /// 変更
            /// </summary> 
            
            // ダメージアニメーションが流れていなければ、流す
            if (!isInterrupt)
            {
                isInterrupt = true;
                StartCoroutine(DamageAnimation());
            }
        }
    }

    /// <summary>
    /// ダメージアニメーション再生
    /// </summary>
    /// <returns></returns>
    IEnumerator DamageAnimation()
    {
        action = false;
        animator.speed = 1.0f;                  // アニメーションの速度を元に1.0f
        animator.SetTrigger("Damage");

        while (!FinishedAnim("Damage"))
        {
            yield return null;
        }

        animator.SetTrigger("Normal");

        animator.speed = GetAgility();          // アニメーションの速度を元に戻す
        isInterrupt = false;
        action = true;
    }

    /// <summary>
    ///  ダメージ計算
    /// </summary>
    /// <param name="atk">攻撃力</param>
    /// <param name="def">防御力</param>
    /// <returns></returns>
    private float CalcDamage(float atk_enemy)
    {
        float damage = 0;

        /// <summary>
        /// ドラクエ式ダメージ計算
        /// </summary>
        float dmg = (atk_enemy / 2) - (defense / 4);

        // ダメージが0よりしたなら、0にする
        if(dmg < 0)
        {
            dmg = 0;
        }

        damage = dmg + 10;

        //PhoneDisplay.Instance.Log("ダメージ量" + damage);

        return damage;
    }

    /// <summary>
    /// 回避されたかチェック
    /// </summary>
    /// <returns></returns>
    IEnumerator CheckAvoided()
    {
        float befor_enemyhp = enemy.hp;

        // ダメージを受けたら処理を終了する
        if (isInterrupt) { yield break; }

        // 攻撃アニメーション
        animator.SetTrigger("Attack");

        while(!animator.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
        {
            if (isInterrupt) { Debug.LogError("AttackにAnimation移動中にDamageを受けた"); yield break; }
            yield return null;
        }

        // 攻撃アニメーションなら
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
        {
            // 待機
            float normalizedTime = 0;

            //0～1-> 0～100 % に変換される。
            while (normalizedTime < 0.8f)
            {
                // ダメージを受ければ処理を抜ける　
                if (isInterrupt) { yield break; }
                normalizedTime = animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
                yield return null;
            }

            action = true;

            // 回避されたかどうか判定
            if (enemy.hp == befor_enemyhp)
            {
                // 同時で攻撃して攻撃が外れたら、処理を終了する
                if (enemy.animator.GetCurrentAnimatorStateInfo(0).IsName("Attack") || enemy.animator.GetCurrentAnimatorStateInfo(0).IsName("Special"))
                {
                    action = true;
                    yield break;
                }

                Debug.Log("避けられました。");

                enemy.specialgauge += 30;
                ++miss;

                /// <summary>
                /// ピンチアニメーション
                /// </summary>
                if (miss >= pinchMissCount)
                {
                    action = false;
                    animator.SetTrigger("Pinch");
                    animator.speed = 1.0f;
                    miss = 0;
                    // ピンチアニメーションの終了を待機
                    while (!FinishedPinchAnim())
                    {
                        // ピンチ中にダメージを受けたら処理を抜ける　
                        if (isInterrupt) 
                        { 
                            animator.speed = GetAgility();                      // ピンチアニメーションの終了
                            yield break; 
                        }
                        yield return null;
                    }

                    animator.speed = GetAgility();
                    animator.SetTrigger("Normal");
                    action = true;
                }
            }
        }
    }

    /// <summary>
    /// 敗北アニメーション終了判定
    /// </summary>
    public bool FinishedPinchAnim()
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Pinch"))
        {
            float normalizedTime = animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
            if (normalizedTime >= 2)
            {
                return true;
            }
        }
        return false;
    }


    /// <summary>
    /// アニメーターコントローラーを切り替える
    /// </summary>
    /// <param name="type"></param>
    public void SetAnimatorController(AnimatorControllerType type)
    {
        switch (type)
        {
            case AnimatorControllerType.None:
                GetComponent<Animator>().runtimeAnimatorController = null;
                break;

            case AnimatorControllerType.Standing:
                GetComponent<Animator>().runtimeAnimatorController = StandingAnimCtrl;
                break;

            case AnimatorControllerType.Battle:
                GetComponent<Animator>().runtimeAnimatorController = BattleAnimCtrl;
                //// 素早さはアニメーターに反映
                //animator.speed = GetAgility();
                break;

            case AnimatorControllerType.Lose:
                GetComponent<Animator>().runtimeAnimatorController = LoseAnimCtrl;
                break;
        }
    }

    /// <summary>
    /// アニメーション終了判定
    /// </summary>
    public bool FinishedAnim(string aniName)
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName(aniName))
        {
            float normalizedTime = animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
            if (normalizedTime >= 1)
            {
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// スピードを取得する
    /// </summary>
    /// <returns></returns>
    public float GetAgility()
    {

        float agy = agility - GameData.UserData.MyCharacterData.Defalut_SPD;

        if(agy != 0)
        {
            /// <summary>
            /// アニメータに追加する速度計算
            /// (数値 - スピードのデフォルト値) × (追加する最大アニメーターの時間 / (スピードの最大値 - スピードのデフォルト値)))
            /// </summary>
            float addValue = (agility - GameData.UserData.MyCharacterData.Defalut_SPD) * 
                (ADD_MAX_ANIMATOR_SPEED / (GameData.UserData.MyCharacterData.MAX_SPD - GameData.UserData.MyCharacterData.Defalut_SPD));
            // アニメータースピード計測
            float speed = 1.0f + addValue;
            // 小数点第二位まで表示
            float aniSpeed = Mathf.Floor(speed * 100.0f) / 100.0f;
            //Debug.LogError(aniSpeed);
            return aniSpeed;
        }
        else
        {
            return 1.0f;
        }
    }


    /// <summary>
    /// 当たり判定(攻撃判定が入ったとき)
    /// </summary>
    /// <param name="col"></param>
    public void OnTriggerEnter2D_AtkBox(Collider2D col)
    {
        // まず無敵モードかどうか判定(デバッグ用)
        if (GameData.invincibleMode == false)
        {
            Debug.Log("攻撃が当たりました。");

            // SE
            AudioManager.Instance.PlaySE("Attack");

            // 通常攻撃で当たった時だけ。SPでは加算しない
            if (animator.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
            {
                // 当たればSPゲージ10%
                specialgauge += 20;
            }

            // 自分の攻撃力を引数にして相手のダメージメソッドを呼び出す
            float amount = 0;


            {
                Debug.Log("通常");
                amount = attack;
            }
            Debug.Log(amount);

            DmgBox dmgBox = col.GetComponent<DmgBox>();
            if (dmgBox != null)
            {
                col.GetComponent<DmgBox>().parent.GetComponent<Player>().Damage(amount);
            }

            // エフェクト
            if (damageEffect != null)
            {
                Destroy(damageEffect);
                damageEffect = null;
            }
            damageEffect = Instantiate(damageEffectPrefab, col.transform.position, Quaternion.identity);
        }
    }

    /// <summary>
    /// アニメーショントリガーを全てリセットする処理
    /// </summary>
    public void AllResetAnimation()
    {
        animator.ResetTrigger("Attack");
        animator.ResetTrigger("Damage");
        animator.ResetTrigger("Step1");
        animator.ResetTrigger("Step2");
        animator.ResetTrigger("Step3");
        animator.ResetTrigger("Special");
        animator.ResetTrigger("Win");
        animator.ResetTrigger("Lose");
        animator.ResetTrigger("Pinch");
        animator.ResetTrigger("Normal");
    }

    /// <summary>
    /// 当たり判定(攻撃判定が入っているとき)
    /// </summary>
    /// <param name="col"></param>
    public void OnTriggerStay2D_AtkBox(Collider2D col)
    {

    }

    /// <summary>
    /// 当たり判定(攻撃判定が抜けた時)
    /// </summary>
    /// <param name="col"></param>
    public void OnTriggerExit2D_AtkBox(Collider2D col)
    {

    }

    /// <summary>
    /// 当たり判定(やられ判定)
    /// </summary>
    /// <param name="col"></param>
    public void OnTriggerEnter2D_DmgBox(Collider2D col)
    {

    }
}
