﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ResultPanel : MonoBehaviour
{

    [SerializeField]
    private Text MoneyText = null;

    [SerializeField]
    private GameObject homeButton = null;

    [SerializeField]
    private Text ResultText = null;

    [SerializeField]
    private GameObject attackButton = null;

    [SerializeField]
    private GameObject avoidanceButton = null;


    private void OnEnable()
    {
        attackButton.SetActive(false);
        avoidanceButton.SetActive(false);
        homeButton.GetComponent<Button>().onClick.AddListener(HomeButtonOnClick);
        StartCoroutine(OnEnabelResultPanel());
    }

	/// <summary>
	/// 勝敗結果の設定
	/// </summary>
	IEnumerator OnEnabelResultPanel()
    {
		//if (GameData.BattleMode == BattleMode.none)
		{
			//if (GameManager.ticketIncrement != 0)
			//{
				ResultText.text = "Win！\n" + GameData.LevelName + "-" + GameData.UserData.examStage + "クリアー!";
			//}
			//else
			//{
			//ResultText.text = "負け";
			//}
			MoneyText.text = GameManager.moneyIncrement.ToString()　+ "ゲット！";
			yield return new WaitForSeconds(0.1f);


			// 勝利数リストクリア
			GameData.UserData.examStage =0 ;

			//AudioManager.Instance.PlaySE("BattleWinSe");
		}


    }

    /// <summary>
    /// ホームボタン押下時
    /// </summary>
    public void HomeButtonOnClick()
    {
        // 一定の勝利数に到達している && レビューしていない && 勝利している場合ポップアップを表示する
        if (GameData.UserData.battleCount % GameData.numWinToReview == 0 && GameData.UserData.reviewed == 0 && GameObject.Find("GameManager").GetComponent<GameManager>().IsWin())
        {
			//// ポップアップ表示
			//GameObject msgBox = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/ReviewMessageBox"));
			//msgBox.GetComponent<MessageBoxManager>().Initialize_Select(
			//    string.Empty,
			//    "あとで",
			//    "いつも遊んでくれてありがとう。\nレビューもらえたら嬉しいです♪",
			//    Review,
			//    delegate { SceneFadeManager.Instance.Load(GameData.Scene_Home, GameData.FadeSpeed); }
			//    );
			SceneFadeManager.Instance.Load(GameData.Scene_Home, GameData.FadeSpeed);
		}
        else
        {
            SceneFadeManager.Instance.Load(GameData.Scene_Home, GameData.FadeSpeed);
        }
    }

    /// <summary>
    /// レビュー
    /// </summary>
    private void Review()
    {
        GameData.UserData.reviewed = 1;
        SaveData.SetInt(SaveKey.Reviewed, GameData.UserData.reviewed);
        SaveData.Save();
#if UNITY_ANDROID
        Application.OpenURL(GameData.REVIEW_URL_ANDROID);
#else
        Application.OpenURL(GameData.REVIEW_URL_IPHONE);
#endif
    }
}
