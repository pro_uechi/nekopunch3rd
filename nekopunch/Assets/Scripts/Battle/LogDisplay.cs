﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

static class AvatarNumberConstants
{
	public const int PLAYER = 0;
	public const int CPU = 1;
}

public class LogDisplay : MonoBehaviour
{
	//デバッグ用
	public Text[] text = null;

    //ゲームで使うテキスト//
	public GameObject Win;
	public GameObject Lose;
	public GameObject Draw;
	public Text Timer;
	public Text RoundDelay;
	public Text WinMessage;
    public Text WinCharacterName;
    public Text SoloModeKOs;

	//各アバターごとに所持
	public Text[] AvoidableCount;
	public Text[] Type;
	public TextMeshProUGUI[] Name;
    public TextMeshProUGUI[] CharaName;
    public TextMeshProUGUI[] Rank;

	public void DebagLog(string str, int num)
    {
        text[num].text = str;
    }
}
