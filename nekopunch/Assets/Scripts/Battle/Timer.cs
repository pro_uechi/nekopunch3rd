﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour
{
    public Text timeText;
    private float time = 30.0f;
    private float initTime;
    public GameObject logDisp;
    private bool start = false;


    void Start()
    {
        time = 90.0f;
        initTime = time;
    }

    void Update()
    {
        if (start)
        {
            time -= Time.deltaTime;
            timeText.text = time.ToString("F0");
        }
    }

    public void GameStart()
    {
        start = true;
    }

    public void Stop()
    {
        start = false;
    }

    public void ResetTime()
    {
        start = false;
        time = initTime;
        timeText.text = time.ToString("F0");
    }

    public int GetTime()
    {
        int timeLeft = (int)time;
        return timeLeft;
    }

    public bool TimeOut()
    {
        if (time <= 0.0f)
        {
            start = false;
            timeText.text = 0.0f.ToString();
            logDisp.GetComponent<LogDisplay>().Win.SetActive(false);
            logDisp.GetComponent<LogDisplay>().Lose.SetActive(false);
            logDisp.GetComponent<LogDisplay>().Draw.SetActive(false);
            return true;
        }
        else
        {
            return false;
        }
    }
}
