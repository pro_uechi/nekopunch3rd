﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SoloModeData
{
    public int WinCount = 0;
    public int StageNumber = 0;
}

public class ResultProduction : MonoBehaviour
{
	//private Text Jewel;
	private GameObject CharacterIntroduction;
	//private GameObject PlayerResultObj;
	private GameObject SoloModeLoseObj;
	private Text ButtonText;
	//private int BaseJewelVolum;
	//紹介用のオブジェクトの大きさ
	private float CharacterIntroductionScale;
	//紹介用のオブジェクトが最終的にたどり着くポジション
	private Vector3 CharacterIntroductionTargetPos;
	//最高勝利数
	public static int MAX_WINS = 5;
	//ジュエルの増え幅
	private const int MONEY_INCREMWNT = 3000;

	//[SerializeField]
	//private GameObject Panel_Ranking = null;

	[SerializeField]
	private GameObject Panel_Result = null;

	private void Awake()
	{
		//CharacterIntroduction = transform.Find("CharacterIntroduction").gameObject;
		//CharacterIntroductionTargetPos = CharacterIntroduction.transform.position;
		//PlayerResultObj = transform.Find("PlayerResult").gameObject;
		//Jewel = transform.Find("PlayerResult/JewelText").GetComponent<Text>();
	}

	private void OnEnable()
	{
		Result();
	}

	/// <summary>
	/// ソロモードで勝ったとき
	/// </summary>
	private void SoloModee_Win()
	{
		SoloModeWinInit();

	}

	/// <summary>
	/// ソロモードで負けたとき
	/// </summary>
	private void SoloMode_Lose()
	{
		//CharacterIntroduction.SetActive(false);
		//PlayerResultObj.SetActive(false);
	}
	/// <summary>
	/// マルチバトル時のリザルド表示演出
	/// </summary>
	private void Result()
	{
		StartCoroutine(BattleModeProduction());
	}

	private void SoloModeWinInit()
	{

	}


	private void BattleModeInit()
	{
		int BeforPoint = GameData.UserData.Point - GameData.PointIncrement;
		//Jewel.text = "ジュエル:" + GameData.UserData.Money;
		//BaseJewelVolum = GameData.UserData.Money;

		InitCharacterIntroduction();
	}

	/// <summary>
	/// キャラクター紹介でつかうオブジェクトのポジションと大きさを検地
	/// </summary>
	private void InitCharacterIntroduction()
	{
		//キャラクター紹介用オブジェクトを見えないように小さくする
		CharacterIntroductionScale = 0.01f;
		//CharacterIntroduction.transform.localScale = new Vector2(CharacterIntroductionScale, CharacterIntroductionScale);

		Vector2 CharacterImagePos = Vector2.zero;
		//勝ったキャラクターの方から出現するように位置調整
		//if (GameManager.Battleresult == BattleResult.Win)
		//{
		//    CharacterImagePos = GameObject.Find("Canvas_UI/SafeArea/NeedMatchUI/Character001").transform.position;
		//}
		//else
		//{
		//    CharacterImagePos = GameObject.Find("Canvas_UI/SafeArea/NeedMatchUI/Character002").transform.position;
		//}

		//CharacterIntroduction.transform.position = CharacterImagePos;
	}

	/// <summary>
	/// バトルモードでの演出
	/// </summary>
	/// <returns></returns>
	IEnumerator BattleModeProduction()
	{
		BattleModeInit();
		const float WaiteTime = 1f;

		yield return new WaitForSeconds(WaiteTime);
		//yield return StartCoroutine(CharacterInfoProduction());
		Panel_Result.SetActive(true);
	}

	/// <summary>
	/// リザルドが表示されているオブジェクトをスライド
	/// </summary>
	/// <returns></returns>
	//IEnumerator MoveBand()
	//{
	//    int CenterPos = 0;
	//    while (PlayerResultObj.GetComponent<RectTransform>().position.x < CenterPos)
	//    {
	//        float MoveSpd = 50f;
	//        Vector3 pos = PlayerResultObj.GetComponent<RectTransform>().position;
	//        pos.x += MoveSpd * Time.deltaTime;
	//        pos.x = Mathf.Min(pos.x, CenterPos);
	//        PlayerResultObj.GetComponent<RectTransform>().position = pos;
	//        yield return null;
	//    }
	//}

	/// <summary>
	/// キャラクター紹介文の表示演出
	/// </summary>
	/// <returns></returns>
	IEnumerator CharacterInfoProduction()
	{
		//const float Spd = 10000f;
		//float FirstDistance = Vector2.Distance(CharacterIntroduction.transform.position, CharacterIntroductionTargetPos);

		//キャラクターを紹介するオブジェクトが真ん中まで来るまで繰り返す
		//while (CharacterIntroduction.transform.position != CharacterIntroductionTargetPos)
		//{
		//	Vector2 CharIntPos = CharacterIntroduction.transform.position;
		//	CharIntPos = Vector2.MoveTowards(CharIntPos, CharacterIntroductionTargetPos, Spd * Time.deltaTime);

		//	中心距離から大きさを測定
		//	float distance = Vector2.Distance(CharIntPos, CharacterIntroductionTargetPos);
		//	CharacterIntroductionScale = 1 - distance / FirstDistance;
		//	CharacterIntroduction.transform.localScale = new Vector2(CharacterIntroductionScale, CharacterIntroductionScale);

		//	ポジションを代入
		//	CharacterIntroduction.transform.position = CharIntPos;

		//	if (distance <= 0.1f)
		//	{
		//		Panel_Result.SetActive(true);
		//		yield break;
		//	}

			yield return null;
		//}


	}

	/// <summary>
	/// 数値を減算する
	/// </summary>
	/// <param name="BaseNum">もともとの数値</param>
	/// <param name="DecrimentSpd">数値が減る速度</param>
	/// <returns></returns>
	private float DecrimentNumber(float BaseNum, float DecrimentSpd)
	{
		BaseNum -= Time.deltaTime * DecrimentSpd;
		BaseNum = Mathf.Max(BaseNum, 0);

		return BaseNum;
	}



	private void ReturnHome()
	{
		SceneFadeManager.Instance.Load(GameData.Scene_Home, GameData.FadeSpeed, true);
	}

	private void ClearSoloData()
	{
		//GameManager.SoloData = new SoloModeData();
		//SaveData.SetClass(SaveKey.SoloModeData, GameManager.SoloData);
		//SaveData.Save();
	}

	//private void GoToBattle()
	//{
	//    SceneFadeManager.Instance.Load(GameData.Scene_Battle, GameData.FadeSpeed, true);
	//}
}

