﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeRank : MonoBehaviour {


    private void Start()
    {

    }

    public void DebugRankUp()
    {
        GameData.UserData.UserLevel++;
        
        // ユーザー情報更新
        //StartCoroutine(DBAccessManager.Instance.UpdateUserData());

    }

    public void DebugRankDown()
    {
        GameData.UserData.UserLevel--;
        GameData.UserData.UserLevel = Mathf.Max(1 , GameData.UserData.UserLevel);
        
        // ユーザー情報更新
        //StartCoroutine(DBAccessManager.Instance.UpdateUserData());
    }
}
