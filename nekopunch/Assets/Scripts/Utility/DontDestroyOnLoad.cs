﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class DontDestroyOnLoad : MonoBehaviour
{
    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    /// <summary>
    /// 終了処理
    /// </summary>
    private void OnApplicationQuit()
    {

#if UNITY_EDITOR
        SaveData.SetDateTime(SaveKey.HomeTime, DateTime.Now);
        SaveData.Save();
#endif
    }

    /// <summary>
    /// バックグランド処理
    /// </summary>
    /// <param name="pauseStatus"></param>
    private void OnApplicationPause(bool pauseStatus)
    {
        // ホーム画面ならこの処理を読む
        if (IsHomeScene())
        {
            // バックグラウンド
            if (pauseStatus)
            {
                ItemManager.Instance.LeaveHomeInitialize(true);
                //PhoneDisplay.Instance.Log("バックグランド");

            }
            // 復帰した
            else
            {
                ItemManager.Instance.StartAnimation((int)(DateTime.Now - SaveData.GetDateTime(SaveKey.HomeTime)).TotalSeconds);
                //PhoneDisplay.Instance.Log("バックグラウンド終了");
            }
        }
    }

    /// <summary>
    /// 現在ホーム画面にいるか？（TRUE: ホーム画面 FALSE: ホーム画面以外）
    /// </summary>
    private bool IsHomeScene()
    {
        if( SceneManager.GetActiveScene().name == GameData.Scene_Battle ||
            SceneManager.GetActiveScene().name == GameData.Scene_Title ||
            SceneManager.GetActiveScene().name == GameData.Scene_Manager ||
            SceneManager.GetActiveScene().name == GameData.Scene_Splash ||
            SceneManager.GetActiveScene().name == GameData.Scene_DataCheck ||
            GameData.IsHomeAbs)
        {
            return false;
        }
        return true;
    }
}
