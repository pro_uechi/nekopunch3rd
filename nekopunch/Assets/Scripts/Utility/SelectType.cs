﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum TypeName
{
    Nomal,
    Attack,
    Defense,
    Tough
}


public class SelectType : MonoBehaviour
{
    private TypeName NowType;
    public Text Text_Type;
    public string[] TypeNames;

    // Use this for initialization
    void Start()
    {
        //Text_Type.text = TypeNames[(int)GameData.UserData.ChooseType];
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void PushTypeButton(int TypeNum)
    {
        AudioManager.Instance.PlaySE("OkSe");
        //GameData.UserData.ChooseType = (TypeName)TypeNum;
        Text_Type.text = TypeNames[TypeNum];
    }

}
