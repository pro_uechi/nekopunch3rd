﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AutoyaFramework;
using TMPro;

using UnityEngine.SceneManagement;

public class SetTextMeshPro : MonoBehaviour
{
    /// <summary>
    /// nikumaruフォントかどうか
    /// </summary>
    [SerializeField]
    private bool isNikumaru = true;

    private TextMeshProUGUI textMeshProUGUI = null;


    private void Start()
    {
        textMeshProUGUI = GetComponent<TextMeshProUGUI>();

        if (isNikumaru)
        {
            textMeshProUGUI.font = Resources.Load("07_nukumaru SDF", typeof(TMP_FontAsset)) as TMP_FontAsset;
            textMeshProUGUI.UpdateFontAsset();
            textMeshProUGUI.ForceMeshUpdate();

            //// アセットバンドル内のアセット読み込み
            //Autoya.AssetBundle_LoadAsset<TMP_FontAsset>
            //(
            //    assetName: "Assets/AssetBundleMaterials/Fonts/07_nukumaru SDF.asset",
            //    loadSucceeded: (name, tmp_font) =>
            //    {
            //        Debug.Log(name + "アセットの読み込み 成功");
            //        textMeshProUGUI.font = tmp_font;
            //        GetComponent<Material>() = Resources.Load("TMP_SDF", typeof(Shader)) as Shader;
            //    },
            //    loadFailed: (name, err, reason, status) =>
            //    {
            //        Debug.LogFormat(name + "アセットの読み込み 失敗\n\n{0}\n\n{1}\n\n{2}", err, reason, status);
            //    }
            //);
        }
        else
        {
            // アセットバンドル内のアセット読み込み
            Autoya.AssetBundle_LoadAsset<TMP_FontAsset>
            (
                assetName: "Assets/AssetBundleMaterials/Fonts/GenEiNuGothic-EB SDF.asset",
                loadSucceeded: (name, tmp_font) =>
                {
                    Debug.Log(name + "アセットの読み込み 成功");
                    textMeshProUGUI.font = tmp_font;
                },
                loadFailed: (name, err, reason, status) =>
                {
                    Debug.LogFormat(name + "アセットの読み込み 失敗\n\n{0}\n\n{1}\n\n{2}", err, reason, status);
                }
            );
        }

        textMeshProUGUI.UpdateFontAsset();
        textMeshProUGUI.ForceMeshUpdate();
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            // デバッグ用
            // 現在のScene名を取得する
            Scene loadScene = SceneManager.GetActiveScene();
            //// Sceneの読み直し
            SceneManager.LoadScene(loadScene.name);
        }
    }
}
