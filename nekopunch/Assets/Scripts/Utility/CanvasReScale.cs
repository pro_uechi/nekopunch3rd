﻿using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class CanvasReScale : MonoBehaviour
{
	private void Start()
	{
		// referenceResolutionの想定サイズを変更する
		var scaler =  this.GetComponent<CanvasScaler>();
		//Undo.RecordObject(scaler, "Canvas Scaler Default");

		float ResolutionX = scaler.referenceResolution.x;
		float ResolutionY = scaler.referenceResolution.y;

		if ( (ResolutionX / ResolutionY) >  (Screen.width / Screen.height))
		{
			// 実行画面サイズの比率に合わせる
			 ResolutionX = scaler.referenceResolution.x;
			 ResolutionY = (scaler.referenceResolution.x * Screen.height) / Screen.width;

			scaler.referenceResolution = new Vector2(ResolutionX, ResolutionY);

			//scaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
			//scaler.screenMatchMode = CanvasScaler.ScreenMatchMode.Expand;
			//scaler.referencePixelsPerUnit = 100;

		}

	}
}