﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bottom : MonoBehaviour {

	public GameObject first_node;
	public GameObject second_node;
	public GameObject last_node;

	public float first_pos;
	public float last_pos;
	public float interval_size;

	//public static bool scroll_flg;

	void Awake()
	{
		first_pos = first_node.transform.position.y;
		last_pos = last_node.transform.position.y;
		interval_size = Mathf.Abs(first_pos - second_node.transform.position.y);
	}

	void Start()
	{
		//first_pos = first_node.transform.position.y;
		//last_pos = last_node.transform.position.y;
		//interval_size = Mathf.Abs(first_pos - second_node.transform.position.y);
	}
   
	void Update () {
		chack_buttom();
	}

	public void SetBottom(float bottom_size)
	{
		gameObject.GetComponent<RectTransform>().offsetMin = new Vector2(0, bottom_size);
	}

	void chack_buttom()
	{

	}


	public float GetMaxPosY()
	{
		return first_pos + interval_size;
	}

	public float GetMinPosY()
	{
		return last_pos;
	}

	public float GetIntervalSize()
	{
		return interval_size;
	}

	public float GetAllSize()
	{
		return Mathf.Abs(first_pos-last_pos);
	}
}
