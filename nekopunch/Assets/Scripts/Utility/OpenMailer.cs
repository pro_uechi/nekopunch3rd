﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenMailer : MonoBehaviour
{
    //メール
    private const string TITLE = "【ネコぱんち】";
    private const string NEW_LINE_STRING = "\n";
    private const string ADRESS_IOS = "support_ios@prolead.co.jp";
    private const string ADRESS_ANDROID = "support.android@prolead.co.jp";
    private const string UNLOCK_ADRESS_START = "返信のメールは｢";
    private const string UNLOCK_ADRESS_END = "｣から届きます。迷惑メール設定をしないようお願い致します。\n\n";
    private const string CAUTION_STATEMENT = "----以下の内容は変更しないで下さい----";//元々は---- Do not change ----

    private static string MAIL_ADRESS = "";

    /// <summary>
    /// サポート用のメーラーを起動する
    /// </summary>
    public static void OpenSupportMail(string subtitle, string announce)
    {
        //タイトルはアプリ名
        string subject = TITLE + subtitle;
        string attention = "";

#if UNITY_ANDROID
        // アドレスを変更
        MAIL_ADRESS = ADRESS_ANDROID;
        // 端末名を取得
        string deviceName = SystemInfo.deviceModel;
#elif UNITY_IPHONE
        // アドレスを変更
        MAIL_ADRESS = ADRESS_IOS;        
        // 端末名を取得
        string deviceName = SystemInfo.deviceName;
#else
        // アドレスを変更
        MAIL_ADRESS = "";
        // 端末名を取得
        string deviceName = "other";
#endif
        // 注意文
        attention = UNLOCK_ADRESS_START + MAIL_ADRESS + UNLOCK_ADRESS_END;

        string body = attention + announce + NEW_LINE_STRING + NEW_LINE_STRING;

        //OS、アプリバージョンの記載
        body += NEW_LINE_STRING + NEW_LINE_STRING + NEW_LINE_STRING + CAUTION_STATEMENT + NEW_LINE_STRING;
        //body += "お客様ID : " + "XZ"+GameData.UserData.UserId.ToString("D9")+"HP" + NEW_LINE_STRING;
        body += "お客様ID : " + "WZ" + GameData.UserData.UserId + "NP" + NEW_LINE_STRING;
        //body += "お名前   : " + GameData.UserData.UserName + NEW_LINE_STRING;
        body += "Device   : " + deviceName + NEW_LINE_STRING;
        body += "OS       : " + SystemInfo.operatingSystem + NEW_LINE_STRING;
        body += "Ver      : " + Application.version + NEW_LINE_STRING;

        //エスケープ処理
        body = EscapeStr(body);
        subject = EscapeStr(subject);

        //メーラーを起動
        Application.OpenURL("mailto:" + MAIL_ADRESS + "?subject=" + subject + "&body=" + body);
    }

    private static string EscapeStr(string str)
    {
        return WWW.EscapeURL(str).Replace("+", "%20");
    }
}
