﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhoneDisplay : SingletonMonoBehaviour<PhoneDisplay>
{
    private int _number = 0;

    [SerializeField]
    private GameObject _content;

    [SerializeField]
    private GameObject _debugText;

    //private void Start()
    //{
    //    Log("Screen Width : " + Screen.width);
    //    Log("Screen  height: " + Screen.height);
        
    //}

    public void Log(string s)
    {
        var obj = Instantiate(_debugText, _content.transform);
        var myText  = obj.GetComponent<Text>();
        myText.text = ++_number + ":" + s + "\n";
    }
}
