﻿#if UNITY_ANDROID
using UnityEngine;
#endif

#if UNITY_EDITOR
using UnityEditor;
#endif

public static class AppVersionCheck_Android
{
    public static int GetVersionCode_Android()
    {
#if UNITY_EDITOR
        return PlayerSettings.Android.bundleVersionCode;
#elif UNITY_ANDROID
        using (var packageInfo = GetPackageInfo()) {
            return packageInfo.Get<int>("versionCode");
        }
#else
        return 0;
#endif
    }

#if UNITY_ANDROID
    static AndroidJavaObject GetPackageInfo()
    {
        using (var unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        using (var currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
        using (var context = currentActivity.Call<AndroidJavaObject>("getApplicationContext"))
        using (var packageManager = context.Call<AndroidJavaObject>("getPackageManager"))
        using (var packageManagerClass = new AndroidJavaClass("android.content.pm.PackageManager"))
        {
            string packageName = context.Call<string>("getPackageName");
            int activities = packageManagerClass.GetStatic<int>("GET_ACTIVITIES");
            return packageManager.Call<AndroidJavaObject>("getPackageInfo", packageName, activities);
        }
    }
#endif
}
