﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGroundScaler : MonoBehaviour
{

    /// <summary>
    /// 縦に合わせるか横に合わせるか
    /// </summary>
    [SerializeField]
    private bool isPortrait = false;


    private void Start()
    {
        Vector2 standardResolution = GetComponent<RectTransform>().sizeDelta;

        // 倍率を計算して背景画像の大きさを調整
        float magnification = 0;

        if (isPortrait)
        {
            float height = Screen.height;
            magnification = height / standardResolution.y;
        }
        else
        {
            float width = Screen.width;
            magnification = width / standardResolution.x;
        }

        GetComponent<RectTransform>().sizeDelta = new Vector2(standardResolution.x * magnification, standardResolution.y * magnification);
    }
}
