﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class AppVersionCheck : MonoBehaviour
{
    /// <summary>
    /// 最新バージョンかどうかチェック
    /// </summary>
    /// <param name="storeVersion_in"></param>
    /// <returns></returns>
    public static bool CheckLatestVersion()
    {
        int userVersion = 0;
#if UNITY_IPHONE
        userVersion = AppVersionCheck_iOS.GetBundleVersion_iOS();
#elif UNITY_ANDROID
        userVersion = AppVersionCheck_Android.GetVersionCode_Android();
#endif

        int storeVersion = ConvertCompareInteger(GameData.AppInfoData.appVersion);

        Debug.Log("userVersion=" + userVersion + "storeVersion=" + storeVersion);

        if (userVersion < storeVersion)
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// バージョン番号を比較用整数に変換
    /// メジャーバージョン x 10000 + マイナーバージョン x 100   + パッチバージョン
    /// バッチバージョンは比較対象からはずす
    /// </summary>
    /// <param name="bundleVersion"></param>
    /// <returns></returns>
    private static int ConvertCompareInteger(string bundleVersion)
    {
        char[] delimiterChar = { '.' };
        string[] splitString = bundleVersion.Split(delimiterChar);

        // バージョン番号x.y.zのz部分は比較しない
        int compareString = int.Parse(splitString[0]) * 10000 + int.Parse(splitString[1]) * 100;
        return compareString;
    }

    /// <summary>
    /// バージョン番号を返す(x.y.zの形)
    /// </summary>
    /// <returns></returns>
    public static string IntToString()
    {
        int userVersion = 0;
        string str = "";

#if UNITY_IPHONE && !UNITY_EDITOR
        userVersion = AppVersionCheck_iOS.GetBundleVersion_iOS();
        str = userVersion.ToString();
        str = str.Substring(0, 2) + "." + str.Substring(2, 2) + "." + str.Substring(4, 2);
        return str;
#elif UNITY_ANDROID && !UNITY_EDITOR
        userVersion = AppVersionCheck_Android.GetVersionCode_Android();
        str = userVersion.ToString();
        str = str.Substring(0, 2) + "." + str.Substring(2, 2) + "." + str.Substring(4, 2);
        return str;
#elif UNITY_EDITOR
        return Application.version;
#endif
    }
}
