﻿#if UNITY_IOS
using System.Runtime.InteropServices;
#endif

#if UNITY_EDITOR
using UnityEditor;
#endif

public static class AppVersionCheck_iOS
{
    public static int GetBundleVersion_iOS()
    {
#if UNITY_EDITOR
        return int.Parse(PlayerSettings.iOS.buildNumber);
#elif UNITY_IOS
        return int.Parse(GetBundleVersion());
#else
        return 0;
#endif
    }

#if UNITY_IOS
    [DllImport("__Internal")]
    static extern string GetBundleVersion();
#endif
}
