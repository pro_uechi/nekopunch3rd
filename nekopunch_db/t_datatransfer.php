<?php

// DB接続
require_once("mysql_connect.php");

// 処理分岐
switch ($_REQUEST["process"])
{
	case "INSERT":
		InsertData($pdo);
    	break;

	case "SELECT":
		SelectData($pdo);
		break;
}

function InsertData($pdo)
{
	$query = "INSERT IGNORE INTO T_DATATRANSFER (
	DATATRANSFERID, 
	USERID, 
	USERNAME,
	CREATEDATE
	) 
	VALUES (?, ?, ?, current_timestamp)";

    if ($stmt = $pdo->prepare($query))
    {
        // パラメータをバインド
        $stmt->bindValue(1, $_REQUEST["id"]);
        $stmt->bindValue(2, $_REQUEST["userid"]);
        $stmt->bindValue(3, $_REQUEST["username"]);
       
        // SQL実行
        $stmt->execute();
    }
}

function SelectData($pdo)
{
	$query = "SELECT * FROM `T_DATATRANSFER` WHERE `DATATRANSFERID` = ?";

    if ($stmt = $pdo->prepare($query))
    {
        // パラメータをバインド
        $stmt->bindValue(1, $_REQUEST["id"]);
        
        // SQL実行
        $stmt->execute();
        
        // 結果を格納
        while ($row = $stmt->fetch())
        {
            $info[] = array (
                            "Id" => $row['DATATRANSFERID'],
                            "UserId" => $row['USERID'],
                            "UserName" => $row['USERNAME'],
							"CreateDate" => $row['CREATEDATE']
                          );
        }

        // 出力結果が空の時はnull、JSON形式で変換
        if (empty($info))
        {
           $info = null;
           echo json_encode($info);
        }
        else
        {
           echo json_encode($info);
        }
    }
}