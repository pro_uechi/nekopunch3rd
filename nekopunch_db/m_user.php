<?php

// DB接続
require_once("mysql_connect.php");

// 処理分岐
switch ($_REQUEST["process"])
{
    case "INSERT":
        Optimaize_Table($pdo);
        // ユーザー新規登録
        InsertUserData($pdo, GetMaxUserId($pdo));
        break;
    case "UPDATE":
        InsertUserDataLog($pdo);
        // ユーザー情報更新
        UpdateUserData($pdo);
        break;
    case "SELECT":
        // ユーザー情報取得
        SelectUserData($pdo);
        break;
    // case "LOGINDATE":
    //     // ログイン日時更新
    //     UpdateLoginDate($pdo);
    //     break;
	case "SELECTLOADFLG":
        // ロードフラグ取得
        SelectLoadFlg($pdo);
        break;
    case "CLEARELOADFLG":
        // ロードフラグの更新
        ClearLoadFlg($pdo);
        break;

    case "DELETE":
        // ユーザ情報削除
        DeleteUserData($pdo);
        break;
}



///-----------------------------------------
// 対象のユーザーの情報を取得する
///-----------------------------------------
function SelectUserData($pdo)
{
	$query = "SELECT * FROM M_USER WHERE USERID = ?";

    if ($stmt = $pdo->prepare($query))
    {
        // パラメータをバインド
        $stmt->bindValue(1, $_REQUEST["userId"]);
        // SQL実行
        $stmt->execute();
        // 結果を格納
        while ($row = $stmt->fetch())
        {
            $userinfo[] = array (
                            "UserId" => $row['USERID'],
                            "UserLevel" => $row['USERLEVEL'],
                            "Money" => $row['MONEY'],
                            "StarCount" => $row['STAR'],

                            "HP" => $row['HP'],
                            "ATK" => $row['ATK'],
                            "DEF" => $row['DEF'],
                            "SPD" => $row['SPD'],

                            "CreateDate" => $row['CREATEDATE'],
                            "UpdateDate" => $row['UPDATEDATE'],
                          );
        }

        // 出力結果が空の時はnull、JSON形式で変換
        if (empty($userinfo))
        {
           $userinfo = null;
           echo json_encode($userinfo);
        }
        else
        {
           echo json_encode($userinfo);
        }
    }
}


///-----------------------------------------
// テーブル最適化
///-----------------------------------------
function Optimaize_Table($pdo)
{
    //$query = "OPTIMIZE TABLE `M_USER`,`T_RANKING`";
    $query = "OPTIMIZE TABLE `M_USER`";

    if ($stmt = $pdo->prepare($query))
    {
        // SQL実行
        $stmt->execute();
    }
}

///-----------------------------------------
// 最大値＋１のユーザーIDを取得する
///-----------------------------------------
function GetMaxUserId($pdo)
{
    $query = "SELECT IFNULL( MAX( USERID ) +1, 1 ) AS MAXUSERID FROM M_USER";
    $stmt = $pdo->query($query);
    $value = $stmt->fetchColumn();

    return $value;
}





///-----------------------------------------
// 新規ユーザーを登録する
///-----------------------------------------
function InsertUserData($pdo, $maxId)
{
     $query = "INSERT INTO M_USER(USERID, USERNAME, CREATEDATE, UPDATEDATE,  OS, APPVERSION) VALUES (?, ?, current_timestamp, current_timestamp,  ?, ?)";

    if ($stmt = $pdo->prepare($query))
    {
        // パラメータをバインド
        $stmt->bindValue(1, $maxId, PDO::PARAM_INT);
        $stmt->bindValue(2, $_REQUEST["userName"]);
        $stmt->bindValue(3, $_REQUEST["os"]);
        $stmt->bindValue(4, $_REQUEST["appVersion"]);
        // SQL実行
        $stmt->execute();
    }
    echo $maxId;
}



///-----------------------------------------
// ユーザーがログインした日時を更新する
///-----------------------------------------
// function UpdateLoginDate($pdo)
// {
//     $query = "UPDATE M_USER SET LOGINFLG = (SELECT CASE WHEN DATE_FORMAT(UPDATEDATE, '%d') <> DATE_FORMAT(NOW(), '%d') THEN 0 ELSE LOGINFLG END FROM (SELECT * FROM M_USER) AS A WHERE USERID = ?) ,  UPDATEDATE = CURRENT_TIMESTAMP WHERE USERID = ?";

//     if ($stmt = $pdo->prepare($query))
//     {
//         // パラメータをバインド
//         $stmt->bindValue(1, $_REQUEST["userId"]);
//         $stmt->bindValue(2, $_REQUEST["userId"]);
//         // SQL実行
//         $stmt->execute();
//     }
// }

///-----------------------------------------
// ロードフラグを取得する
///-----------------------------------------
function SelectLoadFlg($pdo)
{
	$query = "SELECT * FROM M_USER WHERE USERID = ?";

    if ($stmt = $pdo->prepare($query))
    {
        // パラメータをバインド
        $stmt->bindValue(1, $_REQUEST["userId"]);
        // SQL実行
        $stmt->execute();
        // 結果を格納
        while ($row = $stmt->fetch())
        {
            $flg = $row['LOAD'];
        }

		echo $flg;
    }
}

///-----------------------------------------
// ユーザーの強制ロードフラグを更新する
///-----------------------------------------
function ClearLoadFlg($pdo)
{
    $query = "UPDATE `M_USER` SET  `LOAD` =  '0' WHERE  `M_USER`.`USERID` = ?";

    if ($stmt = $pdo->prepare($query))
    {
        // パラメータをバインド
        $stmt->bindValue(1, $_REQUEST["userId"]);
        // SQL実行
        $stmt->execute();
    }
}


///-----------------------------------------
// ユーザー情報を更新する
///-----------------------------------------
function UpdateUserData($pdo)
{
    $query = "UPDATE M_USER SET USERNAME = ?, USERLEVEL = ?, MONEY = ?, STAR = ?, HP=?,ATK=?,DEF=?,SPD=?,OS = ?, APPVERSION = ?, UPDATEDATE = current_timestamp WHERE USERID = ?";
    
    if ($stmt = $pdo->prepare($query))
    {
        // パラメータをバインド
        $stmt->bindValue(1, $_REQUEST["userName"]);
        $stmt->bindValue(2, $_REQUEST["userlevel"]);
        $stmt->bindValue(3, $_REQUEST["money"]);
        $stmt->bindValue(4, $_REQUEST["star"]);
        
        $stmt->bindValue(5, $_REQUEST["hp"]);
        $stmt->bindValue(6, $_REQUEST["atk"]);
        $stmt->bindValue(7, $_REQUEST["def"]);
        $stmt->bindValue(8, $_REQUEST["spd"]);

		$stmt->bindValue(9, $_REQUEST["os"]);
		$stmt->bindValue(10, $_REQUEST["appVersion"]);

        $stmt->bindValue(11, $_REQUEST["userId"]);

        // SQL実行
        $stmt->execute();
    }

}
///-----------------------------------------
// ユーザーをログを登録する
///-----------------------------------------
function InsertUserDataLog($pdo)
{

    $query = "INSERT INTO LOG_M_USER (USERID,USERNAME, USERLEVEL,  MONEY,  HP,ATK,DEF,SPD, OS, APPVERSION, CREATEDATE, UPDATEDATE) 
                  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, current_timestamp, current_timestamp)";

    if ($stmt = $pdo->prepare($query))
    {
        // パラメータをバインド
        $stmt->bindValue(1, $_REQUEST["userId"]);
        $stmt->bindValue(2, $_REQUEST["userName"]);
		$stmt->bindValue(3, $_REQUEST["userlevel"]);
        $stmt->bindValue(4, $_REQUEST["money"]);

        $stmt->bindValue(5, $_REQUEST["hp"]);
        $stmt->bindValue(6, $_REQUEST["atk"]);
        $stmt->bindValue(7, $_REQUEST["def"]);
        $stmt->bindValue(8, $_REQUEST["spd"]);
        
        $stmt->bindValue(9, $_REQUEST["os"]);
        $stmt->bindValue(10, $_REQUEST["appVersion"]);

        // SQL実行
        $stmt->execute();
    }

}


///-----------------------------------------
// ユーザー情報を削除する(未使用？)
///-----------------------------------------
function DeleteUserData($pdo)
{
    $query = "DELETE FROM M_USER WHERE USERID = ?";

    if ($stmt = $pdo->prepare($query))
    {
        // パラメータをバインド
        $stmt->bindValue(1, $_REQUEST["userId"]);
        // SQL実行
        $stmt->execute();
    }
}
