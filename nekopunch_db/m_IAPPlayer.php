<?php

// DB接続
require_once("mysql_connect.php");

// 処理分岐
switch ($_REQUEST["process"])
{
    case "INSERT":
        // ユーザー新規登録
        InsertIAPPlayer($pdo);
        break;
}


///-----------------------------------------
// 課金ユーザーを登録する
///-----------------------------------------
function InsertIAPPlayer($pdo)
{
    $query = "INSERT INTO M_IAP_PLAYER (USERID, USERNAME, STAR, OS, CREATEDATE) VALUES (?, ?, ?, ?, current_timestamp)";

    if ($stmt = $pdo->prepare($query))
    {
        // パラメータをバインド
        $stmt->bindValue(1, $_REQUEST["userId"]);
        $stmt->bindValue(2, $_REQUEST["userName"]);
        $stmt->bindValue(3, $_REQUEST["star"]);
        $stmt->bindValue(4, $_REQUEST["OS"]);
        // SQL実行
        $stmt->execute();
    }

}

