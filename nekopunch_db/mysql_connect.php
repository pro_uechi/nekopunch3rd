<?php
// 定数定義
require_once("const.php");

// MySQLへ接続
try {
    $pdo = new PDO(
        'mysql:dbname='.DB_NAME.';host='.DB_HOST.';charset=utf8',
        DB_USER,
        DB_PASS,
        array (
            //PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_SILENT,
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
        )
    );
} catch (PDOException $e) {
    //header('Content-Type: text/plain; charset=UTF-8', true, 500);
    header('Content-Type: text/plain; charset=UTF-8', true, 999);
    exit('Error:'.$e->getMessage());
}

// 出力形式(JSON形式)
header("Content-Type: application/json; charset=UTF-8");
?>
